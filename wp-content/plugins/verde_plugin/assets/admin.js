jQuery(document).ready(function($){
    
    // WYSIWYG
    $('.wysiwyg').cleditor({
        width: '79.6%',
        height: 200
    })
    
    // jQuery UI Date Picker
    $(".date-picker").datetimepicker()
    
    // Message
    $(".fade").animate({
        width:'auto'
    }, 2500).slideToggle(350);

    // Delete Confirmation
    $(".fw-delete").click(function(){
        var answer = confirm("Are you sure?")
        if (answer){
            return true;
        }
        else{
            return false;
        }
    })  
    
    //Upload file
    $(".upload_image_button").click(function() {
       formfield = $(this).parent().find(".upload_image").attr('name');
       imginputbox = $(this).parent().find(".upload_image").attr('id');
       tb_show('Upload a logo', 'media-upload.php?type=image&TB_iframe=true&post_id=0', false);
       $("#TB_window").attr('style', 'display:block; top:52px; left:50%; width: 783px; margin-left: -391px;');
       return false;
    });

    window.send_to_editor = function(html) {
        imgurl = $('img',html).attr('src');
        $('#'+imginputbox).val(imgurl);
        tb_remove();
    }
    	
})