<?php

if ( class_exists('WPCF7') ){
	function add_custom_scripts() {
		global $wp_scripts;
		if(class_exists('WPCF7')) {
			wp_register_script( '_cf7form',		WP_CONTENT_URL  .'/plugins/contact-form-7/includes/js/jquery.form.min.js', 'jquery');
			wp_register_script( '_cf7scripts',	WP_CONTENT_URL  .'/plugins/contact-form-7/includes/js/scripts.js', 'jquery');
			$_wpcf7 = array();
			if ( defined( 'WP_CACHE' ) && WP_CACHE ) {
				$_wpcf7['cached'] = 1;
			}
			if ( wpcf7_support_html5_fallback() ) {
				$_wpcf7['jqueryUi'] = 1;
			}
			wp_localize_script( '_cf7scripts', '_wpcf7', $_wpcf7 );
		}
	
		$wp_scripts->do_items('jquery');
		$wp_scripts->do_items('_cf7form');
		$wp_scripts->do_items('_cf7scripts');
	}
	add_action ('load_custom_scripts', 'add_custom_scripts', 15);
};

function verde_logo() {

    $logo_type = verde_top('logo_type');

    switch ($logo_type) {
        case 'text_logo':
            $logo = '<a class="a-rounded" href="'.verde_top('logo_link').'" title=""><h1>' . verde_top('logo_text') . '</h1></a>';
            break;
        case 'image_logo':
            $logo = '<a href="'.verde_top('logo_link').'" title=""><img src="' . verde_top('logo_image') . '" alt="" /></a>';
            break;
        default:
            $logo = '';
    }

    return $logo;
}

function verde_content() {
    if (verde_top('module_content_intro') == 'enable') {
        $content = '<h3>'.verde_top('page_content').'</h3>';
    } else {
        $content = '';
    }
    return $content;
}

function verde_countdown() {
	if (verde_top('module_countdown') == 'enable') {
		include('language.php');
        $date = verde_top('launch_date');
        $today = date('Y-m-d h:i:s a');
        if (strtotime($date) > strtotime($today)){

        $content = '<div class="timer">
		<script type="text/javascript">
		var montharray=new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")
		function countdown(yr,m,d,h,i,s){
		theyear=yr;themonth=m;theday=d;thehour=h;themin=i;thesec=s
		var today=new Date()
		var todayy=today.getYear()
		if (todayy < 1000)
		todayy+=1900
		var todaym=today.getMonth()
		var todayd=today.getDate()
		var todayh=today.getHours()
		var todaymin=today.getMinutes()
		var todaysec=today.getSeconds()
		var todaystring=montharray[todaym]+" "+todayd+", "+todayy+" "+todayh+":"+todaymin+":"+todaysec
		futurestring=montharray[m-1]+" "+d+", "+yr+" "+h+":"+i+":"+s
		dd=Date.parse(futurestring)-Date.parse(todaystring)
		dday=Math.floor(dd/(60*60*1000*24)*1)
		dhour=Math.floor((dd%(60*60*1000*24))/(60*60*1000)*1)
		dmin=Math.floor(((dd%(60*60*1000*24))%(60*60*1000))/(60*1000)*1)
		dsec=Math.floor((((dd%(60*60*1000*24))%(60*60*1000))%(60*1000))/1000*1)
		if((dday<=0)&&(dhour<=0)&&(dmin<=0)&&(dsec<=0)){';
        if (verde_top('date_ended') == 'text') {
        	$content.='$(\'.timer\').text(\''.verde_top('date_text').'\');';	
	    } else{
	    	$content.='$(\'.timer\').fadeOut();';	
	    }
		$content.= '}
		else 
		jQuery(\'.timer\').html(\'<span class="days">\'+dday+\'</span> <span class="daysText"></span> / <span class="hours">\'+dhour+\'</span> <span class="hoursText"></span> / <span class="minutes">\'+dmin+\'</span> <span class="minutesText"></span> / <span class="seconds">\'+dsec+\' </span><span class="secondsText"></span>\');
		if ( dday == 1 ){$(\'.daysText\').text("'.$day.'"); }else{ $(\'.daysText\').text("'.$days.'"); }
		if ( dhour == 1 ){ $(\'.hoursText\').text("'.$hour.'"); }else{ $(\'.hoursText\').text("'.$hours.'"); }
		if ( dmin == 1 ){ $(\'.minutesText\').text("'.$minute.'"); }else{ $(\'.minutesText\').text("'.$minutes.'"); }
		if ( dsec == 1 ){ $(\'.secondsText\').text("'.$second.'"); }else{ $(\'.secondsText\').text("'.$seconds.'");}
		setTimeout("countdown(theyear,themonth,theday,thehour,themin,thesec)",1000);
		}
		countdown(' . date('Y', strtotime($date)) . ',' . date('m', strtotime($date)) . ',' . date('d', strtotime($date)) . ',' . date('H', strtotime($date)) . ',' . date('i', strtotime($date)) . ',' . date('s', strtotime($date)) . ')
		</script>
		    <noscript>
		        <span class="launch-date">' . date('M dS, Y') . '</span>
		    </noscript>
		</div>';

        }else{
	        if (verde_top('date_ended') == 'text') {	
		        $content = '
		        <div class="timer">
				<p>'.verde_top('date_text').'</p>
		        </div>';
	        }else{
	        	$content = '';
	        }
        }

    } else {
        $content = '';
    }
    return $content;
}

function verde_email_subscription() {
	$content = '
	<script type="text/javascript">
	jQuery(\'.success-message\').hide();
    jQuery(\'.error-message\').hide();

    jQuery(\'.subscribe form\').submit(function() {
        var postdata = jQuery(\'.subscribe form\').serialize();
        jQuery.ajax({
            type: \'POST\',
            url: \''.verde_product_info('extend_url').'/themes/php/sendmail.php\',
            data: postdata,
            dataType: \'json\',
            success: function(json) {
                if(json.valid == 0) {
                    jQuery(\'.success-message\').hide();
                    jQuery(\'.error-message\').hide();
                    jQuery(\'.error-message\').html(json.message);
                    jQuery(\'.error-message\').fadeIn().delay(3000).fadeOut();
                }
                else {
                    jQuery(\'.error-message\').hide();
                    jQuery(\'.success-message\').hide();
                    jQuery(\'.subscribe form\').hide().delay(3000).fadeIn();';
				if (verde_top('redirect_active') == 'enable'){
				$content.='
					setTimeout(function() { 
					    window.open(\''.verde_top('redirect_link').'\'); 
					 }, 2000);';
				}
    			$content.='
				 	jQuery(\'.subscribe form input\').val(\'\');
                 	jQuery(\'.success-message\').html(json.message);
                    jQuery(\'.success-message\').fadeIn().delay(2000).fadeOut();
                }
            }
        });
        return false;
    });
	</script>';
    return $content;
    
}

function cookies_content(){
	$content = '
	<script type="text/javascript">
		jQuery(".close-cookies").click(function() {
			jQuery("#cookies-message").attr(\'class\',\'fadeout_1\').delay(1000).fadeOut();;
		});
	</script>';
	return $content;
}

function verde_map() {
	$content = '
	<script type="text/javascript">
	var $map 				= jQuery(\'#map\'),
	$address 			= \''.verde_top('direction').'\'; 

	$map.gMap({
		address: $address,
		zoom: 14,
		scrollwheel: false,
		navigationControl: false,
		mapTypeControl: false,
		scaleControl: false,
		draggable: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});
	</script>';
return $content;
}