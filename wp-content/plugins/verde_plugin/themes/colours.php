<?php 

echo '

	html, 
	body, 
	h1, 
	.links a, 
	textarea, 
	input[type="text"], 
	input[type="text"]:hover, 
	input[type="text"]:active, 
	input[type="email"]:focus,
	input[type="email"], 
	input[type="email"]:focus, 
	input[type="email"]:hover, 
	input[type="email"]:active, 
	#mce-EMAIL{
	    color: #'.verde_top('main_color').';
	}

	textarea,
	input,
	input:hover,
	input:focus,
	input:active,
	input[type="text"], 
	input[type="text"]:focus, 
	input[type="text"]:hover, 
	input[type="text"]:active, 
	input[type="email"],
	input[type="email"]:focus,
	input[type="email"]:hover,
	input[type="email"]:active,
	button,
	button[type="submit"]{
		border: 1px solid #'.verde_top('main_color').';
	}
	
	input:-moz-placeholder,
	textarea:-moz-placeholder {
	  color: #'.verde_top('main_color').';
	}
	input::-moz-placeholder,
	textarea::-moz-placeholder {
	  color: #'.verde_top('main_color').';
	}
	input:-ms-input-placeholder,
	textarea:-ms-input-placeholder {
	  color: #'.verde_top('main_color').';
	}
	input::-webkit-input-placeholder,
	textarea::-webkit-input-placeholder {
	  color: #'.verde_top('main_color').';
	}
	
	.a-rounded{
	   	border: 2px solid #'.verde_top('main_color').';
	}
	
	.a-rounded:hover{
		background: #'.verde_top('custom_link_color').';
		border: 2px solid #'.verde_top('custom_link_color').';
	}
	
	.links{
		border-top: 1px dashed #'.verde_top('main_color').';
		border-bottom: 1px dashed #'.verde_top('main_color').';
	}
	
	.links a{
		border: 1px solid #'.verde_top('main_color').';
	}
	
	.links a:hover{
		background: #'.verde_top('custom_link_color').';
		border: 1px solid #'.verde_top('custom_link_color').';
	}
	
	textarea, 
	input[type="text"], 
	input[type="text"]:hover, 
	input[type="text"]:active, 
	input[type="email"]:focus,
	input[type="email"], 
	input[type="email"]:focus, 
	input[type="email"]:hover, 
	input[type="email"]:active, 
	#mce-EMAIL {
		border: 1px solid #'.verde_top('main_color').';
	}
	
	.btn{
		background: #'.verde_top('main_color').';
		border: 1px solid #'.verde_top('main_color').';
	}
	
	.btn:hover {
		background: #'.verde_top('custom_link_color').';
		border: 1px solid #'.verde_top('custom_link_color').';
		color: #'.verde_top('main_color').';
	}
	
	.poster-image {
		background: url('.verde_top('image_replacement').');
	}
	
	.wpcf7-form input,
	.wpcf7-form input:hover,
	.wpcf7-form input:focus,
	#contactform input, 
	textarea{
		border: 1px solid #'.verde_top('main_color').' !important;
		color: #'.verde_top('main_color').';
	}
	
	.wpcf7-form .wpcf7-submit,
	#contactform .button{
		background: #'.verde_top('main_color').';
		border: 1px solid #'.verde_top('main_color').';
	}
	
	.wpcf7-form .wpcf7-submit:hover,
	.wpcf7-form .wpcf7-submit:focus,
	#contactform .button:hover{
		background: #'.verde_top('custom_link_color').';
		border: 1px solid #'.verde_top('custom_link_color').' !important;
		color: #'.verde_top('main_color').';
	}
	
	.success-message,
	.success-message-2,
	.wpcf7-mail-sent-ok{
		color: #'.verde_top('custom_link_color').';
	}
	
	.footer {
		border-top: 1px solid #'.verde_top('main_color').';
	}
	
	.back-to-top a{
		background: #'.verde_top('main_color').';
		border:1px solid #'.verde_top('main_color').';
	}
	
	.back-to-top a:hover{
		background: #'.verde_top('custom_link_color').';
	}
	
	.map-image{
		background: url('.verde_top('image_map').') repeat top left;
	}
	
	p.footer-social a,
	p.footer-social-2 a{
		color: #'.verde_top('main_color').';
		border: 1px solid #'.verde_top('main_color').';
	}
	
	p.footer-social a:hover,
	p.footer-social-2 a:hover{
		background: #'.verde_top('custom_link_color').';
		border: 1px solid #'.verde_top('custom_link_color').';
	}
	
	@media (max-width: 480px) {
		.btn{
			background: #'.verde_top('main_color').';
			border: 1px solid #'.verde_top('main_color').';
		}	
		.btn:hover{
			background: #'.verde_top('custom_link_color').';
			border: 1px solid #'.verde_top('custom_link_color').' !important;
			color: #'.verde_top('main_color').';
		}
	
	}
	
	</style> ';

?>