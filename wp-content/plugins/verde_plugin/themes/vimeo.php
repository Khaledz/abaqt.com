<?php
echo '
<script type="text/javascript">

if( !device.tablet() && !device.mobile() ) {

	jQuery(document).ready(function(){
		"use strict";
		
		/* ---------------------------------------------------------------------- */
		/*  VIDEO VIMEO
		/* ---------------------------------------------------------------------- */
			
		jQuery(function(){
			$(\'#fullscreen-vimeo\').bgVimeoVideo({
				videoId: '.verde_top('video_internal_vimeo').',';
				if ( verde_top('video_sound_vimeo') == 'enable' ){
					echo 'videoVolume: 5';
				} else {
					echo 'videoVolume: 0'; 
				}
		    echo '
		    });
		});
			
	});

} else {
	
	jQuery(\'#bgimg\').addClass(\'poster-image\');
	
}
	
	
</script>';
?>
