/* ---------------------------------------------------------------------- */
/*	LOADER
/* ---------------------------------------------------------------------- */
jQuery(window).load(function() {
	"use strict";
	jQuery("#loading").fadeOut("1000", function() {
	// Animation complete
		jQuery('#loading img').css("display","none");
		jQuery('#loading').css("display","none");
		jQuery('#loading').css("background","none");
		jQuery('#loading').css("width","0");
		jQuery('#loading').css("height","0");
	});
	jQuery("#part1").attr('class', 'fadein_1');
	jQuery("#part2").attr('class', 'fadein_2');
	jQuery("#part2_1").attr('class', 'fadein_2_1');
	jQuery("#part2_2").attr('class', 'fadein_2_2');
	jQuery("#part3").attr('class', 'fadein_3');
	jQuery("#cookies-message").attr('class', 'fadein_1');
});

jQuery(document).ready(function(){
	"use strict";
	/* ---------------------------------------------------------------------- */
	/*	DIV HOME POSITION
	/* ---------------------------------------------------------------------- */
	
	var windowHeight = jQuery(window).height();
	var homepageHeight = jQuery('#home').height();
	
	if (windowHeight >= homepageHeight){
		jQuery('#home').css("margin-top", ((windowHeight-homepageHeight))/2);
	}	

	jQuery(window).resize(function() {		
		var windowHeight = jQuery(window).height();
		var homepageHeight = jQuery('#home').height();
		
		if (windowHeight >= homepageHeight){
			jQuery('#home').css("margin-top", ((windowHeight-homepageHeight))/2);			
		}	
	});
	
	/* ---------------------------------------------------------------------- */
	/*	MAP HEIGHT
	/* ---------------------------------------------------------------------- */
	
	if (windowHeight >= homepageHeight){
		jQuery('#map-content').css("height", (windowHeight));
		jQuery('#map-content').css("margin-top", (windowHeight));
		jQuery('#canvas').css("height", (windowHeight));
	} else{
		jQuery('#map-content').css("height", (homepageHeight+50));
		jQuery('#map-content').css("margin-top", (homepageHeight+50));
		jQuery('#canvas').css("height", (homepageHeight+50));
	}
	
	jQuery(window).resize(function() {
		var windowHeight = jQuery(window).height();
		var homepageHeight = jQuery('#home').height();
		
		if (windowHeight >= homepageHeight){
			jQuery('#map-content').css("height", (windowHeight));
			jQuery('#map-content').css("margin-top", (windowHeight));
			jQuery('#canvas').css("height", (windowHeight));
		} else{
			jQuery('#map-content').css("height", (homepageHeight+50));
			jQuery('#map-content').css("margin-top", (homepageHeight+50));
			jQuery('#canvas').css("height", (homepageHeight+50));
		}
	});
	
	/* ---------------------------------------------------------------------- */
	/*  DIV CONTACTFORM POSITION
	/* ---------------------------------------------------------------------- */
	
	var footerHeight = jQuery('.footer').height();
	var contactHeight = jQuery('.contact').height();
	var windowtWidth = jQuery(window).width();
	var mapContentHeight = jQuery('#map-content').height();
	
	if (windowtWidth >= 478){
		var difference = mapContentHeight - footerHeight;
		jQuery('.contact').css("top", ((difference-contactHeight)/2));
	} else{
		jQuery('.contact').css("top", '0');
	}
	
	jQuery(window).resize(function() {
		var footerHeight = jQuery('.footer').height();
		var contactHeight = jQuery('.contact').height();
		var windowtWidth = jQuery(window).width();
		var mapContentHeight = jQuery('#map-content').height();
		
		if (windowtWidth >= 478){
			var difference = mapContentHeight - footerHeight;
			jQuery('.contact').css("top", ((difference-contactHeight)/2));
		} else{
			jQuery('.contact').css("top", '0');
		}
	});
	
	/* ---------------------------------------------------------------------- */
	/*	SCROLL PAGE WITH EASING EFFECT
	/* ---------------------------------------------------------------------- */
    
	jQuery('#link-map').bind('click', function(e) {
	    e.preventDefault();
	    var target = this.hash;
	    jQuery.scrollTo(target, 750, {
	    	easing: 'swing',
	    	axis: 'y'
	    });
	});
	
	jQuery('#home-top').bind('click', function(e) {
		e.preventDefault();
	    jQuery.scrollTo(0, 750, {
	    	easing: 'swing',
	    	axis: 'y'
	    });
	})
	
	/* ---------------------------------------------------------------------- */
	/*	CIRCULAR COUNTDOWN
	/* ---------------------------------------------------------------------- */
	
	if(jQuery("#DateCountdown").length > 0) {
		
		countDownCircular();
		
		function countDownCircular() {
			jQuery("#DateCountdown").TimeCircles({
			    "animation": "smooth",
			    "bg_width": 0.2,
			    "fg_width": 0.016666666666666666,
			    "circle_bg_color": "#fff",
			    "time": {
			        "Days": {
			            "text": "days",
			            "color": "#fff",
			            "show": true
			        },
			        "Hours": {
			            "text": "hours",
			            "color": "#fff",
			            "show": true
			        },
			        "Minutes": {
			            "text": "minutes",
			            "color": "#fff",
			            "show": true
			        },
			        "Seconds": {
			            "text": "seconds",
			            "color": "#fff",
			            "show": true
			        }
			    }
			});
		};
		
		jQuery(window).resize(function() {		
			jQuery("#DateCountdown").TimeCircles().rebuild();
		});
	
	}
	
	/* ---------------------------------------------------------------------- */
	/*	TEXT EFFECTS
	/* ---------------------------------------------------------------------- */
	if( !device.tablet() && !device.mobile() ) {
		
		jQuery(window).scroll(function(){
			var windowHeight = jQuery(window).height();
			var homepageHeight = jQuery('#home').height();
			
			  // get the height of #wrap
			  var h = jQuery(window).height();
			  var y = jQuery(window).scrollTop();
			  
			  if (windowHeight >= homepageHeight){
				  var altura1 = h*.15;
			  }else{
				  var altura1 = h*.75;
			  }
			  
			  if ( y > altura1 ){
				  jQuery("#part1").attr('class', 'fadeout_1');
				  jQuery("#part2").attr('class', 'fadeout_2');
				  jQuery("#part2_1").attr('class', 'fadeout_2_1');
				  jQuery("#part3").attr('class', 'fadeout_3');
			  } else{
				  jQuery("#part1").attr('class', 'fadein_1');
				  jQuery("#part2").attr('class', 'fadein_2');
				  jQuery("#part2_1").attr('class', 'fadein_2_1');
				  jQuery("#part3").attr('class', 'fadein_3');
			  } 
			  
			  if ( y < (h*.75) ){
				  jQuery("#part4").attr('class', 'fadeout_4');
				  jQuery("#part5_1").attr('class', 'fadeout_5_1');
				  jQuery("#part5_2").attr('class', 'fadeout_5_2');
				  jQuery("#part5_3").attr('class', 'fadeout_5_3');
				  jQuery("#part5_4").attr('class', 'fadeout_5_4');
				  jQuery("#part6").attr('class', 'fadeout_6');
			  } else{
				  jQuery("#part4").attr('class', 'fadein_4');
				  jQuery("#part5_1").attr('class', 'fadein_5_1');
				  jQuery("#part5_2").attr('class', 'fadein_5_2');
				  jQuery("#part5_3").attr('class', 'fadein_5_3');
				  jQuery("#part5_4").attr('class', 'fadein_5_4');
				  jQuery("#part6").attr('class', 'fadein_6');
			  } 
			 
		 });
	}else{
		jQuery("#part4").attr('class', 'fadein_4');
		jQuery("#part5_1").attr('class', 'fadein_5_1');
		jQuery("#part5_2").attr('class', 'fadein_5_2');
		jQuery("#part5_3").attr('class', 'fadein_5_3');
		jQuery("#part5_4").attr('class', 'fadein_5_4');
		jQuery("#part6").attr('class', 'fadein_6');
	}
		
	/* ---------------------------------------------------------------------- */
	/*  TOOLTIP
	/* ---------------------------------------------------------------------- */
	
	jQuery('.footer-social a').tooltip();
	jQuery('.footer-social-2 a').tooltip();
			
});
