<?php
$content = '
<script type="text/javascript">

if( !device.tablet() && !device.mobile() ) {

(function(jQuery) {
  "use strict";
	// initialize BigVideo
    var BV = new jQuery.BigVideo();
	BV.init();
	';
if (verde_top('video_sound') == 'enable'){
	$content.= 'BV.show(\''.verde_top('video_internal').'\');';
	}else{
	$content.= 'BV.show(\''.verde_top('video_internal').'\',{ambient:true});';
	}	
$content.='})(jQuery);

} else {
	
	jQuery(\'#bgimg\').addClass(\'poster-image\');
	
}

</script>';
?>
