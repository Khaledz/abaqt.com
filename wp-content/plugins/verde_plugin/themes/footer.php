<div class="row">
	<div class="span12">
			<p class="footer-social">
			<?php if (verde_top('facebook_profile') != ''): ?>
				<a href="<?php echo verde_top('facebook_profile')?>" target="_blank" title="Facebook" data-gal="tooltip" data-placement="top" data-original-title="Facebook">
					<span class="fa fa-facebook"></span>
				</a>
			<?php endif;
			if (verde_top('twitter_profile') != ''): ?>
				<a href="<?php echo verde_top('twitter_profile')?>" target="_blank" title="Twitter" data-gal="tooltip" data-placement="top" data-original-title="Twitter">
					<span class="fa fa-twitter"></span>
				</a>
			<?php endif;
			if (verde_top('behance_profile') != ''): ?>
				<a href="<?php echo verde_top('behance_profile')?>" target="_blank" title="Behance" data-gal="tooltip" data-placement="top" data-original-title="Behance">
					<span class="fa fa-behance"></span>
				</a>
			<?php endif;
			if (verde_top('deviantart_profile') != ''): ?>
				<a href="<?php echo verde_top('deviantart_profile')?>" target="_blank" title="Deviantart" data-gal="tooltip" data-placement="top" data-original-title="Deviantart">
					<span class="fa fa-deviantart"></span>
				</a>
			<?php endif;
			if (verde_top('digg_profile') != ''): ?>
				<a href="<?php echo verde_top('digg_profile')?>" target="_blank" title="Digg" data-gal="tooltip" data-placement="top" data-original-title="Digg">
					<span class="fa fa-digg"></span>
				</a>
			<?php endif;
			if (verde_top('dribbble_profile') != ''): ?>
				<a href="<?php echo verde_top('dribbble_profile')?>" target="_blank" title="Dribbble" data-gal="tooltip" data-placement="top" data-original-title="Dribbble">
					<span class="fa fa-dribbble"></span>
				</a>
			<?php endif;
			if (verde_top('flickr_profile') != ''): ?>
				<a href="<?php echo verde_top('flickr_profile')?>" target="_blank" title="Flickr" data-gal="tooltip" data-placement="top" data-original-title="Flickr">
					<span class="fa fa-flickr"></span>
				</a>
			<?php endif;
			if (verde_top('github_profile') != ''): ?>
				<a href="<?php echo verde_top('github_profile')?>" target="_blank" title="Github" data-gal="tooltip" data-placement="top" data-original-title="Github">
					<span class="fa fa-git"></span>
				</a>
			<?php endif;
			if (verde_top('googleplus_profile') != ''): ?>
				<a href="<?php echo verde_top('googleplus_profile')?>" target="_blank" title="Google Plus" data-gal="tooltip" data-placement="top" data-original-title="Google Plus">
					<span class="fa fa-google-plus"></span>
				</a>
			<?php endif;
			if (verde_top('instagram_profile') != ''): ?>
				<a href="<?php echo verde_top('instagram_profile')?>" target="_blank" title="Instagram" data-gal="tooltip" data-placement="top" data-original-title="Instagram">
					<span class="fa fa-instagram"></span>
				</a>
			<?php endif;
			if (verde_top('lastfm_profile') != ''): ?>
				<a href="<?php echo verde_top('lastfm_profile')?>" target="_blank" title="Lastfm" data-gal="tooltip" data-placement="top" data-original-title="Lastfm">
					<span class="fa fa-lastfm"></span>
				</a>
			<?php endif;
			if (verde_top('linkedin_profile') != ''): ?>
				<a href="<?php echo verde_top('linkedin_profile')?>" target="_blank" title="Linkedin" data-gal="tooltip" data-placement="top" data-original-title="Linkedin">
					<span class="fa fa-linkedin"></span>
				</a>
			<?php endif;
			if (verde_top('pinterest_profile') != ''): ?>
				<a href="<?php echo verde_top('pinterest_profile')?>" target="_blank" title="Pinterest" data-gal="tooltip" data-placement="top" data-original-title="Pinterest">
					<span class="fa fa-pinterest"></span>
				</a>
			<?php endif;
			if (verde_top('skype_profile') != ''): ?>
				<a href="<?php echo verde_top('skype_profile')?>" target="_blank" title="Skype" data-gal="tooltip" data-placement="top" data-original-title="Skype">
					<span class="fa fa-skype"></span>
				</a>
			<?php endif;	
			if (verde_top('tumblr_profile') != ''): ?>
				<a href="<?php echo verde_top('tumblr_profile')?>" target="_blank" title="Tumblr" data-gal="tooltip" data-placement="top" data-original-title="Tumblr">
					<span class="fa fa-tumblr"></span>
				</a>
			<?php endif;
			if (verde_top('vimeo_profile') != ''): ?>
				<a href="<?php echo verde_top('vimeo_profile')?>" target="_blank" title="Vimeo" data-gal="tooltip" data-placement="top" data-original-title="Vimeo">
					<span class="fa fa-vimeo"></span>
				</a>
			<?php endif;
			if (verde_top('wordpress_profile') != ''): ?>
				<a href="<?php echo verde_top('wordpress_profile')?>" target="_blank" title="Wordpress" data-gal="tooltip" data-placement="top" data-original-title="Wordpress">
					<span class="fa fa-wordpress"></span>
				</a>
			<?php endif;
			if (verde_top('youtube_profile') != ''): ?>
				<a href="<?php echo verde_top('youtube_profile')?>" target="_blank" title="Youtube" data-gal="tooltip" data-placement="top" data-original-title="Youtube">
					<span class="fa fa-youtube"></span>
				</a>
			<?php endif; ?>	
			</p>
	</div>
</div>	
