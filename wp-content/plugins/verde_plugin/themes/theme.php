<?php 
include('language.php');
require_once('theme_functions.php'); 
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]>
<!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
	<meta charset="utf-8" />
    
    <!-- Website Title & Description -->
    <title><?php echo verde_top('meta_title') ?></title>
    <meta name="keywords" content="<?php echo verde_top('meta_keywords') ?>">
    <meta name="description" content="<?php echo verde_top('meta_description') ?>">
    
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="True" />
    <meta name="MobileOptimized" content="320" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
   
    <?php if (verde_top('favicon_image') != ''){
   		echo '<link rel="shortcut icon" href="'.verde_top('favicon_image').'" />';
    }
    ?>
    
    <!-- Included CSS Files -->
    
    <link rel="stylesheet" href="<?php echo verde_product_info('extend_url'); ?>/themes/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo verde_product_info('extend_url'); ?>/themes/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?php echo verde_product_info('extend_url'); ?>/themes/css/style.css" />
    <link rel="stylesheet" href="<?php echo verde_product_info('extend_url'); ?>/themes/css/<?php echo verde_top('text_effects') ?>" />
    
    <?php
    switch (verde_top('back_type')) {
    	case 'images':
    		echo '<link rel="stylesheet" href="'.verde_product_info('extend_url').'/themes/css/supersized.css" type="text/css" media="screen" />';
    		echo '<link rel="stylesheet" href="'.verde_product_info('extend_url').'/themes/css/supersized.shutter.css" type="text/css" media="screen" />';
    		break;
    	case 'image':
    		echo '<link rel="stylesheet" href="'.verde_product_info('extend_url').'/themes/css/supersized.css" type="text/css" media="screen" />';
    		echo '<link rel="stylesheet" href="'.verde_product_info('extend_url').'/themes/css/supersized.shutter.css" type="text/css" media="screen" />';
    		break;
    	case 'video':
    		echo '<link rel="stylesheet" href="'.verde_product_info('extend_url').'/themes/css/bigvideo.css">';
    		break;
    	default:
    		echo '';
    }
    ?>
    <link rel="stylesheet" href="<?php echo verde_product_info('extend_url'); ?>/themes/css/supersized.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo verde_product_info('extend_url'); ?>/themes/css/supersized.shutter.css" type="text/css" media="screen" />
    
    <!-- Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=<?php echo verde_top('typo') ?>:100,200,300,400,600,700,800,900,200italic,300italic,400italic' rel='stylesheet' type='text/css'>
	
	<script type="text/javascript" src="<?php echo verde_product_info('extend_url'); ?>/themes/js/jquery-1.9.1.min.js"></script>	
        
    <?php 
    $typo=str_replace("+"," ",verde_top('typo'));
	echo '<style type="text/css">';
	
	if (verde_top('back_type') == 'none'){
		echo '
		html,body{
			background: #'.verde_top('back_color').' !important;
		}';
	}
	
	echo '
	html, body, textarea, input[type="text"], input[type="text"]:hover, input[type="text"]:active, input[type="email"]:focus, input[type="email"], input[type="email"]:focus, input[type="email"]:hover, input[type="email"]:active, #mce-EMAIL, .btn, #contactform input, textarea, #contactform .button{
		font-family: \''.$typo.'\', sans-serif;
	} ';
	
	include('colours.php');
	
	echo'<style type="text/css">'.verde_top('custom_css').'</style>';
	
	do_action('load_custom_scripts');
	
	?>
        
    </head>
    <body <?php echo (verde_top('countdown_style') == 'circular' ) ? 'class="countdown-circular"' : '';?>>
    
    <!-- Loader -->
	<div id="loading">
    	<img src="<?php echo verde_product_info('extend_url'); ?>/themes/img/loader.gif" alt="Website Loader"/>
    </div>
    
    <!-- Slideshow Pattern-->
    <?php if (verde_top('pattern') == 'enable'){
   		echo '<div class="slideshow-pattern '.verde_top('home_pattern_style').'"></div>';
    } 
    
    if ( (verde_top('back_type') == 'video') || (verde_top('back_type') == 'video_youtube') || (verde_top('back_type') == 'video_vimeo')  ){
    	echo'
    	<!-- Image replacement in mobile and tablets devices -->
		<div id="bgimg"></div>
		';
    }
    
    if ( verde_top('cookies_enable') == 'enable'){
    	echo'
    	<div id="cookies-message" class="fadeout_1">
    	<a href="#" class="close-cookies" title="Close cookies"><img src="'.verde_product_info('extend_url').'/themes/img/close.png" alt="Close cookies" /></a>
	    	'.verde_top('cookies_message').'
    	</div>
		';
    }
    
    if (verde_top('back_type') == 'abstract'){ echo '<div id="canvas" class="canvas"></div>'; }
    
    ?>
 	
 	<div class="container">
    <div class="row">
    <div class="span8 offset2" id="home">
        
        <div id="part1" class="fadeout_1">    
        <?php 
        echo verde_logo();
        echo verde_content();
        if (verde_top('countdown_style') == 'linear'){
        	echo verde_countdown();
        } else {
        	echo '<div id="DateCountdown" data-date="'.verde_top('launch_date').'" class="timerContent animate"></div>';
        }
        ?>
        </div>
        
        <?php if (verde_top('module_contact_form') == 'enable'):?>
        <div id="part2" class="fadeout_2">
			<div class="row">
			<div class="span8 links">
				<div class="row">
				
				<div id="part2_1" class="fadeout_2_1">	
					<div class="span4 offset2">
						<a id="link-map" href="#map-content"><?php echo $contact ?></a>
						
					</div>
				</div>
				
				</div>
			</div>
			</div> 
		</div>
		<?php endif; ?>
		
		<?php 
		if (verde_top('module_email_subscription') == 'enable'):?>
		<div id="part3" class="fadeout_3">
		<div class="singup" >
		<?php switch (verde_top('email_subscription_type')) {
			    case 'wp_database':
			        ?> 
			        <div class="subscribe">
                    <p><?php echo verde_top('page_content_subscription'); ?></p>
                    <form class="form-inline singup" action="#" method="post">
                        <input type="text" name="email" placeholder="<?php echo $input_subscription?>">
                        <button type="submit" class="btn"><span class="singup-image" ><img src="<?php echo verde_product_info('extend_url'); ?>/themes/img/send.png" alt="send email" /></span><span class="singup-text"><?php echo $send ?></span></button>
                    </form>
                    <div class="success-message"></div>
                    <div class="error-message"></div>
                	</div>
			        <?php 
			        echo verde_email_subscription();
			        break;
			    case 'mailchimp':
			        ?>
			        <div class="subscribe-mailchimp">
                    <p><?php echo verde_top('page_content_subscription'); ?></p>
                    <div id="mc_embed_signup">
						<form id="mc-form">
					        <input id="mc-email" type="email" placeholder="<?php echo $input_subscription?>" class="email">
					        <button type="submit" class="btn"><span class="singup-image" ><img src="<?php echo verde_product_info('extend_url'); ?>/themes/img/send.png" alt="send email" /></span><span class="singup-text"><?php echo $send ?></span></button>
					        <label for="mc-email"></label>
					    </form>
					</div>
                    </div>
				<?php
			    break;
			} ?>
		</div>
		<?php 
		endif; ?>
	
	<?php if (verde_top('module_social_media_icons') == 'top'):
	include ('footer2.php');
	endif; ?>
	
	</div>
	
	</div>
	</div> 
	
	</div>
	
	
	<?php if (verde_top('module_contact_form') == 'enable'): ?>
	<div id="map-content">
	
	<?php if (verde_top('choose_back_contact') == 'map') {?>
		<div id="map"></div>
	<?php }
	
	if (verde_top('choose_back_contact') == 'image') { ?>
		<div class="map-image"></div>
	<?php }
	
	if (verde_top('pattern_contact') == 'enable'):?>
		<div class="slideshow-pattern-map <?php echo verde_top('map_pattern_style');?>"></div>
	<?php endif; ?>
	
	<div class="container">
	<div class="row">
		<div class="span6 offset3 contact">	
		
			<div id="part4">
	            <h2><?php echo verde_top('page_title_contact')?></h2>
	            <h4><?php echo verde_top('page_content_contact')?></h4>
	            <?php if ( class_exists( 'WPCF7' ) ) : ?>
	            <p><?php echo do_shortcode(verde_top('contact_form_code'));?></p>
	            <?php endif;?> 
	        </div>
    
		</div>            	
	</div> 
	</div> 

	<div class="footer">
    <div id="part6">
	<div class="container">
		<div class="span12 back-to-top" <?php if (verde_top('module_social_media_icons') != 'bottom'){?> style="margin-bottom: 20px;"<?php } ?>>
			<a href="#home" id="home-top"><img class="back-to-top-1" src="<?php echo verde_product_info('extend_url'); ?>/themes/img/back-to-top.png" alt="Go back to top" /><img class="back-to-top-2" src="<?php echo verde_product_info('extend_url'); ?>/themes/img/back-to-top-hover.png" alt="Go back to top" /></a>
		</div>	
	<?php 
	if (verde_top('module_social_media_icons') == 'bottom'):
	include ('footer.php');
	endif; ?>
	
	</div>
	</div>
	</div>
	
    </div> <!-- /map-content -->
	<?php endif;
	
	if (verde_top('back_type') == 'video_youtube'){ ?>
   		<a id="bgndVideo"  data-property="{videoURL:'<?php echo verde_top('video_internal_youtube')?>',containment:'body',autoPlay:true, mute:<?php if (verde_top('video_sound_youtube') == 'enable'){echo "false";}else{echo "true";}?>, startAt:0,opacity:1,ratio:'16/9', addRaster:true}">My video</a>
    <?php }
    
    if (verde_top('back_type') == 'video_vimeo'){ ?>
    	<div id="fullscreen-vimeo"></div> 
	<?php } ?>
	
    </body>
    
    <!-- Javascript -->
    <script type="text/javascript" src="<?php echo verde_product_info('extend_url'); ?>/themes/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo verde_product_info('extend_url'); ?>/themes/js/jquery.countdown.js"></script>
	<script type="text/javascript" src="<?php echo verde_product_info('extend_url'); ?>/themes/js/device.min.js"></script>
	<?php if (verde_top('choose_back_contact') == 'map'): ?>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=<?php echo verde_top('google_api_code')?>"></script>
	<script type="text/javascript" src="<?php echo verde_product_info('extend_url'); ?>/themes/js/jquery.gmap.min.js"></script>
	<?php endif; ?>
    <script type="text/javascript" src="<?php echo verde_product_info('extend_url'); ?>/themes/js/jquery.scrollTo-1.4.3.1-min.js"></script>
    <script type="text/javascript" src="<?php echo verde_product_info('extend_url'); ?>/themes/js/scripts.js"></script>
        
    <?php
    if (verde_top('email_subscription_type') == 'mailchimp') {
   		echo '<script type="text/javascript" src="'.verde_product_info('extend_url').'/themes/js/jquery.ajaxchimp.js"></script>';
   		echo '<script type="text/javascript" src="'.verde_product_info('extend_url').'/themes/js/jquery.ajaxchimp.langs.js"></script>';
   		include ('ajaxchimp.php');
   		echo $content;
    }
    
    if (verde_top('countdown_style') == 'circular') {
    	echo '<script type="text/javascript" src="'.verde_product_info('extend_url').'/themes/js/TimeCircles.js"></script>';
    }
    
    if ((verde_top('module_contact_form') == 'enable') && (verde_top('choose_back_contact') == 'map' )) {
   		echo verde_map();
    }
    
    if (verde_top('cookies_enable') == 'enable'){
   		echo cookies_content();
    }
        
    switch (verde_top('back_type')) {
    	case 'images':
    		echo '<script type="text/javascript" src="'.verde_product_info('extend_url').'/themes/js/supersized.3.2.7.min.js"></script>';
			echo '<script type="text/javascript" src="'.verde_product_info('extend_url').'/themes/js/supersized.shutter.min.js"></script>';
			include('images.php');
    		echo $content;
    		break;
    	case 'image':
    		echo '<script type="text/javascript" src="'.verde_product_info('extend_url').'/themes/js/supersized.3.2.7.min.js"></script>';
			echo '<script type="text/javascript" src="'.verde_product_info('extend_url').'/themes/js/supersized.shutter.min.js"></script>';
			include('images.php');
    		echo $content;
    		break;
    	case 'video':
    		echo '<script type="text/javascript" src="'.verde_product_info('extend_url').'/themes/js/jquery-ui-1.8.22.custom.min.js"></script>';
    		echo '<script type="text/javascript" src="'.verde_product_info('extend_url').'/themes/js/jquery.imagesloaded.min.js"></script>';
    		echo '<script type="text/javascript" src="http://vjs.zencdn.net/4.9/video.js"></script>';
    		echo '<script type="text/javascript" src="'.verde_product_info('extend_url').'/themes/js/bigvideo.js"></script>';
    		include('video.php');
    		echo $content;
    		break;
    	case 'video_youtube':
    		echo '<script type="text/javascript" src="'.verde_product_info('extend_url').'/themes/js/jquery.mb.YTPlayer.js"></script>';
    		include('video_youtube.php');
    		echo $content;
    		break;
    	case 'video_vimeo':
    		echo '<script type="text/javascript" src="'.verde_product_info('extend_url').'/themes/js/video_vimeo.js"></script>';
    		include('vimeo.php');
    		break;		
    	case 'abstract':
    		echo '<script type="text/javascript" src="'.verde_product_info('extend_url').'/themes/js/fss.js"></script>';
    		include('abstract.php');
    		echo $content;
    		break;
    	default:
    		echo '';
    }
    echo verde_top('analytics');?>

</html>



