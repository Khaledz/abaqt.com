<?php

require_once('admin_options.php');
add_action('admin_menu', 'verde_init');

/*
$verde_uninstall_opt = '_site_' . sha1(verde_product_info('item_name'));
if (get_option($verde_uninstall_opt) == 1) {
    $verde_install_text = 'Re-Install';
} else {
    $verde_install_text = 'Uninstall';
}
*/

$verde_admin_menus = array(
    array(
        'page_type' => 'top_level',
        'page_title' => 'Verde Coming Soon',
        'menu_title' => 'Verde Plugin',
        'page_slug' => verde_product_info('settings_page_slug'),
        'permission' => '"manage_opitons"',
        'callback_function' => 'verde_item_info',
    	'menu_icon'   => 'dashicons-admin-generic',
    	'icon_url' => '',
        //'icon_url' => verde_product_info('assets_url').'/images/menu_icon.png', // Can be used with top_level only
    ),
      
    array(
        'page_type' => 'sub_level',
        'page_title' => 'Plugin Configuration',
        'menu_title' => 'Configuration',
        'page_slug' => 'verde_general_settings',
        'permission' => '"manage_opitons"',
        'callback_function' => 'verde_configuration',
        'icon_url' => '', // Can be used with top_level only
    ),
    
    array(
        'page_type' => 'sub_level',
        'page_title' => 'Branding',
        'menu_title' => 'Options',
        'page_slug' => 'verde_branding',
        'permission' => '"manage_opitons"',
        'callback_function' => 'verde_branding',
        'icon_url' => '', // Can be used with top_level only
    ),
    array(
        'page_type' => 'sub_level',
        'page_title' => 'Module Settings',
        'menu_title' => 'Module Settings',
        'page_slug' => 'verde_modules',
        'permission' => '"manage_opitons"',
        'callback_function' => 'verde_modules',
        'icon_url' => '', // Can be used with top_level only
    ), 

    array(
        'page_type' => 'sub_level',
        'page_title' => 'Social Profiles',
        'menu_title' => 'Social Profiles',
        'page_slug' => 'verde_social',
        'permission' => '"manage_opitons"',
        'callback_function' => 'verde_social',
        'icon_url' => '', // Can be used with top_level only
    ), 
    
    array(
        'page_type' => 'sub_level',
        'page_title' => 'Emails subscribed',
        'menu_title' => 'Emails subscribed',
        'page_slug' => 'verde_users',
        'permission' => '"manage_opitons"',
        'callback_function' => 'verde_users',
        'icon_url' => '', // Can be used with top_level only
    ), 
    
    /*
    array(
        'page_type' => 'sub_level',
        'page_title' => $verde_install_text,
        'menu_title' => $verde_install_text,
        'page_slug' => 'verde_uninstall',
        'permission' => '"manage_opitons"',
        'callback_function' => 'verde_uninstall',
        'icon_url' => '', // Can be used with top_level only
    ),
    */
);

function verde_configuration() {
    global $verde_options;
    verde_display_options($verde_options['configuration']);
}

function verde_branding() {
    global $verde_options;
    verde_display_options($verde_options['branding']);
}

function verde_modules() {
    global $verde_options;
    verde_display_options($verde_options['modules']);
}

function verde_social() {
    global $verde_options;
    verde_display_options($verde_options['social']);
}

function verde_users() {
    global $verde_options;
    verde_display_options($verde_options['users'], 'false', 'false');
}

function verde_item_info() {
    global $verde_options;
    verde_display_options($verde_options['info_page'], 'false');
}
/*
function verde_uninstall() {
    verde_display_uninstall();
}
*/

######################################################################
# PLUGIN INIT
######################################################################

function verde_init() {
    //global $verde_admin_menus, $verde_uninstall_opt;
    global $verde_admin_menus;
    foreach ($verde_admin_menus as $admin_page) {
        if ($admin_page['page_type'] == 'top_level') {
            $menu_page = add_menu_page($admin_page['page_title'], $admin_page['menu_title'], 'manage_options', $admin_page['page_slug'], $admin_page['callback_function'], $icon_url = $admin_page['icon_url']);
            add_action('admin_print_scripts', 'verde_admin_scripts');
            add_action('admin_print_styles', 'verde_admin_styles');          
        } else {
            $menu_page = add_submenu_page(verde_product_info('settings_page_slug'), $admin_page['page_title'], $admin_page['menu_title'], 'manage_options', $admin_page['page_slug'], $admin_page['callback_function']);
            add_action('admin_print_scripts', 'verde_admin_scripts');
            add_action('admin_print_styles', 'verde_admin_styles');
        }
    }
	add_action('admin_head', 'verde_install_db');

    // REMOVE FIRST LINK IN ADMIN PAGES MENU
    add_action('admin_head', 'verde_menu_fix');

    function verde_menu_fix() {
        echo '<style type="text/css">ul#adminmenu li.toplevel_page_' . verde_product_info('settings_page_slug') . ' .wp-first-item{display:none !important;} </style>';
    }

}
