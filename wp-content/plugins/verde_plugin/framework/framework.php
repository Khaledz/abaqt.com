<?php
ob_start();
#############################################################################
#############################################################################
# PRODUCT INFO
#############################################################################
#############################################################################

function verde_product_info($var) {
    global $wpdb;

    $product_info = array(
#######################################################################
        'item_type' => 'Plugin',
        // ITEM INFO
        'item_id' => '1',
        'item_name' => 'Verde Coming Soon Plugin',
        'item_version' => '3.0',
        'item_url' => 'http://themeforest.net/user/CreaboxThemes',
        // WORDPRESS VARIABLES
        'options_table_name' => $wpdb->prefix . 'verde_plugin', // UPDATE
        'settings_page_slug' => 'verde_plugin_info',
#######################################################################
        // General Links
        'contact_url' => 'admin@creabox.es',
    );

    if ($product_info['item_type'] == 'Plugin') {
        
        $extend_url = WP_CONTENT_URL . '/plugins/' . plugin_basename(dirname(__FILE__));
        $extend_url = str_replace('/framework', '', $extend_url);

        $assets_url = $extend_url.'/assets';
        $modules_url = $extend_url.'/modules';
        
        $product_dir = WP_PLUGIN_DIR . '/' . plugin_basename(dirname(__FILE__));
        $product_dir = str_replace('/framework', '', $product_dir);
        
        $deactivate_url = site_url('wp-admin/plugins.php');
    } elseif ($product_info['item_type'] == 'Theme') {
        $extend_url = get_template_directory_uri();
        $assets_url = get_template_directory_uri() . '/extend/assets';
        $modules_url = get_template_directory_uri() . '/extend/modules';
        $product_dir = get_template_directory();
        $deactivate_url = site_url('wp-admin/themes.php');
        
    }
    $assets_url = array(
        'product_url' => $extend_url,
        'extend_url' => $extend_url,
        'assets_url' => $assets_url,
        'deactivate_url' => $deactivate_url,
        'modules_url' => $modules_url,
        'product_dir' => $product_dir,
        
    );

    $return = array_merge($product_info, $assets_url);

    return $return[$var];
}

#############################################################################
#############################################################################
#############################################################################
#############################################################################
# REGISTER ADMIN SCRITPS 
#############################################################################

function verde_admin_scripts() {

    global $verde_admin_menus;

    foreach ($verde_admin_menus as $key => $value) {
        $verde_admin_pages[] = $value['page_slug'];
    }
	$page = $_SERVER['REQUEST_URI'];
	$plugin_page = strpos($page, 'verde');
    if (is_admin() && ($plugin_page != false)) {
        wp_register_script('jquery', verde_product_info('assets_url') . '/jquery-1.10.2.js');
        wp_enqueue_script('jquery');
        wp_register_script('jqueryUI', verde_product_info('assets_url') . '/jquery-ui.min.js');
        wp_enqueue_script('jqueryUI');
        
        // COLOR PICKER
        wp_register_script('color', verde_product_info('assets_url') . '/colorpicker/js/colorpicker.js');
        wp_enqueue_script('color');

        // DATE PICKER 
        wp_register_script('time-picker', verde_product_info('assets_url') . '/jquery-ui-timepicker-addon.js');
        wp_enqueue_script('time-picker');
        wp_register_script('sliderAccess', verde_product_info('assets_url') . '/jquery-ui-sliderAccess.js');
        wp_enqueue_script('sliderAccess');
                
        // CLEDITOR - WYSIWYG
        wp_register_script('verde-wysiwyg', verde_product_info('assets_url') . '/cleditor/jquery.cleditor.js');
        wp_enqueue_script('verde-wysiwyg');
        
        // FILE UPLOAD
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_register_script('verde-admin-js', verde_product_info('assets_url') . '/admin.js');
        wp_enqueue_script('verde-admin-js');

    }
}

#############################################################################
# REGISTER ADMIN STYLES 
#############################################################################

function verde_admin_styles() {
	$page = $_SERVER['REQUEST_URI'];
	$plugin_page = strpos($page, 'verde');
    if (is_admin() && ($plugin_page != false)) {
	    wp_enqueue_style('thickbox');
	    echo '<link rel="stylesheet" media="screen" href="' . verde_product_info('assets_url') . '/jquery-ui.min.css" type="text/css" />'."\n";
	    echo '<link rel="stylesheet" media="screen" href="' . verde_product_info('assets_url') . '/colorpicker/css/colorpicker.css" type="text/css" />'."\n";
	    echo '<link rel="stylesheet" media="screen" href="' . verde_product_info('assets_url') . '/cleditor/jquery.cleditor.css" type="text/css" />'."\n";
	    echo '<link rel="stylesheet" media="screen" href="' . verde_product_info('assets_url') . '/jquery-ui-timepicker-addon.css" type="text/css" />'."\n";
		
	    echo '<link rel="stylesheet" media="screen" href="' . verde_product_info('assets_url') . '/font-awesome.min.css" type="text/css" />'."\n";
	    echo '<link rel="stylesheet" media="screen" href="' . verde_product_info('assets_url') . '/admin.css" type="text/css" />'."\n";
	    echo '<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300" rel="stylesheet" type="text/css">'."\n";
	}
}

#############################################################################
# GET SAVED OPTION
#############################################################################

function verde_top($var) {
    global $wpdb; 
    /*
    $verde_uninstall_opt = '_site_' . sha1(verde_product_info('item_name'));
    if (get_option($verde_uninstall_opt) != 1) {
	    $table = verde_product_info('options_table_name');
	    $query = $wpdb->get_row("SELECT * FROM $table WHERE option_name = '{$var}'");
	
	    $data = @unserialize($query->option_value);
	    if ($data === false) {
	        return $query->option_value;
	    } else {
	        return unserialize($query->option_value);
	    }
    }
    */
	$table = verde_product_info('options_table_name');
	$query = $wpdb->get_row("SELECT * FROM $table WHERE option_name = '{$var}'");
    $data = @unserialize($query->option_value);
    if ($data === false) {
        return $query->option_value;
    } else {
        return unserialize($query->option_value);
    }
}

#############################################################################
# DATABASE SETUP 
#############################################################################

function verde_install_db() {

    global $wpdb, $verde_options;

    // Options Table Setup
    $options_table = verde_product_info('options_table_name');

    $sql[] = "
        CREATE TABLE IF NOT EXISTS `{$options_table}` (
            `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
            `option_name` varchar(64) NOT NULL DEFAULT '',
            `option_value` longtext NOT NULL,
            PRIMARY KEY (`option_id`),
            UNIQUE KEY `option_name` (`option_name`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;
        ";
    foreach ($sql as $query) {
        if ($wpdb->query($query)) {
            $return[] = 1;
        } else {
            echo mysql_error();
        }
    }
    
    $emails_subscrited = $wpdb->prefix . 'verde_emails';
    
	$sql2[] = "
        CREATE TABLE IF NOT EXISTS `{$emails_subscrited}` (
            `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
            `option_email` varchar(64) NOT NULL DEFAULT '',
            PRIMARY KEY (`option_id`),
            UNIQUE KEY `option_email` (`option_email`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;
        ";
    foreach ($sql2 as $query) {
        if ($wpdb->query($query)) {
            $return[] = 1;
        } else {
            echo mysql_error();
        }
    }


    # Update Options Table
    //-- Get Options Count
    $option_count = 0;
    foreach ($verde_options as $options) {
        foreach ($options as $option) {
            $option_count += 1;
        }
    }

    //- Get DB Count

    $query = $wpdb->get_row("SELECT COUNT(option_id) AS db_count FROM $options_table");

    $db_count = $query->db_count;

    if ($db_count == 0) {

        foreach ($verde_options as $options) {
            foreach ($options as $option) {
                
                
                if (is_array($option['odefault'])) {
                    $update_value = serialize($option['odefault']);
                } else {
                    $update_value = $option['odefault'];
                }
                $options_data = array(
                    'option_name' => $option['oid'],
                    'option_value' => $update_value,
                );
                verde_insert($options_table, $options_data);
            }
        }
        
    } elseif ($option_count != $db_count) {

        $temp_options_table = $options_table . '_temp';

        $wpdb->query("CREATE TABLE $temp_options_table SELECT * FROM $options_table");
        $wpdb->query("TRUNCATE TABLE $options_table");

        foreach ($verde_options as $options) {
            foreach ($options as $option) {
                
                
                if (is_array($option['odefault'])) {
                    $update_value = serialize($option['odefault']);
                } else {
                    $update_value = $option['odefault'];
                }
                $options_data = array(
                    'option_name' => $option['oid'],
                    'option_value' => $update_value,
                );
                verde_insert($options_table, $options_data);
            }
        }
        
        $temp_values = $wpdb->get_results("SELECT option_name, option_value FROM $temp_options_table");
        
        foreach($temp_values as $saved_value){
            $wpdb->query("UPDATE $options_table SET option_value = '{$saved_value->option_value}' WHERE option_name = '{$saved_value->option_name}'");
        }
        
        $wpdb->query("DROP TABLE $temp_options_table");
    }
}

################################################################################
# TRIM TEXT
################################################################################

function verde_trim_text($text, $cut) {
    if ($cut < strlen($text)) {
        return substr($text, '0', $cut) . '... ';
    } else {
        return substr($text, '0', $cut);
    }
}

################################################################################
# EMAIL VALIDATION
################################################################################

function verde_is_valid_email($email) {
    $result = preg_match('/[.+a-zA-Z0-9_-]+@[a-zA-Z0-9-]+.[a-zA-Z]+/', $email);
    if ($result == true) {
        return true;
    } else {
        return false;
    }
}

################################################################################
# Insert Function
################################################################################

function verde_insert($table, $data) {	
	global $wpdb;
    foreach ($data as $field => $value) {
        $fields[] = '`' . $field . '`';
        $values[] = "'" .  esc_sql($value) . "'";
        //$values[] = "'" . mysql_real_escape_string($value) . "'";
    }
    $field_list = join(',', $fields);
    $value_list = join(', ', $values);
    $query = "INSERT INTO `" . $table . "` (" . $field_list . ") VALUES (" . $value_list . ")";
    $wpdb->query($query);
    return $wpdb->insert_id;
}

################################################################################
# UPDATE FUNCTION
################################################################################

function verde_update($table, $data, $id_field, $id_value) {
	global $wpdb;
    foreach ($data as $field => $value) {
    	$fields[] = sprintf("`%s` = '%s'", $field,  esc_sql($value));
        //$fields[] = sprintf("`%s` = '%s'", $field, mysql_real_escape_string($value));
    }
    $field_list = join(',', $fields);
    $query = sprintf("UPDATE `%s` SET %s WHERE `%s` = %s", $table, $field_list, $id_field, intval($id_value));
    $wpdb->query($query);
    //mysql_query($query);
}

################################################################################
# EXPORT EMAILS SUBSCRIBED
################################################################################

if (isset($_POST["button_export"])) {
	include('export_emails.php');
}

################################################################################
# GET THE EMAILS SUBSCRIBED
################################################################################
function emails_subscribed(){
	global $wpdb;
	$table = $wpdb->prefix . 'verde_emails';
	$emails = $wpdb->get_results("SELECT * FROM ".$table."");
	return $emails;
}

################################################################################
# DELETE EMAIL SELECTED
################################################################################

if (isset($_POST["delete_email"])) {
	$email_id = $_POST['email_id'];
	$table = $wpdb->prefix . 'verde_emails';
	$wpdb->delete( 'wp_verde_emails', array( 'option_id' => $email_id ) );
}

################################################################################
# Get String based on permalink
################################################################################

function verde_string($url) {
    if (strpos($url, '?') > 0) {
        $string = $url . '&';
    } else {
        $string = $url . '?';
    }
    return $string;
}

################################################################################
# GET FULL URL
################################################################################

function verde_current_url() {
    $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

################################################################################
# Get Real IP Address
################################################################################

function verde_get_ip() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

#############################################################################
# LOAD MODULES
#############################################################################

function verde_load_modules($module_array) {
    if (!empty($module_array)) {
        foreach ($module_array as $file) {
            if (verde_product_info('item_type') == 'Plugin') {
                require_once(str_replace('/framework', '', WP_PLUGIN_DIR . '/' . plugin_basename(dirname(__FILE__))) . '/modules/' . $file . '.php');
            } elseif (verde_product_info('item_type') == 'Theme') {
                require_once(get_template_directory() . '/extend' . '/modules/' . $file . '.php');
            }
        }
    }
}

#############################################################################
# DISPLAY ADMIN OPTIONS
#############################################################################

function verde_display_options($options, $submit_button = null, $export_submit = null) {
    global $wpdb, $verde_msg;
    ?>

    <div id="nc-wrap" class="clearfix">
        <form action="" method="post">
            
    <?php
    if ((!empty($_POST)) && (!isset($_POST["button_export"]))) {

        $options_table = verde_product_info('options_table_name');

        foreach ($_POST as $key => $value) {

            if (is_array($value)) {
                $update_value = serialize($value);
            } else {
                $update_value = $value;
            }

            $wpdb->query("UPDATE {$options_table} SET option_value = '{$update_value}' WHERE option_name = '{$key}'");
        }
		
        if (!isset($_POST["delete_email"])){
        	$fw_message = _e('<span class="fw-message-success fade">Settings updated successfully!</span>','verde-plugin');
        }else{
        	$fw_message = _e('<span class="fw-message-success fade">Email removed successfully!</span>','verde-plugin');
        }
        echo $fw_message;
    }

    // MESSAGES WITHOUT POST
    echo $verde_msg;
    ?>


<table width="100%" border="0" cellspacing="0" cellpadding="0">

<?php 
foreach ($options as $option): 
?>


<?php if ($option['otype'] == 'heading'): ?>
<tr id="<?php echo $option['oid']; ?>" class="fw-heading"><td colspan="2"><?php echo _e($option['odefault'],'verde-plugin'); ?></td></tr>
<?php endif; ?>


<?php if ($option['otype'] == 'sub-heading'): ?>
<tr id="<?php echo $option['oid']; ?>" class="fw-heading"><td colspan="2"><?php echo _e($option['odefault'],'verde-plugin'); ?></td></tr>
<?php endif; ?>


<?php if ($option['otype'] == 'sub-heading-2'): ?>
<tr id="<?php echo $option['oid']; ?>" class="fw-heading-2"><td colspan="2"><?php echo _e($option['odefault'],'verde-plugin'); ?></td></tr>
<?php endif; ?>


<?php if ($option['otype'] == 'info'): ?>
<tr class="fw-info-white">
<td class="label">
<span><?php echo _e($option['oname'],'verde-plugin'); ?></span>
</td>
<td>
<span class="fw-info-panel"><?php echo _e($option['odefault'],'verde-plugin'); ?></span>
</td>
</tr>
<?php endif; ?>


<?php if ($option['otype'] == 'text' || $option['otype'] == 'textbox'): ?>
<tr>
<td class="label">
<span><?php echo _e($option['oname'],'verde-plugin'); ?></span>
</td>
<td>
<input type="text" name="<?php echo $option['oid']; ?>" value="<?php echo verde_top($option['oid']); ?>" /> 
<span class="fw-suffix"><?php echo $option['osuffix'] ?></span>
<span class="fw-desc"><?php echo _e($option['oinfo'],'verde-plugin'); ?></span>
</td>
</tr>
<?php endif; ?>


<?php if ($option['otype'] == 'textarea'): ?>
<tr>
<td class="label">
<span><?php echo _e($option['oname'],'verde-plugin'); ?></span>
</td>
<td>
<textarea rows="6" name="<?php echo $option['oid']; ?>"><?php echo verde_top($option['oid']); ?></textarea> 
<span class="fw-suffix"><?php echo $option['osuffix'] ?></span>
<span class="fw-desc"><?php echo _e($option['oinfo'],'verde-plugin'); ?></span>
</td>
</tr>
<?php endif; ?>


<?php if ($option['otype'] == 'wysiwyg'): ?>
<tr>
<td class="label">
<span><?php echo _e($option['oname'],'verde-plugin'); ?></span>
</td>
<td>
<textarea class="wysiwyg" rows="5" cols="40" name="<?php echo $option['oid']; ?>"><?php echo verde_top($option['oid']); ?></textarea>
<span class="fw-desc"><?php echo _e($option['oinfo'],'verde-plugin'); ?></span>
</td>
</tr>
<?php endif; ?>


<?php if ($option['otype'] == 'dropdown'): ?>
<tr>
<td class="label">
<span><?php echo _e($option['oname'],'verde-plugin'); ?></span>
</td>
<td>
<select name="<?php echo $option['oid'] ?>">
<option value=""><?php _e('Please Select', 'verde-plugin'); ?></option>
<?php
foreach ($option['ovalue'] as $key => $opt):
if ($key == verde_top($option['oid'])) {
echo '<option selected value="' . $key . '">' . $opt . '</option>' . "\n";
} else {
echo '<option value="' . $key . '">' . $opt. '</option>' . "\n";
}
endforeach;
?>
</select>
<span class="fw-desc"><?php echo _e($option['oinfo'],'verde-plugin'); ?></span>
</td>
</tr>
<?php endif; ?>


<?php if ($option['otype'] == 'radio'): ?>
<tr>
<td class="label">
<span><?php echo _e($option['oname'],'verde-plugin'); ?></span>
</td>
<td class="fw-checkboxes">
<span class="fw-info-panel">
<?php
foreach ($option['ovalue'] as $key => $opt):
if ($key == verde_top($option['oid'])) {
echo '<label><input checked name="' . $option['oid'] . '" type="radio" value="' . $key . '" /> ' . _e($opt,'verde-plugin') . ' </label>';
} else {
echo '<label><input name="' . $option['oid'] . '" type="radio" value="' . $key . '" /> ' . _e($opt,'verde-plugin') . ' </label>';
}
endforeach;
?>
</span>
<span class="fw-desc"><?php echo _e($option['oinfo'],'verde-plugin'); ?></span>
</td>
</tr>
<?php endif; ?>


<?php if ($option['otype'] == 'date'): ?>
<tr>
<td class="label">
<span><?php echo _e($option['oname'],'verde-plugin'); ?></span>
</td>
<td class="fw-relative">
<input type="text" name="<?php echo $option['oid']; ?>" class="date-picker" value="<?php echo verde_top($option['oid']) ?>" />
<span class="fw-desc"><?php echo _e($option['oinfo'],'verde-plugin'); ?></span>
</td>
</tr>
<?php endif; ?>


<?php if ($option['otype'] == 'list'): ?>
<?php $emails = emails_subscribed();?>
<tr class="total_emails">
<td class="label" colspan="2">
<h2>
<?php _e('Total emails subscribed: ', 'verde-plugin')?>
<?php echo count($emails); ?>
</h2>
</td>
<td>
</td>
</tr>
<?php $email_num = 1;?>
<?php foreach($emails as $email) :?>
<tr class="email_list">
<td class="label">
<span><?php echo $email->option_email; ?></span>
</td>
<td class="fw-relative">
<form name="form1" class="delete-email" method="post" action="#">
	<input name="email_id" type="hidden" value="<?php echo $email->option_id?>" />
	<input class="button-secondary" type="submit" name="delete_email" value="<?php _e('Delete', 'verde-plugin'); ?>">
</form>
</td>
</tr>
<?php $email_num++;?>
<?php endforeach; ?>
<?php endif; ?>


<?php if ($option['otype'] == 'upload' || $option['otype'] == 'file'): ?>
<tr>
<td class="label">
<span><?php echo _e($option['oname'],'verde-plugin'); ?></span>
</td>
<td class="fw-relative">
<input id="<?php echo 'img-' . rand(4, 9999); ?>" class="upload_image" type="text" name="<?php echo $option['oid']; ?>" value="<?php echo verde_top($option['oid']) ?>" />
<input class="upload_image_button button-secondary" type="button" value="<?php _e('Upload Image', 'verde-plugin'); ?>" />
<div class="info_select_image">
	<span class="fa fa-info info-option"></span>
	<img src="<?php echo verde_product_info('assets_url').'/images/select_image.jpg'; ?>" alt="Info select image" />
</div>
<span class="fw-desc"><?php echo _e($option['oinfo'],'verde-plugin'); ?></span>
</td>
</tr>
<?php endif; ?>


<?php if ($option['otype'] == 'color'): ?>
<tr>
<td class="label">
<span><?php echo _e($option['oname'],'verde-plugin'); ?></span>
</td>
<td>

<input id="<?php echo $option['oid']; ?>_color" name="<?php echo $option['oid']; ?>" class="fw-colorpicker-input" type="text" value="<?php echo verde_top($option['oid']) ?>" />
<span id="<?php echo $option['oid']; ?>_preview_color" class="fw_preview_color" style="background-color:#<?php echo verde_top($option['oid']); ?>">&nbsp;</span>
<span class="fw-desc"><?php echo _e($option['oinfo'],'verde-plugin'); ?></span>

<script type="text/javascript">
jQuery('#<?php echo $option['oid']; ?>_color').ColorPicker({
	color: '#0000ff',
	onShow: function (colpkr) {
		jQuery(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		jQuery(colpkr).fadeOut(500);
		return false;
	},
	onChange: function (hsb, hex, rgb) {
		jQuery('#<?php echo $option['oid']; ?>_color').val(hex);
		jQuery('#<?php echo $option['oid']; ?>_preview_color').css({"background-color":"#"+hex});
	}
});
</script>

</td>
</tr>

<?php endif; ?>

<?php endforeach; ?>

    <?php
    if ($submit_button == null):
	?>
	<tr>
		<td>&nbsp;</td>
		<td>
		<input name="save_settings" type="submit" class="button-primary" value="<?php _e('Save Settings', 'verde-plugin'); ?>" />  
    <?php endif; ?>
        
		</td>
	</tr>
	</table>
    </form>  
      
     <?php if ($export_submit != null): ?>
    <form name="form1" class="export-emails" method="post" action="#">
    	<p><?php echo _e('Export a csv file with the subscribed email:','verde-plugin') ?> </p>
		<input class="button-secondary" type="submit" name="button_export" value="<?php _e('Export emails', 'verde-plugin'); ?>">
    </form>
    <?php endif; ?>  
    </div><!-- /nc-wrap -->

    <?php
}