<?php
/*
  Plugin Name: Verde - Minimal Coming Soon Plugin
  Description: This plugin shows a 'Custom Coming Soon' page to all users who are not logged in however, the Site Administrators see the fully functional website with the applied theme and active plugins as well as a fully functional Dashboard.
  Author: CreaboxThemes
  Version: 3.0
  Author URI: http://themeforest.net/user/CreaboxThemes
  Email: admin@creabox.es
*/ 

require_once('framework/framework.php');
require_once('framework/init.php');

#################################################################################
# LOAD MODULES
#################################################################################
$verde_modules = array(
        'functions/coming_soon_page'
);

verde_load_modules($verde_modules);


if (verde_product_info('item_type') == 'Plugin') {
#################################################################################
# PLUGIN SETTINGS LINK
#################################################################################

    function verde_action_links($links, $file) {
        static $this_plugin;
        if (!$this_plugin)
            $this_plugin = plugin_basename(__FILE__);
        if ($file == $this_plugin) {
            $settings_link = '<a href="' . site_url('wp-admin/admin.php?page=') . verde_product_info('settings_page_slug') . '">' . 'Settings' . '</a>';
            $links = array_merge(array($settings_link), $links);
        }
        return $links;
    }

    add_filter('plugin_action_links', 'verde_action_links', 10, 2);
    /** LOCALIZATION ************************************************************* */
    load_plugin_textdomain('verde-plugin', true, dirname( plugin_basename( __FILE__ ) ) . '/languages/');
} else {
    /** LOCALIZATION ************************************************************* */
    load_theme_textdomain('verde-plugin', true, get_template_directory() . '/languages/');
}