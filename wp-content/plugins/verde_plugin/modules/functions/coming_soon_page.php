<?php

if (verde_top('plugin_status') == 'enable') {
    add_action('get_header', 'verde_coming_soon_page');
}

function verde_coming_soon_page() {
	
	function access(){
		$current_user = wp_get_current_user();
		if (is_user_logged_in()) {
			if (verde_top('site_access') == "enable") {
		   		echo '';
			} else {
		    	require_once(verde_product_info('product_dir') . '/themes/theme.php');
		        die();
		    }
		} else {
			require_once(verde_product_info('product_dir') . '/themes/theme.php');
		    die();
		}
	}
	
	function access_ip() {
		if (verde_top('date_ended') == 'disable') {
		    $date = verde_top('launch_date');
		    $today = date('Y-m-d h:i:s a');
		    if (strtotime($date) < strtotime($today)){ 
	    		echo '';
			} else {
			    access();
	    	}
		} else {
			access();
		}
	}
	
	$ip_array = verde_top('site_access_ip');
	$ip_user = $_SERVER['REMOTE_ADDR'];
	$pos = strpos($ip_array, $ip_user);
	if ( !is_user_logged_in() ) {
		if ( ($pos === false) ) {
			access_ip();
		} else {
			echo '';
		}
	} else {
		access_ip();
	}
	
}