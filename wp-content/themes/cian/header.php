<!DOCTYPE html>
<html <?php language_attributes(); ?> class="<?php echo Mobile_Detect::is_mobile_or_tablet() ? 'small-device' : 'large-device'; ?>">

	<head>
		<meta charset="<?php bloginfo('charset'); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		
		<?php
		$cian_favicon = cian_option( 'favicon' );
		if ( $cian_favicon ) {
			echo '<link rel="icon" href="'.cian_option( 'favicon' ).'" sizes="32x32" >';
		}
		?>

		<?php wp_head(); ?>
	</head>

	<?php $preloader_class = ''; if ( cian_option( 'enable_preloader', 1 ) ) $preloader_class .= 'js-preloader'; ?>

	<body <?php body_class( $preloader_class ); ?>>

		<?php
		$enable_slider_section = cian_page_option( 'enable_slider_section', null );
		$enable_parallax = cian_page_option( 'slider_section.0.enable_parallax', null );
		$background_overlay = cian_page_option( 'slider_section.0.background_overlay', null );
		$box_slides = cian_page_option( 'slider_section.0.slides', array() );
		$slider_interval = cian_page_option( 'slider_section.0.interval', 0 );
		$button_down = cian_page_option( 'slider_section.0.enable_button_down', 0 );
		
		include( get_template_directory() . '/menu.php');
		
		if ( $enable_parallax ) cian_parallax_script(); ?>

		<?php if ( $enable_slider_section ) : ?>

			<?php cian_caroufredsel_script(); ?>

			<section id="box" class="box-section section">

				<?php $has_video = false; ?>
				<ul class="section-background-slider caroufredsel js-caroufredsel <?php echo ( ! empty( $background_overlay ) ) ? $background_overlay : ''; ?>" data-caroufredsel="box-slider"<?php echo esc_attr(cian_data_printer(array('interval' => $slider_interval))); ?>>
					<?php foreach ( $box_slides as $i => $slide_id ) : ?>

						<?php
						/**
						 * Get Settings
						 */
						
						$media = cian_slide_option( 'background_media', 'image', $slide_id );
						$color_scheme = cian_slide_option( 'color_scheme', 'dark', $slide_id );
						$layout_slider = cian_slide_option( 'layout_slider', 'layout-0', $slide_id );
						$image_slide = cian_slide_option( 'image_slide', 'image', $slide_id );
						$title = cian_slide_option( 'title', '', $slide_id );
						$middle_text = cian_slide_option( 'text_rotator', '', $slide_id );
						$middle_text_interval = cian_slide_option( 'mt_interval', 5000, $slide_id );
						$button_text = cian_slide_option( 'button_text', '', $slide_id );
						$button_link = cian_slide_option( 'button_link', '#', $slide_id );
						$button_style = cian_slide_option( 'button_link_style', '#', $slide_id );
						$button_2_text = cian_slide_option( 'button_2_text', '', $slide_id );
						$button_2_link = cian_slide_option( 'button_2_link', '#', $slide_id );
						$button_2_style = cian_slide_option( 'button_2_link_style', '#', $slide_id );
						$css_animation = cian_slide_option( 'css_animation', '', $slide_id );

						// middle text interval normalization
						if( empty( $middle_text_interval ) ) $middle_text_interval = 5000;
						if( $middle_text_interval < 1000 ) $middle_text_interval = 1000;

						?>

						<?php
						/**
						 * Buffering Slide Content
						 */
						
						ob_start(); ?>
						<div class="box-text container">
							<div class="box-text-table">
									
								<?php if ( $layout_slider == 'layout-0' ): ?>
									 <div class="box-text-table-cell box-text-table-half text-center animate_css animated <?php echo esc_attr($css_animation); ?>">
								<?php elseif ( $layout_slider == 'layout-1' ): ?>
									<div class="box-text-table-cell box-text-table-half-center text-center animate_css animated <?php echo esc_attr($css_animation); ?>">
								<?php else: ?>
									<div class="box-text-table-cell box-text-table-half box-text-table-img text-center animate_css animated <?php echo esc_attr($css_animation); ?>">
										<img class="img-responsive" src="<?php echo $image_slide ?>" alt="<?php bloginfo( 'name' ); ?>" width="<?php echo cian_get_width_image($image_slide); ?>" height="<?php echo cian_get_height_image($image_slide); ?>" />
									</div>
									<div class="box-text-table-cell box-text-table-half text-center animate_css animated <?php echo esc_attr($css_animation); ?>">
								<?php endif; ?>
										
									<?php if ( !empty($title)): ?>
									<h2><?php echo cian_wp_kses_heading($title); ?></h2>
									<?php endif; ?>

									<?php $texts = preg_split( "/\\r\\n|\\r|\\n/", $middle_text ); ?>
									<?php if ( count( $texts ) > 1 ) : ?>
									<ul class="text-rotator" <?php echo cian_data_printer(array('interval' => $middle_text_interval)); ?>>
										<?php foreach ( $texts as $text ) : ?>
											<li><?php echo empty($title) ? '<h3>'.esc_html($text).'</h3>' : '<h4>'.esc_html($text).'</h4>'; ?></li>
										<?php endforeach; ?>
									</ul>
									<?php else : ?>
									<h4><?php echo esc_html($middle_text); ?></h4>
									<?php endif;?>
									
									<?php if ( !empty( $button_text ) || ! empty( $button_2_text ) ):?>
										<div class="box-buttons">
											<?php if ( !empty( $button_text ) ) : ?>
												<a href="<?php echo esc_url($button_link); ?>" class="btn <?php echo esc_attr(( $button_style == 'flat' ) ? 'btn-default' : 'btn-accent'); ?> js-anchor-link"><?php echo esc_html($button_text); ?></a>
											<?php endif; ?>
		
											<?php if ( !empty( $button_2_text ) ) : ?>
												<a href="<?php echo esc_url($button_2_link); ?>" class="btn <?php echo esc_attr(( $button_2_style == 'flat' ) ? 'btn-default' : 'btn-accent'); ?> js-anchor-link"><?php echo esc_html($button_2_text); ?></a>
											<?php endif; ?>
										</div>
									<?php endif; ?>
									
									</div>
								
								<?php if ( $layout_slider == 'layout-0' ): ?>
									<div class="box-text-table-cell box-text-table-half box-text-table-img text-center animate_css animated <?php echo esc_attr($css_animation); ?>">
										<img class="img-responsive" src="<?php echo $image_slide ?>" alt="<?php bloginfo( 'name' ); ?>" width="<?php echo cian_get_width_image($image_slide); ?>" height="<?php echo cian_get_height_image($image_slide); ?>" />
									</div>
								<?php endif; ?>
									
									
							</div>
						</div>
						<?php $slide_content = ob_get_clean(); ?>

						<?php
						/**
						 * Render Slide
						 */
						if ( $media == 'image' ) : ?>

							<?php
							$background_image = cian_slide_option( 'background_image', null, $slide_id );
							$background_repeat = cian_slide_option( 'background_repeat', '', $slide_id );
							?>
							<li class="<?php echo esc_attr($enable_parallax ? 'parallax-background js-parallax' : ''); ?> <?php echo "$color_scheme-scheme "; ?>" style="background-image: url(<?php echo esc_url($background_image); ?>); <?php echo esc_attr(cian_build_background_repeat_style( $background_repeat )); ?>" data-parallax="background">
								<?php echo $slide_content; ?>
							</li>

						<?php elseif ( $media == 'video' ) : ?>

							<?php
							$video_source = cian_slide_option( 'video_source', null, $slide_id );
							$video_source_webm = cian_slide_option( 'video_source_webm', null, $slide_id );
							$video_source_ogg = cian_slide_option( 'video_source_ogg', null, $slide_id );
							$video_poster = cian_slide_option( 'video_poster', null, $slide_id );
							$video_ratio = cian_slide_option( 'video_ratio', '16:9', $slide_id );
							$video_mute = cian_slide_option( 'video_mute', false, $slide_id );
							$video_id = cian_generate_id( 'video-background-' );
							$has_video = true;
							?>

							<?php if ( Mobile_Detect::is_mobile_or_tablet() ) : ?>
								<li class="<?php echo esc_attr($enable_parallax ? 'parallax-background js-parallax' : ''); ?> <?php echo "$color_scheme-scheme"; ?>" style="background-image: url(<?php echo esc_url($video_poster); ?>); background-size: cover; background-repeat: no-repeat; background-position: center center;" data-parallax="background">
									<?php echo $slide_content; ?>
								</li>
							<?php else : ?>
								<li class="<?php echo "$color_scheme-scheme"; ?>">

									<video id="<?php echo esc_attr($video_id); ?>" class="<?php echo esc_attr($enable_parallax ? 'parallax js-parallax' : ''); ?> video-background js-video-background" preload="none" loop="true" controls="false" volume=<?php echo esc_attr( $video_mute ? 0 : 1 );?> data-section="#box" data-ratio="<?php echo esc_attr($video_ratio); ?>" data-parallax="element">
										<?php if ( ! empty( $video_source ) ) : ?>
											<source src="<?php echo esc_url($video_source); ?>" type="video/mp4" />
										<?php endif; ?>
										
										<?php if ( ! empty( $video_source_webm ) ) : ?>
											<source src="<?php echo esc_url($video_source_webm); ?>" type="video/webm" />
										<?php endif; ?>
										
										<?php if ( ! empty( $video_source_ogg ) ) : ?>
											<source src="<?php echo esc_url($video_source_ogg); ?>" type="video/ogg" />
										<?php endif; ?>
									</video>
									<?php echo $slide_content; ?>
								</li>
							<?php endif; ?>

						<?php endif; ?>

					<?php endforeach; ?>
				</ul>

				<?php if ( $has_video ) : ?>
					<a href="#" class="video-volume-toggle <?php echo esc_attr($video_mute ? '' : 'volume-active');?>"></a>
				<?php endif; ?>

				<div class="section-background-pagination caroufredsel-pagination">
					<?php foreach ( $box_slides as $i => $slide_id ) : ?>
						<a href="#"></a>
					<?php endforeach; ?>
				</div>
				
				<?php if ($button_down == true):?>
				<button id="scroll-down" type="button">
					<img src="<?php echo CIAN_IMAGES?>/scroll-down.png" alt="Button to go down" />
				</button>
				<?php endif; ?>

			</section>
		<?php endif; ?>
		
		<div id="document" class="document">
			