<?php

class Widget_Cian_Icon_List extends WP_Widget {

	function __construct() {
		parent::__construct(
			'cian_icon_list',
			__( 'Cian: Icon List', 'cian' ),
			array( 'description' => __( 'Display Icon List Set', 'cian' ) )
		);

		add_action( 'save_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );
	}

	public function widget( $args, $instance ) {
		$cache = wp_cache_get( 'widget_cian_icon_list', 'widget' );

		if ( ! is_array( $cache ) ) $cache = array();

		if ( ! isset( $args['widget_id'] ) ) $args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		$title = apply_filters( 'widget_title', $instance['title'] );
		$items = $instance['items'];

		if ( empty( $items ) ) return;
		else $items = preg_split( "/\\r\\n|\\r|\\n/", $items );

		$dl = array();
		foreach ( $items as $item ) {
			if ( empty( $item ) ) continue;
			$item = preg_replace( "/(^)?(<br\s*\/?>\s*)+$/", "", $item );
			$arr = explode( ':' , $item, 2 );
			$arr = array_map( 'trim', $arr );
			$dl[ $arr[0] ] = $arr[1];
		}

		ob_start();

		echo $args['before_widget'];
		echo ( ! empty( $title ) ) ? $args['before_title'] . $title . $args['after_title'] : ''; ?>
		<ul class="dl-icon">
			<?php foreach ( $dl as $key => $value ) : ?>
				<li>
				<span class="<?php echo $key; ?>"></span>
				<?php echo $value; ?>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php echo $args['after_widget'];

		$cache[ $args['widget_id'] ] = ob_get_flush();
		
		wp_cache_set( 'widget_cian_icon_list', $cache, 'widget' );
	}

	public function form( $instance ) {
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$items = isset( $instance['items'] ) ? esc_attr( $instance['items'] ) : 
		'fa fa-globe: www.yourwebsite.com
fa fa-envelope-o: email@yourwebsite.com
fa fa-phone: +132 456 789';
		
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'cian' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'items' ); ?>"><?php _e( 'Items (line break separated):', 'cian' ); ?></label>
			<textarea class="widefat" id="<?php echo $this->get_field_id( 'items' ); ?>" name="<?php echo $this->get_field_name( 'items' ); ?>" rows="5"><?php echo $items; ?></textarea>
			<small><?php _e( cian_wp_kses_description('You can input <a href="http://fontawesome.io/cheatsheet/" target="_blank">FontAwesome</a> icons. And please do not forget to include the provider prefix. For example:<br/><br/>if you use "fa-globe", it should be "fa fa-globe".'), 'cian' ); ?></small>
		</p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['items'] = $new_instance['items'];
		$this->flush_widget_cache();

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete( 'cian_icon_list', 'widget' );
	}

}

// register
function register_widget_cian_icon_list() {
    register_widget( 'Widget_Cian_Icon_List' );
}
add_action( 'widgets_init', 'register_widget_cian_icon_list' );