<?php
/**
 * ======================================================================================
 * Includes
 * ======================================================================================
 */

/**
 * Include Vafpress Framework
 */
require_once 'vafpress-framework/bootstrap.php';
require_once 'admin/data-sources.php';

/**
 * Abstract Function to access theme_options values
 */
if ( ! function_exists( 'cian_option' ) ) {

	function cian_option( $key, $default = null ) {
		return vp_option( CIAN_OPTION_KEY . '.' . $key, $default );
	}

}
/**
 * Abstract Function to access page_options metabox values
 */
if ( !function_exists( 'cian_page_option' ) ) {

	function cian_page_option( $key, $default = null, $post_id = null ) {
		return vp_metabox( '_page_options' . '.' . $key, $default, $post_id );
	}

}
/**
 * Abstract Function to access post_options metabox values
 */
if ( ! function_exists( 'cian_post_option' ) ) {

	function cian_post_option( $key, $default = null, $post_id = null ) {
		return vp_metabox( '_post_options' . '.' . $key, $default, $post_id );
	}

} 
/**
 * Abstract Function to access portfolio_options metabox values
 */
if ( ! function_exists( 'cian_portfolio_option' ) ) {

	function cian_portfolio_option( $key, $default = null, $post_id = null ) {
		return vp_metabox( '_portfolio_options' . '.' . $key, $default, $post_id );
	}

} 
/**
 * Abstract Function to access slide_options metabox values
 */
if ( !function_exists( 'cian_slide_option' ) ) {

	function cian_slide_option( $key, $default = null, $post_id = null ) {
		return vp_metabox( '_slide_options' . '.' . $key, $default, $post_id );
	}

}

/**
 * Initialize Theme Options
 */
$cian_theme_option_scheme = CIAN_ADMIN_DIR . 'option/option.php';
global $cian_theme_options;
$cian_theme_options = new VP_Option( array(
	'is_dev_mode'           => false,
	'option_key'            => CIAN_OPTION_KEY,
	'page_slug'             => CIAN_OPTION_KEY,
	'template'              => $cian_theme_option_scheme,
	'menu_page' 			=> array(
        						'icon_url' => 'dashicons-admin-generic',
        						'position' => 50,
    ),	
	'use_auto_group_naming' => true,
	'use_exim_menu'         => true,
	'minimum_role'          => 'edit_theme_options',
	'layout'                => 'fixed',
	'page_title'            => __( 'Theme Options', 'cian' ),
	'menu_label'            => __( 'Theme Options', 'cian' ),
) );

/**
 * Initialize Metaboxes
 */
$cian_metabox_schemes = array_merge(
	array(
		CIAN_ADMIN_DIR . 'metabox/metabox_page_options.php',
		CIAN_ADMIN_DIR . 'metabox/metabox_slide_options.php',
		CIAN_ADMIN_DIR . 'metabox/metabox_post_options.php',
	),
	class_exists( 'VP_Portfolio' ) ? array( CIAN_ADMIN_DIR . 'metabox/metabox_portfolio_options.php' ) : array()
);
foreach ( $cian_metabox_schemes as $cian_metabox_scheme ) {
	new VP_Metabox( $cian_metabox_scheme );
}

/**
 * Custom CSS for Theme Options
 */
function cian_theme_options_custom_styles() {
	?>
	<style type="text/css">
		#pages_with_title_section .field label {
			display: block;
		}
	</style>
	<?php
}
add_action( 'admin_print_styles-appearance_page_cian_option', 'cian_theme_options_custom_styles' );