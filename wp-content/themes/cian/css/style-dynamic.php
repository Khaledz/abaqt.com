<?php
/**
 * Typography and colors
 */
$main_font    	   = cian_option( 'main_typograhpy' );
$accent_font  	   = cian_option( 'accent_typograhpy' );

/**
 * Colors
 */
$main_color    	   = cian_option( 'main_color' );
$heading_color     = cian_option( 'heading_color' );
$accent_color  	   = cian_option( 'accent_color' );
$light_color  	   = cian_option( 'light_color' );

/**
 * RGB colors
 */
$heading_color_rgb = cian_hex2rgb(cian_option( 'heading_color' ));
$main_color_rgb	   = cian_hex2rgb(cian_option( 'main_color' ));
$accent_color_rgb  = cian_hex2rgb(cian_option( 'accent_color' ));
$light_color_rgb  = cian_hex2rgb(cian_option( 'light_color' ));

?>

body {
	font-family: '<?php echo esc_attr($main_font); ?>';
}
.blog_title h3 strong, .wpb_cian_section_heading h3 strong, .box-section .box-text .box-text-table-cell h2 strong{
	font-family: '<?php echo esc_attr($accent_font); ?>';
}
a, body, .btn.btn-white, #jprePercentage, footer ul.footer-social a, .post-entry-meta strong, .post-pagination h5, .wpb_cian_blog_grid .blog-grid-post-content .post-date a {
	color: <?php echo esc_attr($main_color); ?>;
}
input[type="text"],input[type="text"]:focus,input[type="password"],input[type="password"]:focus,input[type="email"],input[type="email"]:focus,input[type="url"],input[type="url"]:focus,select,select:focus,textarea,table,textarea:focus,textarea.form-control,textarea.form-control:focus,textarea.wpcf7-form-control,textarea.wpcf7-form-control:focus,.pagination li > a,.wpb_cian_portfolio_grid .portfolio-grid-filter a{
	border: 1px solid <?php echo esc_attr($main_color); ?>;
}
.wpb_cian_service_block.horizontal-style .service-icon{
	border: 2px solid <?php echo esc_attr($main_color); ?>;
}
.btn.btn-main,table tr th,.box-section .caroufredsel-pagination a::before,.related-posts .grid-post-thumbnail,footer .footer-content .toTop,form.wpcf7-form input.wpcf7-submit,form.comment-form input[type="submit"],.widget.widget_calendar table caption,.navbar-default .navbar-toggle .icon-bar,.widget form.wpcf7-form .wpcf7-submit,.widget.widget_cian_recent_posts ul li .widget-post-thumbnail {
	background: <?php echo esc_attr($main_color); ?>;
}
.search-info,.wpb_cian_price_table .plan{
	background: <?php echo esc_attr("rgba(".$main_color_rgb[0].", ".$main_color_rgb[1].", ".$main_color_rgb[2].", 0.1)"); ?>;
}
.post-entry-meta .social-shares .post-share a{
	background: <?php echo esc_attr("rgba(".$main_color_rgb[0].", ".$main_color_rgb[1].", ".$main_color_rgb[2].", 0.25)"); ?>;
}
.header-horizontal-top,.navbar-default .navbar-header,.navbar-default .collapse.in,.wpb_cian_price_table .plan-heading,.wpb_cian_price_table .plan ul li,.blog-full-style article,.blog-loop article,.search-loop article{
	border-bottom: 1px solid <?php echo esc_attr("rgba(".$main_color_rgb[0].", ".$main_color_rgb[1].", ".$main_color_rgb[2].", 0.25)"); ?>;
}
.horizontalMenu .menu li ul,ul.comments-list li .comment-avatar img{
	border: 1px solid <?php echo esc_attr("rgba(".$main_color_rgb[0].", ".$main_color_rgb[1].", ".$main_color_rgb[2].", 0.25)"); ?>;
}
footer .footer-content hr{
	border-color: <?php echo esc_attr($main_color); ?>;
}
.comments,.comment-respond,.post-pagination,.related-posts,.widget.widget_nav_menu ul.sub-menu,.wpb_cian_blog_grid .blog-grid-post-content .post-date{
	border-top: 1px dashed <?php echo esc_attr("rgba(".$main_color_rgb[0].", ".$main_color_rgb[1].", ".$main_color_rgb[2].", 0.25)"); ?>;
}
footer.light.footer-top-border{
	border-top: 1px dashed <?php echo esc_attr("rgba(".$main_color_rgb[0].", ".$main_color_rgb[1].", ".$main_color_rgb[2].", 0.5)"); ?>;
}
.widget ul li{
	border-bottom: 1px dashed <?php echo esc_attr("rgba(".$main_color_rgb[0].", ".$main_color_rgb[1].", ".$main_color_rgb[2].", 0.25)"); ?>;
}
table tr{
	border-bottom: 1px solid <?php echo esc_attr($main_color); ?>;
}
table tr td{
	border-right: 1px solid <?php echo esc_attr($main_color); ?>;
}
blockquote{
	border-left: 3px solid <?php echo esc_attr("rgba(".$main_color_rgb[0].", ".$main_color_rgb[1].", ".$main_color_rgb[2].", 0.25)"); ?>;	
}
.post-entry-meta .categories ul li a{
	border-right: 1px solid <?php echo esc_attr("rgba(".$main_color_rgb[0].", ".$main_color_rgb[1].", ".$main_color_rgb[2].", 0.5)"); ?>;
}
footer.footer-widgets-content{
	border-top: 1px solid <?php echo esc_attr($main_color); ?>;
}
.box-section .caroufredsel-pagination a::after{
	box-shadow: inset 0 0 0 2px <?php echo esc_attr($main_color); ?>;
}
h1, h2, h3, h4, h5, h6,.btn.btn-default,.btn.btn-light,.btn.btn-white:hover,.horizontalMenu .menu li a,.blog_title h3,.post-title a,.search-info,.wpb_cian_section_heading h3,.widget h3,dl dt,.light-scheme,.post-pagination a,ul.comments-list li .comment-header,.comments h3.separator-title,.comment-respond h3.comment-reply-title,.widget_rss .widget-title a.rsswidget,.navbar-default .navbar-collapse ul li a,.wpb_cian_service_block.vertical-style h5,.wpb_cian_service_block.horizontal-style .service-icon,.wpb_cian_service_block.vertical-style .service-icon,.wpb_cian_blog_grid .blog-grid-post-content .post-category a,.wpb_cian_blog_grid .blog-grid-post-content .post-title a {
	color: <?php echo esc_attr($heading_color); ?>;
}
.btn.btn-default,.wpb_cian_service_block.vertical-style .service-icon{
	border: 1px solid <?php echo esc_attr($heading_color); ?>;
}
footer.dark,.dark-scheme.section,.post-thumbnail.blog-grid-post-link,.post-thumbnail.blog-grid-post-quote,.wpb_cian_blog_grid .blog-grid-post-thumbnail.blog-grid-post-link,.wpb_cian_blog_grid .blog-grid-post-thumbnail.blog-grid-post-quote,.post-thumbnail .bx-wrapper .bx-controls-direction a,.wpb_cian_blog_grid .blog-grid-post-thumbnail .bx-wrapper .bx-controls-direction a{
	background: <?php echo esc_attr($heading_color); ?>;
}
a:hover,strong,.widget_rss .rsswidget:hover,footer.dark .widget ul li a:hover,footer.dark .widget_tag_cloud a:hover,.horizontalMenu .menu li a:hover,.page-not-found .page-not-found-text,.post-thumbnail.blog-grid-post-quote a:hover,.post-thumbnail.blog-grid-post-link a:hover,.navbar-default .navbar-collapse ul li a:hover,.wpb_cian_mailchimp_newsletter form label.valid,.wpb_cian_service_block.vertical-style:hover h5,.wpb_cian_twitter .twitter-content .icon-twitter,.box-section .caroufredsel-pagination a.selected,footer.dark .widget_rotatingtweets_widget a:hover,footer.dark .widget_rss .widget-title a.rsswidget:hover,.wpb_cian_blog_grid .blog-grid-post-content .post-date a:hover,.wpb_cian_blog_grid .blog-grid-post-content .post-category a:hover,.wpb_cian_blog_grid .blog-grid-post-content .post-title a:hover,.wpb_cian_blog_grid .blog-grid-post-thumbnail.blog-grid-post-link h4 a:hover, .wpb_cian_blog_grid .blog-grid-post-thumbnail.blog-grid-post-quote h3 a:hover,.wpb_cian_blog_grid .blog-grid-post-thumbnail.blog-grid-post-quote h5 a:hover,.wpb_cian_service_block.horizontal-style:hover .service-heading  {
	color: <?php echo esc_attr($accent_color); ?>;
}
.blog_title span.line,.wpb_cian_section_heading span.line{
	border-bottom: 2px solid <?php echo esc_attr($accent_color); ?>;
}
.box-section .caroufredsel-pagination a.selected::after{
	box-shadow: inset 0 0 0 2px <?php echo esc_attr($accent_color); ?>;
}
.widget h3,footer .footer-widgets h3,.comments h3.separator-title,.comment-respond h3.comment-reply-title,.related-posts h3.separator-title{
	border-left: 2px solid <?php echo esc_attr($accent_color); ?>;
}
#jpreBar,.btn.btn-accent:hover,.btn.btn-main:hover,.btn.btn-light:hover,.btn.btn-default:hover,footer .footer-content .toTop:hover,.wpb_cian_quotes_carousel .icon-title,.navbar .navbar-toggle:hover,.pagination li.active >a,.pagination li > a:hover,.wpb_cian_portfolio_grid .portfolio-grid-filter a:hover,.wpb_cian_portfolio_grid .portfolio-grid-filter a.active,.widget form.wpcf7-form .wpcf7-submit:hover,form.wpcf7-form input.wpcf7-submit:hover,footer ul.footer-social a:hover,form.comment-form input[type="submit"]:hover,.wpb_cian_price_table .plan-heading .circle-price,.wpb_cian_service_block.vertical-style:hover .service-icon,.wpb_cian_service_block.horizontal-style:hover .service-icon,.post-thumbnail .bx-wrapper .bx-controls-direction a:hover,.wpb_cian_blog_grid .blog-grid-post-thumbnail .bx-wrapper .bx-controls-direction a:hover {
	background: <?php echo esc_attr($accent_color); ?>;
}
.btn.btn-default:hover,.pagination li.active >a,.pagination li > a:hover,.box-section .dark-scheme .box-buttons .btn.btn-default:hover,.wpb_cian_portfolio_grid .portfolio-grid-filter a:hover,.wpb_cian_portfolio_grid .portfolio-grid-filter a.active,.wpb_cian_service_block.vertical-style:hover .service-icon,.wpb_cian_service_block.horizontal-style:hover .service-icon{
	border: 1px solid <?php echo esc_attr($accent_color); ?>;
}
.btn.btn-accent {
	background: <?php echo esc_attr("rgba(".$accent_color_rgb[0].", ".$accent_color_rgb[1].", ".$accent_color_rgb[2].", 0.75)"); ?>;
}
.wpb_cian_portfolio_grid .portfolio-grid-loop .portfolio-grid-post-background{
	background: <?php echo esc_attr("rgba(".$accent_color_rgb[0].", ".$accent_color_rgb[1].", ".$accent_color_rgb[2].", 0.25)"); ?>;
}
.btn.btn-main,.btn.btn-accent,.btn.btn-light:hover,.pagination li > a:hover,.pagination li.active >a,.btn.btn-default:hover,footer.dark,footer.dark .footer-widgets h3,footer.dark .widget ul li a,footer.dark .widget_tag_cloud a,footer.dark .widget .search-form input,footer.dark .widget_rotatingtweets_widget a,footer.dark .widget_rss .widget-title a.rsswidget,footer .footer-content .toTop,.dark-scheme,.dark-scheme h2,.dark-scheme h3,.dark-scheme h4,.lg-sub-html h4,.dark-scheme a,table tr th,.related-posts .grid-post-thumbnail span,.dark-scheme strong,.dark-scheme input[type="text"],.dark-scheme input[type="text"]:focus,.dark-scheme input[type="password"],.dark-scheme input[type="password"].form-control,.dark-scheme input[type="password"]:focus,.dark-scheme input[type="email"],.dark-scheme input[type="email"]:focus,.dark-scheme input[type="url"],.dark-scheme input[type="url"]:focus,.dark-scheme select,.dark-scheme select:focus,.dark-scheme textarea,.dark-scheme textarea:focus,.dark-scheme textarea.form-control,.dark-scheme textarea.form-control:focus,.dark-scheme textarea.wpcf7-form-control,.dark-scheme textarea.wpcf7-form-control:focus,.box-section .dark-scheme .box-buttons .btn.btn-default,footer ul.footer-social a:hover,form.wpcf7-form input.wpcf7-submit,form.comment-form input[type="submit"],.wpb_cian_quotes_carousel .icon-title,.widget.widget_calendar table caption,.post-thumbnail.blog-grid-post-link a,.post-thumbnail.blog-grid-post-quote,.post-thumbnail.blog-grid-post-quote a,.box-section .caroufredsel-pagination a,.widget.widget_cian_recent_posts ul li .widget-post-thumbnail,.wpb_cian_blog_grid .blog-grid-post-thumbnail.blog-grid-post-link a,.wpb_cian_blog_grid .blog-grid-post-thumbnail.blog-grid-post-quote a,.wpb_cian_price_table .plan-heading .circle-price,.post-entry-meta .social-shares .post-share a:hover,.wpb_cian_service_block.vertical-style:hover .service-icon,.wpb_cian_service_block.horizontal-style:hover .service-icon,.post-thumbnail .bx-wrapper .bx-controls-direction a,.wpb_cian_portfolio_grid .portfolio-grid-filter a.active,.wpb_cian_portfolio_grid .portfolio-grid-filter a:hover,.wpb_cian_portfolio_grid .portfolio-grid-loop .portfolio-grid-post-text h3,.wpb_cian_portfolio_grid .portfolio-grid-loop .portfolio-grid-post-text p,.wpb_cian_blog_grid .blog-grid-post-thumbnail .bx-wrapper .bx-controls-direction a{
	color: <?php echo esc_attr($light_color); ?>;
}
#jpreOverlay,.btn.btn-light,.light-scheme.section,.header-horizontal-top,.horizontalMenu .menu li ul li,.navbar-default .navbar-collapse,.navbar .navbar-toggle:hover .icon-bar,.wpb_cian_blog_grid .blog-grid-post-content{
	background: <?php echo esc_attr($light_color); ?>;
}
footer.dark .widget ul li{
	border-bottom: 1px dashed <?php echo esc_attr("rgba(".$light_color_rgb[0].", ".$light_color_rgb[1].", ".$light_color_rgb[2].", 0.25)"); ?>;
}
.dark-scheme input[type="text"],.dark-scheme input[type="text"]:focus,.dark-scheme input[type="password"],.dark-scheme input[type="password"]:focus,.dark-scheme input[type="email"],.dark-scheme input[type="email"]:focus,.dark-scheme input[type="url"],.dark-scheme input[type="url"]:focus,.dark-scheme select,.dark-scheme select:focus,.dark-scheme textarea,.dark-scheme textarea:focus,.dark-scheme textarea.form-control,.dark-scheme textarea.form-control:focus,.dark-scheme textarea.wpcf7-form-control,.dark-scheme textarea.wpcf7-form-control:focus,.dark-scheme .pagination li > a,.dark-scheme .wpb_cian_portfolio_grid .portfolio-grid-filter a,.box-section .dark-scheme .box-buttons .btn.btn-default{
	border: 1px solid <?php echo esc_attr($light_color); ?>;
}