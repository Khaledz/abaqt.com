<?php get_header(); ?>

<section id="content" class="content-section section">
	<div class="container">

		<div class="page-not-found">
			<p class="page-not-found-text"><?php esc_html_e( '404', 'cian' ); ?></p>
			<h3><?php esc_html_e( 'Page does not exist or some other error occured', 'cian' ); ?></h2>
			<h4><?php esc_html_e( 'Try to use searching', 'cian' ); ?></h3>

			<div class="search-form row">
				<?php the_widget( 'WP_Widget_Search', array(), array(
					'before_widget' => '<div>',
					'after_widget' => '</div>',
				) ); ?>
			</div>
		</div>

	</div>
</section>

<?php if ( cian_option( 'enable_preloader', 1 ) ) : ?>
	<div id="jpreContent">
		<div id="loading-center"></div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>