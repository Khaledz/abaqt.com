	<article id="post-<?php the_ID(); ?>" <?php post_class( 'content-post' ); ?>>

		<?php if ( has_post_format('audio') ) : ?>
						
			<div class="post-thumbnail">
				<?php $cb_audio = get_post_meta( get_the_ID(), '_format_audio_embed', true ); ?>
				<?php if(wp_oembed_get( $cb_audio )) : ?>
					<?php echo wp_oembed_get($cb_audio); ?>
				<?php else : ?>
					<?php echo cian_wp_kses_post($cb_audio); ?>
				<?php endif; ?>
			</div>
		
		<?php elseif(has_post_format('gallery')) : ?>
			
			<?php cian_bxslider_style_script(); ?>
			<?php $images = get_post_meta( get_the_ID(), '_format_gallery_images', true ); ?>
			<?php if($images) : ?>
			<div class="post-thumbnail">
				<ul class="bxslider">
				<?php foreach($images as $image) : ?>
					<?php $the_image = wp_get_attachment_image_src($image, 'full'); ?> 
					<li><img src="<?php echo esc_url($the_image[0]); ?>" alt="<?php echo get_the_title(); ?>" width="<?php echo esc_attr($the_image[1]); ?>" height="<?php echo esc_attr($the_image[2]); ?>" /></li>
				<?php endforeach; ?>
				</ul>
			</div>
			<?php endif; ?>
		
		<?php elseif(has_post_format('video')) : ?>
		
			<div class="post-thumbnail">
				<?php $cb_video = get_post_meta( get_the_ID(), '_format_video_embed', true ); ?>
				<?php if(wp_oembed_get( $cb_video )) : ?>
					<?php echo wp_oembed_get($cb_video); ?>
				<?php else : ?>
					<?php echo cian_wp_kses_post($cb_video); ?>
				<?php endif; ?>
			</div>
			
		<?php elseif(has_post_format('link')) : ?>
			<?php $link = get_post_meta( get_the_ID(), '_format_link_url', true); ?>
			<?php if ( !empty($link)) :?>
			<div class="post-thumbnail blog-grid-post-link">
				<h3><a class="link" href="<?php echo esc_url($link);?>" title="<?php echo esc_attr( get_the_title() ); ?>"><?php echo esc_url($link); ?></a></h3>
			</div>
			<?php endif; ?>
			
		<?php elseif(has_post_format('quote')) : ?>
			<div class="post-thumbnail blog-grid-post-quote">
				<h3><a href="<?php the_permalink(); ?>"><?php the_content(); ?></a></h3>
				<?php $quote_author = get_post_meta( get_the_ID(), '_format_quote_source_name', true); ?>
				<?php $quote_url = get_post_meta( get_the_ID(), '_format_quote_source_url', true); ?>
				<h5 class="quote-author"><a href="<?php the_permalink(); ?>" target="_blank"><?php echo esc_html($quote_author); ?></a></h5>
			</div>
			
		<?php else: ?>
		
			<?php if ( has_post_thumbnail() ) : ?>
				<div class="post-thumbnail">
					<a href="<?php the_permalink(); ?>">
						<?php $image_data = wp_get_attachment_metadata(get_post_thumbnail_id());?>
						<img src="<?php echo esc_url(cian_aq_resize( get_post_thumbnail_id(), null, null, true, true )); ?>" alt="<?php echo get_the_title(); ?>" width="<?php echo esc_attr($image_data['width']);?>" height="<?php echo esc_attr($image_data['height']);?>" />
					</a>
				</div>
			<?php endif; ?>
		
		<?php endif; ?>

		<div class="post-meta-date">
			<a href="<?php the_permalink(); ?>"><?php echo get_the_date(); ?></a>
		</div>
		
		<?php if ( !has_post_format('quote') ) : ?>
		<h3 class="post-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
		</h3>
		<?php endif; ?>

		<div class="post-content">
			<?php if(!has_post_format('quote')) :?>
			<?php echo '<p>'.cian_get_my_excerpt(70).'</p>'?>
			<?php wp_link_pages(); ?>
			<?php endif; ?>
		</div>
		
		<div class="post-entry-meta">
			<div class="social-shares">
				<?php 
				$blog_social_media_links = cian_option( 'social_share_links', array() );
				if ( ! empty( $blog_social_media_links ) ): ?>
					<p><?php echo cian_wp_kses_heading('<strong>Share: </strong>','cian')?></p>
					<?php echo cian_social_shares(); ?>
				<?php endif; ?>
			</div>
			<div class="author">
				<p><?php echo cian_wp_kses_heading('<strong>By: </strong>','cian')?> <?php the_author_posts_link(); ?></p>
			</div>
			<div class="categories">
				<ul>
					<li><?php echo cian_wp_kses_heading('<strong>Category: </strong>','cian')?></li>
					<li><?php the_category( ); ?></li>
				</ul>
			</div>
		</div>

	</article>