<?php
/**
 * Template Name: Blog with Sidebar
 * Description: This page template works as blog post index page with Sidebar enabled.
 * 
 * @package CIAN
 * @since CIAN 2.0
 */ 

get_header();

global $wp_query, $paged, $page; $temp = $wp_query;

if ( get_query_var('paged') )
	$paged = get_query_var('paged');
elseif ( get_query_var('page') )
	$paged = get_query_var('page');
else
	$paged = 1;

$wp_query = new WP_Query( array(
	'post_type' 	=> 'post',
	'orderby'       => 'date',
	'paged'     	=> max( $paged, $page ),
	'post_status'   => 'publish'
) );

?>

<section id="content" class="content-section section">
	<div class="container container-table">
		<?php if ( cian_option( 'enable_blog_title') == true ) : ?>
		<div class="row">
			<div class="blog_title">
				<h3><?php echo cian_wp_kses_heading(cian_option( 'blog_title')); ?></h3>
				<span class="line"></span>
			</div>
		</div>	
		<?php endif; ?>
		<div class="row">
			<div class="col-md-9 main-section" role="main">
			<?php 
			if (have_posts()) :	
				if (cian_option( 'blog_style') == 'full'){
					echo '<div class="blog-loop">';
					while (have_posts()) : the_post();
						get_template_part( 'loop-'.cian_option( 'blog_style') ); 
					endwhile;
					echo '</div>';
				} else { 
					cian_isotope_script();
					?>
					<div class="wpb_cian_blog_grid wpb_content_element js-isotope-blog-grid">
						<div class="wpb_wrapper">
							<div class="blog-grid-loop row">
								<div class="blog-grid-sizer"></div>
								<?php 
								while (have_posts()) : the_post();
									get_template_part( 'loop-'.cian_option( 'blog_style') ); 
								endwhile;
								?>
							</div>
						</div>
					</div>
				<?php 
				}
			else : 
				get_template_part('no-results');
			endif;	
			cian_numeric_posts_nav(); 
			wp_reset_postdata();
			?>
				
			</div>
			
			<?php get_sidebar(); ?>

		</div>
	</div>
</section>

<?php if ( cian_option( 'enable_preloader', 1 ) ) : ?>
	<div id="jpreContent">
		<div id="loading-center"></div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>