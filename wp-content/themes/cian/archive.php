<?php
/**
 * The template for displaying archive pages
 * 
 * @package CIAN
 * @since CIAN 2.0
 */ 

get_header(); 

$show_sidebar = cian_sidebar_enable(); ?>

<section id="content" class="content-section section">
	<div class="container container-table">
		<div class="row">
			<div class="blog_title">
				<?php
					the_archive_title( '<h3 class="page-title">', '</h3>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
				<span class="line"></span>
			</div>
		</div>	
		<div class="row">
			<div class="<?php if ( $show_sidebar ) { echo 'col-md-9 '; } else { echo 'col-md-12 '; } ?>main-section" role="main">
			<?php 
			if (have_posts()) :	
				if (cian_option( 'archive_style') == 'full'){
					echo '<div class="blog-full-style">';
					while (have_posts()) : the_post();
						get_template_part( 'loop-'.cian_option( 'archive_style') ); 
					endwhile;
					echo '</div>';
				} else { 
					cian_isotope_script();
					?>
					<div class="wpb_cian_blog_grid wpb_content_element js-isotope-blog-grid">
						<div class="wpb_wrapper">
							<div class="blog-grid-loop row">
								<div class="blog-grid-sizer"></div>
								<?php 
								while (have_posts()) : the_post();
									get_template_part( 'loop-'.cian_option( 'archive_style') ); 
								endwhile;
								?>
							</div>
						</div>
					</div>
				<?php 
				}
			else :  
				get_template_part('no-results');
			endif;	
			cian_numeric_posts_nav(); 
			wp_reset_postdata();
			?>
				
			</div>
			
		<?php if ( $show_sidebar ): ?>
		<?php get_sidebar(); ?>
		<?php endif; ?>

		</div>
	</div>
</section>

<?php if ( cian_option( 'enable_preloader', 1 ) ) : ?>
	<div id="jpreContent">
		<div id="loading-center"></div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>