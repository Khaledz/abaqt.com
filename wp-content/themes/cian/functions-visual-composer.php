<?php

function cian_custom_visual_composer() {

	$add_css_animation = array(
		'type' => 'dropdown',
		'heading' => esc_html__( 'CSS Animation', 'cian' ),
		'param_name' => 'css_animation',
		'admin_label' => true,
		'value' => array_merge( array(
			esc_html__( 'No', 'cian' ) => '',
			esc_html__( 'Top to bottom', 'cian' ) => 'top-to-bottom',
			esc_html__( 'Bottom to top', 'cian' ) => 'bottom-to-top',
			esc_html__( 'Left to right', 'cian' ) => 'left-to-right',
			esc_html__( 'Right to left', 'cian' ) => 'right-to-left',
			esc_html__( 'Appear from center', 'cian' ) => 'appear',
		), cian_get_animate_css() ),
		'save_always' => true,
		'description' => esc_html__( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'cian' ),
	);

	$add_icon = array(
		'type' => 'dropdown',
		'holder' => 'div',
		'heading' => esc_html__( 'Icon', 'cian' ),
		'param_name' => 'icon',
		'value' => array_merge( array(
			esc_html__( 'No Icon', 'cian' ) => '',
		), cian_get_icons() ),
		'save_always' => true,
	);

	$vc_column_text_css_animation = WPBMap::getParam( 'vc_column_text', 'css_animation' );
	$vc_column_text_css_animation = $add_css_animation;
	WPBMap::mutateParam( 'vc_column_text', $vc_column_text_css_animation );

	$vc_message_css_animation = WPBMap::getParam( 'vc_message', 'css_animation' );
	$vc_message_css_animation = $add_css_animation;
	WPBMap::mutateParam( 'vc_message', $vc_message_css_animation );

	$vc_toggle_css_animation = WPBMap::getParam( 'vc_toggle', 'css_animation' );
	$vc_toggle_css_animation = $add_css_animation;
	WPBMap::mutateParam( 'vc_toggle', $vc_toggle_css_animation );

	$vc_single_image_css_animation = WPBMap::getParam( 'vc_single_image', 'css_animation' );
	$vc_single_image_css_animation = $add_css_animation;
	WPBMap::mutateParam( 'vc_single_image', $vc_single_image_css_animation );

	$vc_cta_button_css_animation = WPBMap::getParam( 'vc_cta_button', 'css_animation' );
	$vc_cta_button_css_animation = $add_css_animation;
	WPBMap::mutateParam( 'vc_cta_button', $vc_cta_button_css_animation );

	$vc_cta_button2_css_animation = WPBMap::getParam( 'vc_cta_button2', 'css_animation' );
	$vc_cta_button2_css_animation = $add_css_animation;
	WPBMap::mutateParam( 'vc_cta_button2', $vc_cta_button2_css_animation );
	
	/**
	 * Add Content Element: Featured Images
	 */
	require_once( get_template_directory() . '/vc_extends/cian_featured_images.php' );
	vc_map( array(
		'name' => esc_html__( 'Featured Images', 'cian' ),
		'base' => 'vc_cian_featured_images',
		'icon' => 'cian_element',			
		'category' => esc_html__( 'Content', 'cian' ),
		'params' => array(
			array(
				'type' => 'dropdown',
				'holder' => 'div',
				'heading' => esc_html__( 'Number of featured images', 'cian' ),
				'param_name' => 'number_featured_images',
				'value' => array(
					'1' => 1,
					'2' => 2,
					'3' => 3,
				),
				'description'	=> esc_html__('Select the number of featured images you want to show', 'cian'),
				'save_always' => true,
			),
			array(
				'type' => 'attach_image',
				'holder' => 'div',
				'heading' => esc_html__( 'Select the left image', 'cian' ),
				'param_name' => 'left_image',
				'dependency' => array( 'element' => 'number_featured_images', 'value' => array( '2', '3' ) ),	
			),
			array(
				'type' => 'attach_image',
				'holder' => 'div',
				'heading' => esc_html__( 'Select the center image', 'cian' ),
				'param_name' => 'center_image',
				'dependency' => array( 'element' => 'number_featured_images', 'value' => array( '1', '3' ) ),
			),
			array(
				'type' => 'attach_image',
				'holder' => 'div',
				'heading' => esc_html__( 'Select the right image', 'cian' ),
				'param_name' => 'right_image',
				'dependency' => array( 'element' => 'number_featured_images', 'value' => array( '2', '3' ) ),
			),
			$add_css_animation,
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'cian' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cian' ),
			),
		),
	) );

	/**
	 * Add Content Element: Blog Grid
	 */
	require_once( get_template_directory() . '/vc_extends/cian_blog_grid.php' );
	vc_map( array(
		'name' => esc_html__( 'Blog grid', 'cian' ),
		'base' => 'vc_cian_blog_grid',
		'icon' => 'cian_element',			
		'category' => esc_html__( 'Content', 'cian' ),
		'params' => array(
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Number of items', 'cian' ),
				'param_name' => 'count',
				'value' => 8,
				'description' => esc_html__( '-1 to show all', 'cian' ),
			),
			array(
				'type' => 'checkbox',
				'holder' => 'div',
				'heading' => esc_html__( 'Enable Pagination', 'cian' ),
				'param_name' => 'blog_pagination',
				'save_always' => true,
			),
			array(
				'type' => 'textarea',
				'holder' => 'div',
				'heading' => esc_html__( 'Only shows specified categori(es)', 'cian' ),
				'param_name' => 'category',
				'description' => esc_html__( 'Comma separated SLUG of the categori(es).', 'cian' ),
			),
			$add_css_animation,
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'cian' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cian' ),
			),
		),
	) );
	
	/**
	 * Add Content Element: Mailchimp Newsletter
	 */
	require_once( get_template_directory() . '/vc_extends/cian_mailchimp_newsletter.php' );
	vc_map( array(
		'name' => esc_html__( 'Mailchimp newsletter', 'cian' ),
		'base' => 'vc_cian_mailchimp_newsletter',
		'icon' => 'cian_element',			
		'category' => esc_html__( 'Content', 'cian' ),
		'params' => array(
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Value urlForm', 'cian' ),
				'param_name' => 'mailchimp_urlform',
				'description' => cian_wp_kses_description(__( 'You have to get the value urlForm from your mailchimp signup form. If you don\'t know how to get this value, follow <a href="http://kb.mailchimp.com/lists/signup-forms/host-your-own-signup-forms" target="_blank">this tutorial</a>.', 'cian' )),
				'save_always' => true,
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Value u', 'cian' ),
				'param_name' => 'mailchimp_u',
				'description' => cian_wp_kses_description(__( 'You have to get the value u from your mailchimp signup form. If you don\'t know how to get this value, follow <a href="http://kb.mailchimp.com/lists/signup-forms/host-your-own-signup-forms" target="_blank">this tutorial</a>.', 'cian' )),
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Value id', 'cian' ),
				'param_name' => 'mailchimp_id',
				'description' => cian_wp_kses_description(__( 'You have to get the value id from your mailchimp signup form. If you don\'t know how to get this value, follow <a href="http://kb.mailchimp.com/lists/signup-forms/host-your-own-signup-forms" target="_blank">this tutorial</a>.', 'cian' )),
			),
			$add_css_animation,
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'cian' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cian' ),
			),
		),
	) );
	
	/**
	 * Add Content Element: Price table
	 */
	require_once( get_template_directory() . '/vc_extends/cian_price_table.php' );
	vc_map( array(
		'name' => esc_html__( 'Price Table', 'cian' ),
		'base' => 'vc_cian_price_table',
		'category' => esc_html__( 'Content', 'cian' ),
		'icon' => 'cian_element',
		'params' => array(
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Title table', 'cian' ),
				'param_name' => 'title',
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Subtitle table', 'cian' ),
				'param_name' => 'subtitle',
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Price', 'cian' ),
				'param_name' => 'price',
			),
			array(
				'type' => 'textarea',
				'holder' => 'div',
				'heading' => esc_html__( 'Details', 'cian' ),
				'description' => esc_html__( 'Line break separated.', 'cian' ),
				'param_name' => 'details',
				'value' => esc_html__( "One Person|15 Minute Session|10 Digital Images|Photo release", 'cian' ),
			),
			array(
				'type' => 'checkbox',
				'holder' => 'div',
				'heading' => esc_html__( 'Enable button', 'cian' ),
				'param_name' => 'button',
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Button text', 'cian' ),
				'param_name' => 'button_text',
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Button url', 'cian' ),
				'param_name' => 'button_url',
			),
			$add_css_animation,
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'cian' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cian' ),
			),
		),
	) );

	/**
	 * Add Content Element: Button
	 */
	require_once( get_template_directory() . '/vc_extends/cian_button.php' );
	vc_map( array(
		'name' => esc_html__( 'Button', 'cian' ),
		'base' => 'vc_cian_button',
		'icon' => 'cian_element',			
		'category' => esc_html__( 'Content', 'cian' ),
		'params' => array(
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Text', 'cian' ),
				'param_name' => 'text',
				'value' => esc_html__( 'Anchor Text', 'cian' ),
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Link URL', 'cian' ),
				'param_name' => 'link',
				'value' => '#',
			),
			$add_icon,
			array(
				'type' => 'dropdown',
				'holder' => 'div',
				'heading' => esc_html__( 'Style', 'cian' ),
				'param_name' => 'style',
				'value' => array(
					esc_html__( 'default', 'cian' ) => 'btn-default',
					esc_html__( 'main', 'cian' ) => 'btn-main',
					esc_html__( 'accent', 'cian' ) => 'btn-accent',
					esc_html__( 'light', 'cian' ) => 'btn-light',
					esc_html__( 'success', 'cian' ) => 'btn-success',
					esc_html__( 'info', 'cian' ) => 'btn-info',
					esc_html__( 'warning', 'cian' ) => 'btn-warning',
					esc_html__( 'danger', 'cian' ) => 'btn-danger',
				),
				'save_always' => true,
			),
			array(
				'type' => 'dropdown',
				'holder' => 'div',
				'heading' => esc_html__( 'Size', 'cian' ),
				'param_name' => 'size',
				'value' => array(
					esc_html__( 'normal', 'cian' ) => '',
					esc_html__( 'large', 'cian' ) => 'btn-lg',
					esc_html__( 'small', 'cian' ) => 'btn-sm',
					esc_html__( 'xs', 'cian' ) => 'btn-xs',
				),
				'save_always' => true,
			),
			$add_css_animation,
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'cian' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cian' ),
			),
		),
	) );

	/**
	 * Add Content Element: Portfolio Grid
	 */
	if ( class_exists( 'Portfolio_Post_Type' ) ) {

		require_once( get_template_directory() . '/vc_extends/cian_portfolio_grid.php' );
		vc_map( array(
			'name' => esc_html__( 'Portfolio grid', 'cian' ),
			'base' => 'vc_cian_portfolio_grid',
			'icon' => 'cian_element',				
			'category' => esc_html__( 'Content', 'cian' ),
			'params' => array(
				array(
					'type' => 'textfield',
					'holder' => 'div',
					'heading' => esc_html__( 'Number of items', 'cian' ),
					'param_name' => 'count',
					'value' => 9,
					'description' => esc_html__( '-1 to show all', 'cian' ),
				),
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'heading' => esc_html__( 'Enable Category Filter', 'cian' ),
					'param_name' => 'filter',
					'value' => array(
						esc_html__( 'true', 'cian' ) => 'true',
						esc_html__( 'false', 'cian' ) => 'false',
					),
				),
				array(
					'type' => 'textarea',
					'holder' => 'div',
					'heading' => esc_html__( 'Only shows specified categori(es)', 'cian' ),
					'param_name' => 'category',
					'description' => esc_html__( 'Comma separated SLUG of the categori(es).', 'cian' ),
				),
				$add_css_animation,
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Extra class name', 'cian' ),
					'param_name' => 'el_class',
					'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cian' ),
				),
			),
		) );

	}	
	
	/**
	 * Add Content Element: Quotes Carousel
	 */
	require_once( get_template_directory() . '/vc_extends/cian_quotes_carousel.php' );
	require_once( get_template_directory() . '/vc_extends/cian_quote.php' );
	vc_map( array(
		'name' => esc_html__( 'Quotes carousel', 'cian' ),
		'base' => 'vc_cian_quotes_carousel',
		'icon' => 'cian_element',
		'admin_enqueue_js' => array( get_template_directory_uri() . '/vc_extends/assets/js/vc-cian-quotes-carousel.js' ),
		'admin_enqueue_css' => array( get_template_directory_uri() . '/vc_extends/assets/css/quotes-carousel.css' ),
		'category' => esc_html__( 'Content', 'cian' ),
		'show_settings_on_create' => true,
		'is_container' => true,
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Auto Rotate Duration (mili seconds)', 'cian' ),
				'param_name' => 'auto_rotate',
				'description' => esc_html__( 'Fill 0 to disable', 'cian' ),
				'value' => 0,
			),
			$add_css_animation,
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'cian' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cian' ),
			),
		),
		'custom_markup' => '
			<div class="wpb_holder clearfix vc_container_for_children">
				%content%
			</div>
			<div class="tab_controls">
				<button class="add_tab" title="' . esc_html__( 'Add quote', 'cian' ) . '">' . esc_html__( 'Add quote', 'cian' ) . '</button>
			</div>
		',
		'default_content' => '
			[vc_cian_quote cite="John Doe - Company, Inc."]' . esc_html__( '<p>This is quote content. Click edit button to change this text.</p>', 'cian' ) . '[/vc_cian_quote]
		',
		'js_view' => 'CianQuotesCarousel',
	) );
	vc_map( array(
		'name' => esc_html__( 'Quote', 'cian' ),
		'base' => 'vc_cian_quote',
		'icon' => 'cian_element',
		'content_element' => false,
		'params' => array(
			array(
				'type' => 'textarea_html',
				'holder' => 'div',
				'heading' => esc_html__( 'Content', 'cian' ),
				'param_name' => 'content',
				'value' => esc_html__( 'This is quote content. Click edit button to change this text.', 'cian' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Cite', 'cian' ),
				'param_name' => 'cite',
				'description' => esc_html__( 'Cite.', 'cian' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'cian' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cian' ),
			),
		),
	) );

	/**
	 * Add Content Element: Section Heading
	 */
	require_once( get_template_directory() . '/vc_extends/cian_section_heading.php' );
	vc_map( array(
		'name' => esc_html__( 'Section heading', 'cian' ),
		'base' => 'vc_cian_section_heading',
		'icon' => 'cian_element',
		'category' => esc_html__( 'Content', 'cian' ),
		'params' => array(
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Small Heading', 'cian' ),
				'param_name' => 'small_heading',
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Heading', 'cian' ),
				'param_name' => 'heading',
			),
			array(
				'type' => 'textarea',
				'holder' => 'div',
				'heading' => esc_html__( 'Text', 'cian' ),
				'param_name' => 'text',
			),
			$add_css_animation,
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'cian' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cian' ),
			),
		),
	) );
	
	/**
	 * Add Content Element: Twitter 
	 */
	require_once( get_template_directory() . '/vc_extends/cian_twitter.php' );
	vc_map( array(
		'name' => esc_html__( 'Twitter feed', 'cian' ),
		'base' => 'vc_cian_twitter',
		'icon' => 'cian_element',
		'category' => esc_html__( 'Content', 'cian' ),
		'params' => array(
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Shortcode', 'cian' ),
				'param_name' => 'shortcode',
				'description' => esc_html__( 'Use the next shortcode [rotatingtweets screen_name=\'your_twitter_name\'] to load your twitter feed', 'cian' ),
			),
			$add_css_animation,
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'cian' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cian' ),
			),
		),
	) );

	/**
	 * Add Content Element: Service Block
	 */
	require_once( get_template_directory() . '/vc_extends/cian_service_block.php' );
	vc_map( array(
		'name' => esc_html__( 'Service block', 'cian' ),
		'base' => 'vc_cian_service_block',
		'icon' => 'cian_element',
		'category' => esc_html__( 'Content', 'cian' ),
		'params' => array(
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Heading Text', 'cian' ),
				'param_name' => 'heading',
				'value' => esc_html__( 'Service Heading', 'cian' ),
			),
			array(
				'type' => 'dropdown',
				'holder' => 'div',
				'heading' => esc_html__( 'Style', 'cian' ),
				'param_name' => 'style',
				'value' => array(
					'Horizontal style' => 'horizontal-style',
					'Vertical style' => 'vertical-style',
				),
				'save_always' => true,
			),
			$add_icon,
			array(
				'type' => 'textarea_html',
				'holder' => 'div',
				'heading' => esc_html__( 'Content', 'cian' ),
				'param_name' => 'content',
				'value' => esc_html__( '<p>I am a service block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>', 'cian' ),
			),
			$add_css_animation,
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'cian' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cian' ),
			),
		),
	) );

}
add_action( 'init', 'cian_custom_visual_composer' );

/**
 * Visual Composer Custom Styling
 */
function cian_admin_print_styles() {
	?>
	<style type="text/css">
	.wpb-select.icon .fa{
		display: block;
		font-family: inherit;
	}
	.wpb-select.icon option[class*='fa-']:before{
		content: '';
	}
	.wpb_vc_cian_quotes_carousel h4.wpb_element_title,
	.wpb_vc_cian_quotes_carousel .vc_container_for_children {
		float: left;
		width: 100%;
	}
	.wpb_vc_cian_quotes_carousel > .vc_controls > .vc_controls-cc{
		top: 35px;
	}
	.cian_element.vc_element-icon {
	    background-image: url('<?php echo CIAN_THEME;?>vc_extends/assets/img/cian_element.png');
	    background-position: initial !important;
	}
	.filter {
		width: 100%;
    	margin: 2px 0px !important;	
	}
	.wpb_vc_param_value.dropdown option.animated {
		-webkit-animation-duration: inherit;
		   -moz-animation-duration: inherit;
			-ms-animation-duration: inherit;
			 -o-animation-duration: inherit;
				animation-duration: inherit;
		-webkit-animation-fill-mode: inherit;
		   -moz-animation-fill-mode: inherit;
			-ms-animation-fill-mode: inherit;
			 -o-animation-fill-mode: inherit;
				animation-fill-mode: inherit;
	}
	.vc_ui-panel-window-inner .wpb_vc_param_value.dropdown {
		-webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.07);
		   -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,.07);
			-ms-box-shadow: inset 0 1px 2px rgba(0,0,0,.07);
			 -o-box-shadow: inset 0 1px 2px rgba(0,0,0,.07);
				box-shadow: inset 0 1px 2px rgba(0,0,0,.07);
		-webkit-transition: 50ms border-color ease-in-out;
		   -moz-transition: 50ms border-color ease-in-out;
			-ms-transition: 50ms border-color ease-in-out;
			 -o-transition: 50ms border-color ease-in-out;
				transition: 50ms border-color ease-in-out;
		border: 1px solid #ddd;	
		background-color: #fff;
		color: #32373c;
		outline: 0;			
	}
	.wpb_vc_param_value.dropdown option{
		background: #fff;
		color: #333;
		text-shadow: none;
	}
	</style>
	<?php
}
add_action( 'admin_print_styles-post-new.php', 'cian_admin_print_styles' );
add_action( 'admin_print_styles-post.php', 'cian_admin_print_styles' );