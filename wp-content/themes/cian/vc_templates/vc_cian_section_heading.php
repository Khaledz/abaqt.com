<?php

extract( shortcode_atts( array(
	'small_heading' => '',
	'heading' 		=> '',
	'text' 			=> '',
	'align'         => 'center',
	'css_animation' => '',
	'el_class'      => '',
), $atts ) );

$el_class = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_cian_section_heading wpb_content_element text-center ' . $this->getCSSAnimation( $css_animation ) . $el_class, $this->settings['base'] );
?>

<div class="<?php echo esc_attr($css_class); ?>">
	<div class="wpb_wrapper">
		<span class="section-name"><?php echo esc_html($small_heading) ?></span>
		<h3><?php echo cian_wp_kses_heading($heading); ?></h3>
		<span class="line"></span>
		<p><?php echo cian_wp_kses_heading($text); ?></p>
	</div>
</div>
<?php echo $this->endBlockComment( 'cian_section_heading' ); ?>