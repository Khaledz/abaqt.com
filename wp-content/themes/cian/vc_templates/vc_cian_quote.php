<?php

extract( shortcode_atts( array(
	'cite'     => 0,
	'el_class' => '',
	'active'   => 0,
), $atts ) );

$el_class = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_cian_quote quote text-center ' .  ( ( $active ) ? 'active ' : '' ) . $el_class, $this->settings['base'] );
?>

<li class="<?php echo esc_attr($css_class); ?>">
	<div class="quote-content">
		<h3><?php echo wpb_js_remove_wpautop( $content ); ?></h3>
		<p><?php echo esc_html($cite); ?></p>
	</div>
</li>
<?php echo $this->endBlockComment( 'cian_quote' ); ?>