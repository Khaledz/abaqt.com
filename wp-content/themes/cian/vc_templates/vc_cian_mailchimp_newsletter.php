<?php

extract( shortcode_atts( array(
	'mailchimp_urlform'	=> '',
	'mailchimp_u'      	=> '',
	'mailchimp_id' 		=> '',
	'css_animation' => '',
	'el_class'      => '',
), $atts ) );

$el_class = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, "wpb_cian_mailchimp_newsletter wpb_content_element $el_class " . $this->getCSSAnimation( $css_animation ), $this->settings['base'] );
?>

<div class="<?php echo esc_attr($css_class); ?>">
	<div class="wpb_wrapper">
		<div class="subscribe">
			<div id="mc_embed_signup">
				<form id="mc-form" class="form-inline">
					<input id="mc-email" type="email" placeholder="<?php echo esc_attr__('Type your email...', 'cian')?>" class="form-control">
				    <button type="submit" class="btn btn-main css3transition"><?php echo esc_html__('Send', 'cian')?></button>
				   	<label for="mc-email"></label>
			    </form>
			</div>
		</div>
	</div>
</div>
<?php if( !empty($mailchimp_urlform) && !empty($mailchimp_u) && !empty($mailchimp_id)): ?>
<script type="text/javascript" src="<?php echo CIAN_JS?>jquery.ajaxchimp.js?ver=1.0.0"></script>
<script>
	var urlform = '<?php echo $mailchimp_urlform; ?>';
	var u = '<?php echo $mailchimp_u; ?>';
	var id = '<?php echo $mailchimp_id; ?>';
	jQuery("#mc-form").ajaxChimp({
		url: urlform+"?u="+u+"&amp;id="+id
	}); 
</script>
<?php endif; ?>

<?php echo $this->endBlockComment( 'cian_mailchimp_newsletter' ); ?>