<?php

extract( shortcode_atts( array(
	'auto_rotate' => 0,
	'align'       => 'center',
	'el_class'    => '',
	'css_animation' => '',		
), $atts ) );

$auto_rotate = absint( $auto_rotate );

wp_enqueue_script( 'jquery-caroufredsel' );

// Get quotes
global $shortcode_tags;
$temp = $shortcode_tags;
$shortcode_tags = array( 'vc_cian_quote' => '' );
$content = preg_replace( '/\[vc_cian_quote/', '[vc_cian_quote active="1"', $content, 1 );
preg_match_all( '/' . get_shortcode_regex() . '/s', $content, $matches );
$quotes = array();
foreach ( $matches[3] as $atts ) {
	$quotes[] = shortcode_parse_atts( $atts );
}
$shortcode_tags = $temp;

$el_class = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_cian_quotes_carousel wpb_content_element ' . $el_class, $this->settings['base'] );
$css_class .= $this->getCSSAnimation( $css_animation );
?>

<div class="<?php echo esc_attr($css_class); ?>">
	<div class="wpb_wrapper">
		<div class="icon-title-content">
			<div class="icon-title fa fa-quote-right"></div>
		</div>
		<ul class="quotes-carousel caroufredsel js-caroufredsel" data-caroufredsel="quotes-carousel" data-interval="<?php echo esc_attr($auto_rotate); ?>">
			<?php echo wpb_js_remove_wpautop( $content ); ?>
		</ul>

	</div>
</div>
<?php echo $this->endBlockComment( 'cian_quotes_carousel' ); ?>