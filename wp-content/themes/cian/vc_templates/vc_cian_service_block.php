<?php

extract( shortcode_atts( array(
	'heading'       => '',
	'style'         => '',
	'icon'          => '',
	'el_class'      => '',
	'css_animation' => '',
), $atts ) );

$el_class = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_cian_service_block wpb_content_element ' . $style . $el_class, $this->settings['base'] );
$css_class .= $this->getCSSAnimation( $css_animation );
?>

<div class="<?php echo esc_attr($css_class); ?>">
	<div class="wpb_wrapper">
		<?php if ( ! empty( $icon ) ) : ?>
			<div class="service-icon css3transition">
				<span class="<?php echo esc_attr($icon); ?>"></span>
			</div>
		<?php endif; ?>
		<?php if ($style == 'horizontal-style'):?>
			<p class="service-heading css3transition"><?php echo cian_wp_kses_heading($heading); ?></p>
		<?php else:?>
			<h5 class="service-heading css3transition"><?php echo cian_wp_kses_heading($heading); ?></h5>
		<?php endif;?>
			<p><?php echo wpb_js_remove_wpautop( $content ); ?></p>
	</div>
</div>
<?php echo $this->endBlockComment( 'cian_service_block' ); ?>