<?php

extract( shortcode_atts( array(
	'count'         => '',
	'filter'        => 'true',
	'el_class'      => '',
	'css_animation' => '',
	'category'      => '',
), $atts ) );

if ( ! class_exists( 'Portfolio_Post_Type' ) ) return null;

$uid = cian_generate_id('pf-');
cian_isotope_script();
cian_lightGallery_script();

global $wp_query, $more; $more = 0; $temp = $wp_query;

$wp_query = new WP_Query(array_merge(
	array(
		'post_type'           => 'portfolio',
		'posts_per_page'      => $count,
		'ignore_sticky_posts' => 1,
	),
	(empty($category)) ? array() : array(
		'tax_query' => array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'portfolio_category',
				'field'    => 'slug',
				'terms'    => array_map('trim', explode(',', $category)),
				'operator' => 'IN'
			)
		)
	)
));

$el_class  = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_cian_portfolio_grid wpb_content_element js-isotope-portfolio-grid ' . $el_class, $this->settings['base'] );
?>

<?php if ( have_posts() ) : ?>

	<div class="<?php echo $css_class; ?>" id="<?php echo $uid; ?>" >
		<div class="wpb_wrapper">

			<?php if ( $filter == 'true' ) : ?>
				<div class="portfolio-grid-filter">
					<a href="#" data-filter="*" class="active"><?php _e( 'All', 'cian' ); ?></a>
					<?php $all_categories = get_terms( 'portfolio_category', array(
						'hide_empty' => false,
					) );

					foreach ( $all_categories as $category) : ?>
					<a href="#" data-filter=".filter-<?php echo $category->slug; ?>"><?php echo $category->name; ?></a>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>

			<div class="portfolio-grid-loop row">
				<div class="portfolio-grid-sizer"></div>
				<div id="lightgallery">
				
				<?php while( have_posts() ) : the_post(); ?>

					<?php $categories = get_the_terms( get_the_ID(), 'portfolio_category' );

					$cat_string = array();
					$filter_string = array();
					if ( ! empty( $categories ) ) {
						foreach ( $categories as $category ) {
							$cat_string[] = $category->name;
							$filter_string[] = 'filter-' . $category->slug;
						}
					} ?>
					<?php if(has_post_format('video')) : ?>
					<?php $cb_video = get_post_meta( get_the_ID(), '_format_video_embed', true ) ?>
					<a href="<?php echo $cb_video ?>" class="portfolio-grid-post <?php echo implode( ' ', $filter_string ) . ' ' . $this->getCSSAnimation( $css_animation ); ?>" data-sub-html="<h4><?php the_title(); ?></h4><p><?php the_content(); ?></p>" >
					<?php else : ?>	
					<a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>" class="portfolio-grid-post <?php echo implode( ' ', $filter_string ) . ' ' . $this->getCSSAnimation( $css_animation ); ?>" data-sub-html="<h4><?php the_title(); ?></h4><p><?php the_content(); ?></p>" >
					<?php endif; ?>		
						<div class="portfolio-grid-post-content">
							<img src="<?php echo cian_aq_resize( get_post_thumbnail_id(), 480, null, true, true ); ?>" alt="<?php the_title(); ?>" />
							<div class="portfolio-grid-post-background css3transition"></div>
							<div class="portfolio-grid-post-text">
								<h3><?php the_title(); ?></h3>
								<p><?php the_content(); ?></p>
							</div>
						</div>
					</a>
					
				<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>
	<?php echo $this->endBlockComment( 'cian_portfolio_grid' ); ?>

<?php endif; ?>

<?php $wp_query = $temp; wp_reset_postdata(); ?>