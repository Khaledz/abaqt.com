<?php

extract( shortcode_atts( array(
	'title' 		=> '',
	'subtitle' 		=> '',
	'price' 		=> '',
	'details'    	=> '',
	'button'    	=> '',
	'button_text'   => '',
	'button_url'    => '',
	'css_animation' => '',
	'el_class'      => '',
), $atts ) );

$details_array = explode('|', $details);

$el_class = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_cian_price_table text-center' . $el_class, $this->settings['base'] );
$css_class .= $this->getCSSAnimation( $css_animation );
?>

<div class="<?php echo esc_attr($css_class); ?>">
	<div class="wpb_wrapper">
		<div class="plan">
       		<div class="plan-heading">
	            <h3><?php echo esc_html($title);?></h3>
	            <p><?php echo esc_html($subtitle);?></p>
	            <div class="circle-price">
	            	<p><?php echo esc_html($price);?></p>
	            </div>
        	</div>
			<ul>
			<?php foreach ( $details_array as $detail ) : ?>
				<li><?php echo cian_wp_kses_heading($detail); ?></li>
			<?php endforeach; ?>
			</ul>
			<?php if ( $button == true ): ?>
          	<div class="buy-now">
          		<a class="btn btn-default btn-sm" href="<?php echo esc_url($button_url);?>"><?php echo esc_html($button_text);?></a>
          	</div>
	    	<?php endif; ?>
		</div>
	</div>
</div>
<?php echo esc_html($this->endBlockComment( 'cian_price_table' )); ?>