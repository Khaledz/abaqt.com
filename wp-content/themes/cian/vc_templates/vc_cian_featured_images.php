<?php

extract( shortcode_atts( array(
	'number_featured_images' => '',	
	'left_image'	=> '',
	'center_image'	=> '',
	'right_image'	=> '',
	'css_animation' => '',
	'el_class'      => '',
), $atts ) );

$el_class = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_cian_featured_images wpb_content_element' . $this->getCSSAnimation( $css_animation ) . $el_class, $this->settings['base'] );
?>

<div class="<?php echo esc_attr($css_class); ?>">
	<div class="wpb_wrapper">
	<?php if ( (($number_featured_images == '2') || ($number_featured_images == '3')) && (!empty($left_image))  ):?>
		<?php $image_data = wp_get_attachment_metadata($left_image);?>
		<img class="feat1 <?php if ($number_featured_images == '2') { echo 'feat_All';} ?>" src="<?php echo esc_url( wp_get_attachment_url( $left_image )) ?>" alt="<?php echo get_the_title($left_image); ?>" width="<?php echo $image_data['width'];?>" height="<?php echo $image_data['height'];?>" />
	<?php endif; ?>
	<?php if ( (($number_featured_images == '2') || ($number_featured_images == '3')) && (!empty($right_image))  ):?>
		<?php $image_data = wp_get_attachment_metadata($right_image);?>
		<img class="feat2 <?php if ($number_featured_images == '2') { echo 'feat_All';} ?>" src="<?php echo esc_url( wp_get_attachment_url( $right_image )) ?>" alt="<?php echo get_the_title($right_image); ?>" width="<?php echo $image_data['width'];?>" height="<?php echo $image_data['height'];?>" />
	<?php endif; ?>
	<?php if ( (($number_featured_images == '1') || ($number_featured_images == '3')) && (!empty($center_image))  ):?>
		<?php $image_data = wp_get_attachment_metadata($center_image);?>
		<img class="feat3" src="<?php echo esc_url( wp_get_attachment_url( $center_image )) ?>" alt="<?php echo get_the_title($center_image); ?>" width="<?php echo $image_data['width'];?>" height="<?php echo $image_data['height'];?>" />
	<?php endif; ?>
	</div>
</div><?php echo $this->endBlockComment( 'cian_featured_images' ); ?>