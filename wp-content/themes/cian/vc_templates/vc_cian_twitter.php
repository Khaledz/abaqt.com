<?php

extract( shortcode_atts( array(
	'shortcode'         => '',
	'css_animation' => '',		
	'el_class'      => '',
), $atts ) );

$el_class = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_cian_twitter wpb_content_element ' . $el_class, $this->settings['base'] );
?>

<div class="<?php echo $css_class; ?>">
	<div class="wpb_wrapper">
		<div class="twitter-content">
			<div class="icon-twitter"><i class="fa fa-twitter"></i></div>
			<?php echo do_shortcode($shortcode); ?>
		</div>
	</div>
</div><?php echo $this->endBlockComment( 'cian_twitter' ); ?>