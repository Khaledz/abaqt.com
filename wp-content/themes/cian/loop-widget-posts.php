<?php if ( have_posts() ) : ?>
	<ul class="widget-posts-list">
		<?php while( have_posts() ) : the_post(); ?>
			<li <?php echo post_class( 'widget-post' ); ?>>
				
				<?php if(has_post_format('link')) : ?>
					<div class="widget-post-thumbnail">
						<div class="widget-post-thumbnail-icon">
							<span class="fa fa-quote-left"></span>
						</div>
					</div>
				
				<?php elseif(has_post_format('quote')) : ?>
					<div class="widget-post-thumbnail">
						<div class="widget-post-thumbnail-icon">
							<span class="fa fa-chain"></span>
						</div>
					</div>
				
				<?php else: ?>
					<?php if ( has_post_thumbnail() ) : ?>
					<div class="widget-post-thumbnail">
						<img src="<?php echo cian_aq_resize( get_post_thumbnail_id(), 110, 70, true, true ); ?>" alt="<?php echo esc_url( get_the_title() ); ?>" width="110" height="70" />
					</div>
					<?php else: ?>
					<div class="widget-post-thumbnail">
						<div class="widget-post-thumbnail-icon">
							<span class="fa fa-camera"></span>
						</div>
					</div>
					<?php endif; ?>
				<?php endif; ?>
				
				<h5><a href="<?php the_permalink(); ?>" class="widget-post-title"><?php the_title(); ?></a></h5>
				<?php if ( $show_date ) : ?>
					<small class="widget-post-date"><?php echo get_the_date(); ?></small>
				<?php endif; ?>
			</li>
		<?php endwhile; ?>
	</ul>
<?php endif; ?>