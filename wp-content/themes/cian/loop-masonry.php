<div class="wpb_cian_blog_grid wpb_content_element js-isotope-blog-grid">
	<div id="blog-grid-<?php the_ID(); ?>" <?php post_class( 'blog-grid-post ' ); ?>>
		<div class="blog-grid-post-wrapper">

			<?php if ( has_post_format('audio') ) : ?>
			<div class="blog-grid-post-thumbnail">
				<?php $cb_audio = get_post_meta( get_the_ID(), '_format_audio_embed', true ); ?>
				<?php if(wp_oembed_get( $cb_audio )) : ?>
					<?php echo wp_oembed_get($cb_audio); ?>
				<?php else : ?>
					<?php echo $cb_audio; ?>
				<?php endif; ?>
			</div>
			
			<?php elseif(has_post_format('gallery')) : ?>
			<?php cian_bxslider_style_script(); ?>
			<?php $images = get_post_meta( get_the_ID(), '_format_gallery_images', true ); ?>
			<?php if($images) : ?>
			<div class="blog-grid-post-thumbnail">
				<ul class="bxslider">
				<?php foreach($images as $image) : ?>
					
					<?php $the_image = wp_get_attachment_image_src($image, 'full'); ?> 
					<?php $the_caption = get_post_field('post_excerpt', $image); ?>
					<li><img src="<?php echo $the_image[0]; ?>" <?php if($the_caption) : ?>title="<?php echo $the_caption; ?>"<?php endif; ?> width="<?php echo $the_image[1]; ?>" height="<?php echo $the_image[2]; ?>" /></li>
					
				<?php endforeach; ?>
				</ul>
			</div>
			<?php endif; ?>
			
			<?php elseif(has_post_format('video')) : ?>
			<div class="blog-grid-post-thumbnail">
				<?php $cb_video = get_post_meta( get_the_ID(), '_format_video_embed', true ); ?>
				<?php if(wp_oembed_get( $cb_video )) : ?>
					<?php echo wp_oembed_get($cb_video); ?>
				<?php else : ?>
					<?php echo $cb_video; ?>
				<?php endif; ?>
			</div>
				
			<?php elseif(has_post_format('link')) : ?>
				<?php $link = get_post_meta( get_the_ID(), '_format_link_url', true); ?>
				<?php if ( !empty($link)) :?>
				<div class="blog-grid-post-thumbnail blog-grid-post-link">
					<h4><a class="link" href="<?php echo $link;?>" title="<?php echo esc_attr( get_the_title() ); ?>"><?php echo $link; ?></a></h4>
				</div>
				<?php endif; ?>
				
			<?php elseif(has_post_format('quote')) : ?>
			<div class="blog-grid-post-thumbnail blog-grid-post-quote">
				<h4><a href="<?php the_permalink(); ?>"><?php the_content(); ?></a></h4>
				<?php $quote_author = get_post_meta( get_the_ID(), '_format_quote_source_name', true); ?>
				<?php $quote_url = get_post_meta( get_the_ID(), '_format_quote_source_url', true); ?>
				<h6 class="quote-author"><a href="<?php the_permalink(); ?>" target="_blank"><?php echo esc_html($quote_author); ?></a></h6>
			</div>
				
			<?php else: ?>
				<?php if ( has_post_thumbnail() ) : ?>
				<div class="blog-grid-post-thumbnail">
					<a href="<?php the_permalink(); ?>">
						<img src="<?php echo cian_aq_resize( get_post_thumbnail_id(), 570, 250, true, true ); ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" width="570" height="250" />
					</a>
				</div>
				<?php endif; ?>
			
			<?php endif; ?>

			<?php $categories = get_the_terms( get_the_ID(), 'category' );

			$cat_string = array();
			foreach ( $categories as $category ) {
				$cat_string[] = $category->name;
			} ?>
			
			<?php if(!has_post_format('quote')) : ?>
			<div class="blog-grid-post-content">
				<p class="post-category"><?php the_category( ' | ' );?></p>
				<h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<?php echo cian_get_my_excerpt(17); ?>
				<p class="post-date"><span class="fa fa-calendar"></span> <a href="<?php the_permalink(); ?>"><?php echo get_the_date(); ?></a> | <span class="fa fa-comments-o"></span>  <?php comments_popup_link( 'No comments', '1 comment', '% comments' ); ?></p>
				<a href="<?php the_permalink(); ?>" class="btn btn-sm btn-main"><?php esc_html_e('Read More', 'cian'); ?></a>
			</div>
			<?php else: ?>
			<div class="blog-grid-post-content">
				<p class="post-category"><?php the_category( ' | ' );?></p>
				<p class="post-date post-date-quote"><span class="fa fa-calendar"></span> <a href="<?php the_permalink(); ?>"><?php echo get_the_date(); ?></a> | <span class="fa fa-comments-o"></span>  <?php comments_popup_link( 'No comments', '1 comment', '% comments' ); ?></p>
				<a href="<?php the_permalink(); ?>" class="btn btn-sm btn-main"><?php esc_html_e('Read More', 'cian'); ?></a>
			</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>