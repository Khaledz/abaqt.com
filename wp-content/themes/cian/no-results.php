<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * @package CIAN
 * @since CIAN 1.0
 */
?>

<h1 class="no-results text-center"><?php esc_html_e( 'Apologies, but there is no post yet.', 'cian' ); ?></h1>