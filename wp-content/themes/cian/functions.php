<?php

/**
 * Define constants
 */
define( 'CIAN_CSS_DIR', trailingslashit(get_template_directory() . '/css' ) );
define( 'CIAN_JS_DIR', trailingslashit(get_template_directory() . '/js' ) );
define( 'CIAN_IMAGES_DIR', trailingslashit(get_template_directory() . '/images' ) );
define( 'CIAN_ADMIN_DIR', trailingslashit(get_template_directory() . '/admin' ) );
define( 'CIAN_INCLUDES_DIR', trailingslashit(get_template_directory() . '/includes' ) );
define( 'CIAN_PLUGINS_DIR', trailingslashit(get_template_directory() . '/plugins' ) );
define( 'CIAN_CSS', trailingslashit(get_template_directory_uri() . '/css' ) );
define( 'CIAN_JS', trailingslashit(get_template_directory_uri() . '/js' ) );
define( 'CIAN_IMAGES', trailingslashit(get_template_directory_uri() . '/images' ) );
define( 'CIAN_ADMIN', trailingslashit(get_template_directory_uri() . '/admin' ) );
define( 'CIAN_INCLUDES', trailingslashit(get_template_directory_uri() . '/includes' ) );
define( 'CIAN_THEME', trailingslashit(get_template_directory_uri()) );
define( 'CIAN_OPTION_KEY', 'cian_option' );

/**
 * Global variables
 */
global $cian_data;
$cian_data = array(
		'unique_counter' => 0,
);

/**
 * Generate Random ID with specified prefix string
 */
function cian_generate_id( $prefix = '' ) {
	global $cian_data;
	return $prefix . ++$cian_data['unique_counter'];
}

/**
 * Content Width
 */
if ( ! isset( $content_width ) ) $content_width = 760;

/**
 * Theme set up
 */
if ( !function_exists('cian_theme_setup') ) {
	function cian_theme_setup() {
		// Post formats
		add_theme_support( 'post-formats', array( 'gallery', 'video', 'audio', 'link', 'quote' ) );
		// Post thumbnails
		add_theme_support( 'post-thumbnails' );
		add_post_type_support( 'portfolio', 'post-formats' );
	}
}
add_action( 'after_setup_theme', 'cian_theme_setup' );

/**
 * Remove the default widget 'Recent posts'
 */
add_action( 'widgets_init', 'unregister_widgets' );

function unregister_widgets() {
	unregister_widget( 'WP_Widget_Recent_Posts' );
}

/**
 * One Click Install
 */
require get_template_directory() .'/includes/one-click-install/init.php';

/**
 * Function to get recent posts
 */
function cian_get_recent_posts( $numposts, $exclude ) {
	global $wpdb;		
	$get_posts = $wpdb->get_results(
		 $wpdb->prepare(
			"
			SELECT *
			FROM $wpdb->posts
			WHERE post_status = %s
			AND post_type = %s
			AND ID NOT IN (%d)
			ORDER BY post_date desc
			LIMIT %d
			",
			'publish',
			'post',
			$exclude,
			$numposts
		));
	return $get_posts;
}

/**
 * Check if the sidebar is enable
 */
function cian_sidebar_enable(){
	
	$pages_with_sidebar = cian_option( 'pages_with_sidebar', array() );
	$native_types = array( 'blog_index', 'blog_archive', 'portfolio_index', 'portfolio_single', 'search' );
	
	if ( is_search() )                             { $type = 'search'; }
	elseif ( is_post_type_archive( 'portfolio' ) ) { $type = 'portfolio_index'; }
	elseif ( is_singular( 'portfolio' ) )          { $type = 'portfolio_single'; }
	elseif ( is_page() )                           { $type = 'page_single'; }
	elseif ( is_home() )                           { $type = 'blog_index'; }
	elseif ( is_archive() )                        { $type = 'blog_archive'; }
	elseif ( is_single() )                         { $type = 'blog_single'; }
	else                                           { $type = ''; }
	
	if ( empty( $type ) ) {
		// No page title
		$show_sidebar = false;
	} elseif ( 'page_single' == $type ) {
		// if page
		// this is called by the active page template, so yes!
		$show_sidebar = true;
	} elseif ( in_array( $type, $native_types ) || -1 == cian_post_option( 'enable_sidebar', -1 ) || is_null( cian_post_option( 'enable_sidebar', -1 ) ) ) {
		// if doesn't have metabox value (native pages or unset metabox value)
		// or native pages
		// inherit from theme options
		$show_sidebar = in_array( $type, $pages_with_sidebar );
	} else {
		// there is a specific setting for current page
		$show_sidebar = cian_post_option( 'enable_sidebar', 1 );
	}
	return $show_sidebar;
}

function portfolio_custom_css() {
  echo '<style>
    .post-type-portfolio #vp-post-formats-ui-tabs ul li:nth-child(2),
    .post-type-portfolio #vp-post-formats-ui-tabs ul li:nth-child(4),
    .post-type-portfolio #vp-post-formats-ui-tabs ul li:nth-child(5),
    .post-type-portfolio #vp-post-formats-ui-tabs ul li:nth-child(6) {
		display: none;
    } 
  </style>';
}
add_action('admin_head', 'portfolio_custom_css');

/**
 * The excerpt
 */
function cian_my_excerpt($excerpt_length = 55, $id = false, $echo = true) {
	 
	$text = '';
	if($id) {
		$the_post = & get_post( $my_id = $id );
		$text = ($the_post->post_excerpt) ? $the_post->post_excerpt : $the_post->post_content;
	} else {
		global $post;
		$text = ($post->post_excerpt) ? $post->post_excerpt : get_the_content('');
	}
	$text = strip_shortcodes( $text );
	$text = apply_filters('the_content', $text);
	$text = str_replace(']]>', ']]&gt;', $text);
	$text = strip_tags($text);
	$excerpt_more = '...';
	$words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
	if ( count($words) > $excerpt_length ) {
		array_pop($words);
		$text = implode(' ', $words);
		$text = $text . $excerpt_more;
	} else {
		$text = implode(' ', $words);
	}
	if($echo)
		echo apply_filters('the_content', $text);
	else
		return $text;
}

function cian_get_my_excerpt($excerpt_length = 55, $id = false, $echo = false) {
	return cian_my_excerpt($excerpt_length, $id, $echo);
}

/**
 * Numeric pagination
 */
function cian_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="pagination"><ul>' . "\n";

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li class="dotted">...</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li class="dotted">...</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	echo '</ul></div>' . "\n";
}

/**
 * Numeric pagination
 */
function cian_posts_nav() {
	$defaults = array(
		'before'           => '<p>' . esc_html__( 'Pages:', 'cian' ),
		'after'            => '</p>',
		'link_before'      => '',
		'link_after'       => '',
		'next_or_number'   => 'number',
		'separator'        => ' ',
		'nextpagelink'     => esc_html__( 'Next page', 'cian' ),
		'previouspagelink' => esc_html__( 'Previous page', 'cian' ),
		'pagelink'         => '%',
		'echo'             => 1
	);
    wp_link_pages( $defaults );
}

function cian_social_shares(){
	$blog_social_media_links = cian_option( 'social_share_links', array() );
	if ( ! empty( $blog_social_media_links ) ) : 
		$content = '<ul class="post-share">';
		foreach ( $blog_social_media_links as $value ) :
			switch ($value){
				case 'facebook':
					$link = 'https://www.facebook.com/sharer/sharer.php?u='.get_the_permalink();
					break;	
				case 'twitter':
					$link = 'https://twitter.com/home?status=Check%20out%20this%20article:%20'.get_the_title().'%20-%20'.get_the_permalink();
					break;
				case 'google-plus':
					$link = 'https://plus.google.com/share?url='.get_the_permalink();
					break;
				case 'linkedin':
					$link = 'http://www.linkedin.com/shareArticle?mini=true&url='.get_the_permalink().'&title='.get_the_title().'&source='.esc_url( home_url() );
					break;
				case 'pinterest':
					$pin_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
					$link = 'https://pinterest.com/pin/create/button/?url='.get_the_permalink().'&media='.$pin_image.'&description='.get_the_title();
					break;
			} 
			$content.='
			<li>
				<a target="_blank" class="share-'.esc_attr($value).'" href="'.esc_url($link).'">
					<i class="fa fa-'.esc_attr($value).'"></i>
				</a>
			</li>
			';
			endforeach;
		$content.='</ul>';	
		return $content;	
	endif;	
	
}

/**
 * Load languages
 */
load_theme_textdomain( 'cian', get_template_directory() . '/languages' );

/**
 * Include all files in /includes directory
 */
$includes_files = array_filter( glob( CIAN_INCLUDES_DIR . '*' ), 'is_file' );
foreach ( $includes_files as $file ) {
	require_once( $file );
}

/**
 * Function for escaping section heading
 */
if(!function_exists('cian_wp_kses_heading')) {
	function cian_wp_kses_heading($content) {
		$heading_atts = array(
		    'strong' => array(),
		);
		return wp_kses($content, $heading_atts);
	}
}

/**
 * Function for escaping links in the description of an element in VC
 */
if(!function_exists('cian_wp_kses_description')) {
	function cian_wp_kses_description($content) {
		$description_atts = array(
		    'a' => array(
		        'href' => array(),
				'target' => array(),
		    ),
		);
		return wp_kses($content, $description_atts);
	}
}

/**
 * Function for escaping audio and video blog post
 */
if(!function_exists('cian_wp_kses_post')) {
	function cian_wp_kses_post($content) {
		$blog_atts = array(
		    'iframe' => array(
		        'width' => array(),
				'height' => array(),
				'src' => array(),
		    ),
		);
		return wp_kses($content, $blog_atts);
	}
}

/**
 * Function to get the width of the images
 */
function cian_get_width_image($image){
	list($width, $height, $type, $attr) = getimagesize($image);
	return $width;
}

/**
 * Function to get the height of the images
 */
function cian_get_height_image($image){
	list($width, $height, $type, $attr) = getimagesize($image);
	return $height;
}

/**
 * Include Vafpress Framework functions
 */
require_once( 'functions-vafpress-framework.php' );

/**
 * Include Visual Composer functions
 */
if ( class_exists( 'WPBakeryVisualComposerAbstract' ) ) {
	require_once( 'functions-visual-composer.php' );
}

/**
 * Load Languages
 */
add_action( 'after_setup_theme', 'cian_languages' );
function cian_languages() {
    load_theme_textdomain( 'cian', get_template_directory() . '/lang' );
}

/**
 * TGM Plugin Activation
 */
function cian_tgmpa() {

	$plugins = array(
		array(
			'name'     => 'Visual Composer',
			'slug'     => 'js_composer',
			'source'   => CIAN_PLUGINS_DIR . 'js_composer.zip',
			'required' => true,
		),
		array(
			'name' 		=> 'Portfolio Post Type',
			'slug' 		=> 'portfolio-post-type',
			'source'   	=> CIAN_PLUGINS_DIR .'portfolio-post-type.zip',
			'required' 	=> true,
		),
		array(
			'name'     => 'Vafpress Post Formats UI',
			'slug'     => 'vafpress-post-formats-ui-develop',
			'source'   => CIAN_PLUGINS_DIR .'vafpress-post-formats-ui-develop.zip',
			'required' => true,
		),
		array(
			'name'     => 'Contact Form 7',
			'slug'     => 'contact-form-7',
			'required' => false,
		),
		array(
			'name'     => 'Rotating Tweets',
			'slug'     => 'rotatingtweets',
			'required' => true,
		),
		array(
			'name'     => 'Verde - Minimal Coming Soon Plugin',
			'slug'     => 'verde_plugin',
			'source'   => CIAN_PLUGINS_DIR .'verde_plugin.zip',
			'required' => false,
		),
	);

	$config = array(
		'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);

	tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'cian_tgmpa' );

/**
 * Abstract Function to Call Aqua Resizer
 */
function cian_aq_resize( $attachment_id, $width = null, $height = null, $crop = true, $single = true ) {

	if ( is_null( $attachment_id ) ) return null;

	$image = wp_get_attachment_image_src( $attachment_id, 'full' );

	$return = aq_resize( $image[0], $width, $height, $crop, $single );

	if ( $return ) {
		return $return;
	}
	else {
		return $image[0];
	}
}

/**
 * Abstract Function to build Background Repeat inline style
 */
function cian_build_background_repeat_style( $background_repeat ) {
	$background_repeat_style = '';

	switch ( $background_repeat ) {
		case 'cover':
			$background_repeat_style .= 'background-size: cover; background-repeat: no-repeat;';
			break;
		case 'contain':
			$background_repeat_style .= 'background-size: contain; background-repeat: no-repeat;';
			break;
		case 'no-repeat':
			$background_repeat_style .= 'background-repeat: no-repeat;';
			break;
	}
	return $background_repeat_style;
}

/**
 * Color code hex to rgb
 */
function cian_hex2rgb($hex) {
	$hex = str_replace("#", "", $hex);

	if(strlen($hex) == 3) {
		$r = hexdec(substr($hex,0,1).substr($hex,0,1));
		$g = hexdec(substr($hex,1,1).substr($hex,1,1));
		$b = hexdec(substr($hex,2,1).substr($hex,2,1));
	} else {
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
	}
	$rgb = array($r, $g, $b);
	return $rgb;
}

/**
 * Enqueue styles and scripts
 */
function cian_action_enqueue_scripts() {
	$theme_data = wp_get_theme();

	/**
	 * CSS
	 */
	
	// GoogleFonts
	
	$font_weights = array( 100, 200, 300, 400, 500, 600, 700, 800, 900 );
	$font_styles  = array( 'normal', 'italic' );
	
	VP_Site_GoogleWebFont::instance()->add( cian_option( 'main_typograhpy' ), $font_weights, $font_styles );
	VP_Site_GoogleWebFont::instance()->register();
	foreach ( VP_Site_GoogleWebFont::instance()->get_names() as $name ) {
		wp_enqueue_style( $name );
	}
	
	VP_Site_GoogleWebFont::instance()->add( cian_option( 'accent_typograhpy' ), $font_weights, $font_styles );
	VP_Site_GoogleWebFont::instance()->register();
	foreach ( VP_Site_GoogleWebFont::instance()->get_names() as $name ) {
		wp_enqueue_style( $name );
	}

	// Other CSS
	wp_register_style( 'bootstrap', CIAN_CSS . 'bootstrap.cian.min.css', array(), '3.1.1' );
	wp_register_style( 'fontawesome', CIAN_CSS . 'font-awesome.min.css', array(), '4.5.0' );
	wp_register_style( 'animate', CIAN_CSS . 'animate.css', array(), '3.1.0-dev' );
	wp_register_style( 'mediaelement', CIAN_CSS . 'mediaelementplayer.min.css', array(), '2.14.2' );
	wp_register_style( 'main-style', CIAN_CSS . 'style.css', array(), '1.0.0' );
	wp_register_style( 'responsive', CIAN_CSS . 'responsive.css', array(
		'bootstrap',
		'fontawesome',
		'animate',
		'mediaelement',
		'main-style',
	), $theme_data->get( 'Version' ) );
	wp_enqueue_style( 'responsive' );

	// Dynamic sStyle
	ob_start(); include( CIAN_CSS_DIR . '/style-dynamic.php' ); $dynamic_style = ob_get_clean();
	wp_add_inline_style( 'responsive', $dynamic_style );
	wp_add_inline_style( 'responsive', cian_option( 'custom_css' ) );

	// Theme stylesheet
	wp_enqueue_style( 'style', get_stylesheet_uri() ); // WP default stylesheet

	/**
	 * JS
	 */

	// Less than IE9 polyfills
	global $wp_scripts;
	wp_register_script( 'html5shiv', CIAN_JS . 'html5shiv.js', array(), '3.7.0' );
	$wp_scripts->add_data( 'html5shiv', 'conditional', 'lt IE 9' );
	wp_enqueue_script( 'html5shiv' );

	wp_register_script( 'respond', CIAN_JS . 'respond.min.js', array(), '1.4.2' );
	$wp_scripts->add_data( 'respond', 'conditional', 'lt IE 9' );
	wp_enqueue_script( 'respond' );

	wp_register_script( 'bootstrap', CIAN_JS . 'bootstrap.min.js', array( 'jquery' ), '3.1.1' );
	wp_register_script( 'smoothscroll', CIAN_JS . 'smoothscroll.min.js', array( 'jquery' ), '1.2.1' );
	wp_register_script( 'jquery-touchswipe', CIAN_JS . 'jquery.touchswipe.min.js', array( 'jquery' ), '1.3.3' );
	wp_register_script( 'jquery-caroufredsel', CIAN_JS . 'jquery.caroufredsel-packed.js', array( 'jquery', 'imagesloaded', 'jquery-touchswipe' ), '6.2.1' );
	wp_register_script( 'jquery-waypoints', CIAN_JS . 'waypoints.min.js', array(), '2.0.4' );
	wp_register_script( 'jquery-jpreloader', CIAN_JS . 'jpreloader.min.js', array( 'jquery' ), '2.1' );
	wp_register_script( 'imagesloaded', CIAN_JS . 'imagesloaded.pkgd.min.js', array(), '3.1.4' );		
	wp_register_script( 'mediaelement', CIAN_JS . 'mediaelement-and-player.min.js', array( 'jquery' ), '2.14.2' );
	wp_register_script( 'modernizr', CIAN_JS . 'modernizr.cian.min.js', array(), '2.8.1' );
	wp_register_script( 'cian-script', CIAN_JS . 'script.js', array(
		// 'modernizr',
		'jquery',
		'jquery-waypoints',
		'bootstrap',
		'smoothscroll',
		'mediaelement',
	), $theme_data->get( 'Version' ) );
	wp_enqueue_script( 'cian-script' );

	if ( cian_option( 'enable_preloader', 1 ) ) {
		wp_enqueue_script( 'jquery-jpreloader' );
	}

	$cian_data['localize']['is_mobile_or_tablet'] = Mobile_Detect::is_mobile_or_tablet() ? 'true' : 'false';
	wp_localize_script( 'cian-script', 'cian', $cian_data['localize'] );
	
	// Dynamic Scripts
	wp_add_inline_script( 'cian-script', cian_option( 'custom_script' ) );
	
}
add_action( 'wp_enqueue_scripts', 'cian_action_enqueue_scripts' );


/**
 * Enqueue isotope script
 */
function cian_isotope_script(){
	if( !wp_script_is( 'isotope', 'registered' ) ) {
		wp_register_script( 'isotope', CIAN_JS . 'isotope.pkgd.min.js', array( 'imagesloaded' ), '2.0.0', true );
	} else {
		cian_append_dependency( 'isotope', 'imagesloaded' );
	}
	wp_enqueue_script( 'isotope' );
}

/**
 * Enqueue lightGallery styles and script
 */
function cian_lightGallery_script(){
	wp_register_style( 'lightGallery', CIAN_INCLUDES . 'lightGallery/css/lightgallery.css', array(), '1.2.22' );
	wp_enqueue_style( 'lightGallery' );
	wp_register_script( 'jquery-lightGallery', CIAN_INCLUDES . 'lightGallery/js/lightgallery.min.js', array( 'jquery' ), '1.2.22', true );
	wp_enqueue_script( 'jquery-lightGallery' );
	wp_register_script( 'lg-video', CIAN_INCLUDES . 'lightGallery/js/lg-video.js', array( 'jquery' ), '1.2.22', true );
	wp_enqueue_script( 'lg-video' );
	wp_register_script( 'lg-fullscreen', CIAN_INCLUDES . 'lightGallery/js/lg-fullscreen.js', array( 'jquery' ), '1.2.22', true );
	wp_enqueue_script( 'lg-fullscreen' );
	wp_register_script( 'lg-zoom', CIAN_INCLUDES . 'lightGallery/js/lg-zoom.js', array( 'jquery' ), '1.2.22', true );
	wp_enqueue_script( 'lg-zoom' );
	wp_register_script( 'lg-autoplay', CIAN_INCLUDES . 'lightGallery/js/lg-autoplay.js', array( 'jquery' ), '1.2.22', true );
	wp_enqueue_script( 'lg-autoplay' );
}

/**
 * Enqueue bxslider styles and script
 */
function cian_bxslider_style_script(){
	wp_register_style( 'bxslider', CIAN_CSS . 'jquery.bxslider.css', array(), '1.0.0' );
	wp_enqueue_style( 'bxslider' );
	wp_register_script( 'jquery-bxslider', CIAN_JS . 'jquery.bxslider.min.js', array( 'jquery' ), '4.1.2', true );
	wp_enqueue_script( 'jquery-bxslider' );
}

/**
 * Enqueue caroufredsel script
 */
function cian_caroufredsel_script(){
	wp_register_script( 'jquery-caroufredsel', CIAN_JS . 'jquery.caroufredsel-packed.js', array( 'jquery', 'imagesloaded', 'touchswipe' ), '6.2.1', true );
	wp_enqueue_script( 'jquery-caroufredsel' );
}

/**
 * Enqueue parallax script
 */
function cian_parallax_script(){
	wp_register_script( 'jquery-parallax', CIAN_JS . 'jquery.parallax.min.js', array( 'jquery' ), '1.1.3', true );
	wp_enqueue_script( 'jquery-parallax' );
}

/**
 * Change WP Title format for better SEO
 */
function cian_filter_wp_title( $title ) {
	global $page, $paged;

	if ( is_feed() ) return $title;

	$site_description = get_bloginfo( 'description' );

	$filtered_title = $title . get_bloginfo( 'name' );
	$filtered_title .= ( ! empty( $site_description ) && ( is_home() || is_front_page() ) ) ? ' | ' . $site_description: '';
	$filtered_title .= ( 2 <= $paged || 2 <= $page ) ? ' | ' . sprintf( __( 'Page %s', 'cian' ), max( $paged, $page ) ) : '';

	return $filtered_title;
}
add_filter( 'wp_title', 'cian_filter_wp_title' );

/**
 * Register navigation location
 */
function cian_action_register_menus() {
	register_nav_menus( array(
		'header-navigation' => __( 'Header Navigation', 'cian' ),
	) );
}
add_action( 'init', 'cian_action_register_menus' );

/**
 * Register sidebars
 */
function cian_register_sidebars() {
	register_sidebar( array(
		'name'          => __( 'Content Sidebar', 'cian' ),
		'id'            => 'content-sidebar',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div id="%1s" class="widget %2s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>'
	) );
	
	for ( $i = 1; $i <= 4; $i++ ) {
		register_sidebar( array(
			'name'          => sprintf( __( 'Footer Column %s', 'cian' ), $i ),
			'id'            => sprintf( 'footer-sidebar-%s', $i ),
			'description'   => '',
			'class'         => '',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}
}
add_action( 'widgets_init', 'cian_register_sidebars' );

/**
 * Widget Text do_shortcode
 */
add_filter( 'widget_text', 'do_shortcode' );

/**
 * Add theme supports
 */
function cian_action_add_theme_supports() {
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
}
add_action( 'after_setup_theme', 'cian_action_add_theme_supports' );

/**
 * Add Twitter Contact Info
 */
function cian_add_user_contactmethods( $contactmethods ) {
	$contactmethods['facebook'] = __( 'Facebook Username', 'cian' );
	$contactmethods['twitter'] = __( 'Twitter Username (without @)', 'cian' );
	$contactmethods['googleplus'] = __( 'Google Plus ID', 'cian' );
	$contactmethods['instagram'] = __( 'Instagram ID', 'cian' );
	return $contactmethods;
}
add_filter( 'user_contactmethods', 'cian_add_user_contactmethods' );

/**
 * Change Widget Search
 */
function cian_change_search_widget( $html ) {
	ob_start(); ?>
	<form role="search" method="get" action="<?php echo esc_url( home_url() ); ?>" class="search-form">
		<input type="text" value="<?php echo get_search_query(); ?>" name="s" placeholder="<?php esc_html_e( 'Search...', 'cian' ); ?>" class="form-control" />
		<button class="btn btn-main css3transition" type="submit">
			<i class="fa fa-search"></i>
			<?php if (is_404()):?>
			<?php esc_html_e( 'Search', 'cian' ); ?>
			<?php endif; ?>
		</button>
	</form>
	<?php return ob_get_clean();
}
add_filter( 'get_search_form', 'cian_change_search_widget' );

/**
 * Add all dropdown widget class with "form-control"
 */
function cian_widget_categories_dropdown_args( $args ) {
	if ( array_key_exists( 'class', $args ) ) {
		$args['class'] .= ' form-control';
	} else {
		$args['class'] = 'form-control';
	}
	return $args;
}
add_filter( 'widget_categories_dropdown_args', 'cian_widget_categories_dropdown_args' );

/**
 * Callback comment item html
 */
function cian_wp_list_comments_callback( $comment, $args, $depth ) {
	include( locate_template( 'comment.php' ) );
}

/**
 * Box Slides
 */
function cian_register_post_type_slide() {
	$labels = array(
		'name'               => _x( 'Slider', 'post type general name', 'cian' ),
		'singular_name'      => _x( 'Slide', 'post type singular name', 'cian' ),
		'menu_name'          => _x( 'Slider', 'admin menu', 'cian' ),
		'name_admin_bar'     => _x( 'Slide', 'add new on admin bar', 'cian' ),
		'add_new'            => _x( 'Add New', 'box slide', 'cian' ),
		'add_new_item'       => __( 'Add New Slide', 'cian' ),
		'new_item'           => __( 'New Slide', 'cian' ),
		'edit_item'          => __( 'Edit Slide', 'cian' ),
		'view_item'          => __( 'View Slide', 'cian' ),
		'all_items'          => __( 'All Slides', 'cian' ),
		'search_items'       => __( 'Search Slides', 'cian' ),
		'parent_item_colon'  => __( 'Parent Slides:', 'cian' ),
		'not_found'          => __( 'No slides found.', 'cian' ),
		'not_found_in_trash' => __( 'No slides found in Trash.', 'cian' ),
	);

	$args = array(
		'labels'              => $labels,
		'description'         => __( 'Slides used in the pages. Edit a page and assign some slides your want to show.' , 'cian' ),
		'public'              => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'show_in_nav_menus'   => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_icon'			  => 'dashicons-slides',
		'capability_type'     => 'page',
		'hierarchical'        => false,
		'supports'            => array( 'title' ),
		'query_var'           => true,
		'can_export'          => true,
	);

	register_post_type( 'cian-slide', $args );
}
add_action( 'init', 'cian_register_post_type_slide' );

/**
 * Excerpt ellipsis
 */
function cian_excerpt_more( $excerpt ) {
	return '&hellip;';
}
add_filter( 'excerpt_more', 'cian_excerpt_more' );

/**
 * Cian Remove Vafpress Portfolio Shortcodes Generator
 */
function cian_vp_pf_remove_sc_generator( $params ) {
	return null;
}
add_filter( 'vp_pf_shortcode_generator_params', 'cian_vp_pf_remove_sc_generator', 1 );

/**
 * Cian Helper Functions
 */
function cian_data_printer( $datas ) {
	$data_str = '';
	foreach ( $datas as $key => $value ) {
		if ( ! empty( $value ) ) {
			$data_str .= " data-{$key}=\"{$value}\"";
		}
	}
	return $data_str;
}
function cian_extract_css( $css ) {

    $results = array();

    preg_match_all( '/(.+?)\s?\{\s?(.+?)\s?\}/', $css, $matches );
    foreach($matches[0] as $i=>$original)
        foreach( explode( ';', $matches[2][$i] ) as $attr )
            if ( strlen( trim( $attr ) ) > 0 ) {
            	// for missing semicolon on last element, which is legal
                list($name, $value) = explode(':', $attr);
                $results[$matches[1][$i]][trim($name)] = trim($value);
            }
    return $results;
}
// credit to http://frankiejarrett.com/get-an-attachment-id-by-url-in-wordpress/
function cian_get_attachment_id_by_url( $url ) {

	// Split the $url into two parts with the wp-content directory as the separator.
	$parse_url  = explode( parse_url( WP_CONTENT_URL, PHP_URL_PATH ), $url );

	// Get the host of the current site and the host of the $url, ignoring www.
	$this_host = str_ireplace( 'www.', '', parse_url( home_url(), PHP_URL_HOST ) );
	$file_host = str_ireplace( 'www.', '', parse_url( $url, PHP_URL_HOST ) );

	// Return nothing if there aren't any $url parts or if the current host and $url host do not match.
	if ( ! isset( $parse_url[1] ) || empty( $parse_url[1] ) || ( $this_host != $file_host ) ) {
		return;
	}

	// Now we're going to quickly search the DB for any attachment GUID with a partial path match.
	// Example: /uploads/2013/05/test-image.jpg
	global $wpdb;

	$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}posts WHERE guid RLIKE %s;", $parse_url[1] ) );

	// Returns null if no attachment is found.
	return $attachment[0];
}

/**
 * Get current post / page thumbnail for sharing purpose (pinterest must use image)
 */
function cian_get_share_thumbnail( $post_id = null ) {
	return wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );
}

/**
 * Append dependency to registered script
 */
function cian_append_dependency( $handle, $dep ){
    global $wp_scripts;

    $script = $wp_scripts->query( $handle, 'registered' );
    if( !$script )
        return false;

    if( !in_array( $dep, $script->deps ) ){
        $script->deps[] = $dep;
    }

    return true;
}

/**
 * KSES
 */
function cian_kses( $html ) {
	$allow = array_merge( wp_kses_allowed_html( 'post' ), array(
		'link' => array(
			'href'    => true,
			'rel'     => true,
			'type'    => true,
		),
		'script' => array(
			'src' => true,
			'charset' => true,
			'type'    => true,
		)
	) );
	return wp_kses( $html, $allow );
}

/**
 * Simple Like's Plugin
 */
function sl_enqueue_scripts() {
	wp_register_script( 'simple-likes-public-js', CIAN_JS.'simple-likes-public.js', array('jquery') );
	wp_localize_script( 'simple-likes-public-js', 'simpleLikes', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'like' => __( 'Like', 'cian' ),
			'unlike' => __( 'Unlike', 'cian' )
	) );
	wp_enqueue_script( 'simple-likes-public-js' );
}
add_action( 'wp_enqueue_scripts', 'sl_enqueue_scripts' );

/**
 * Processes like/unlike
 * @since    0.5
*/
add_action( 'wp_ajax_nopriv_process_simple_like', 'process_simple_like' );
add_action( 'wp_ajax_process_simple_like', 'process_simple_like' );
function process_simple_like() {
	// Security
	$nonce = isset( $_REQUEST['nonce'] ) ? sanitize_text_field( $_REQUEST['nonce'] ) : 0;
	if ( !wp_verify_nonce( $nonce, 'simple-likes-nonce' ) ) {
		exit( __( 'Not permitted', 'cian' ) );
	}
	// Test if javascript is disabled
	$disabled = ( isset( $_REQUEST['disabled'] ) && $_REQUEST['disabled'] == true ) ? true : false;
	// Test if this is a comment
	$is_comment = ( isset( $_REQUEST['is_comment'] ) && $_REQUEST['is_comment'] == 1 ) ? 1 : 0;
	// Base variables
	$post_id = ( isset( $_REQUEST['post_id'] ) && is_numeric( $_REQUEST['post_id'] ) ) ? $_REQUEST['post_id'] : '';
	$result = array();
	$post_users = NULL;
	$like_count = 0;
	// Get plugin options
	if ( $post_id != '' ) {
		$count = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_comment_like_count", true ) : get_post_meta( $post_id, "_post_like_count", true ); // like count
		$count = ( isset( $count ) && is_numeric( $count ) ) ? $count : 0;
		if ( !already_liked( $post_id, $is_comment ) ) { // Like the post
			if ( is_user_logged_in() ) { // user is logged in
				$user_id = get_current_user_id();
				$post_users = post_user_likes( $user_id, $post_id, $is_comment );
				if ( $is_comment == 1 ) {
					// Update User & Comment
					$user_like_count = get_user_option( "_comment_like_count", $user_id );
					$user_like_count =  ( isset( $user_like_count ) && is_numeric( $user_like_count ) ) ? $user_like_count : 0;
					update_user_option( $user_id, "_comment_like_count", ++$user_like_count );
					if ( $post_users ) {
						update_comment_meta( $post_id, "_user_comment_liked", $post_users );
					}
				} else {
					// Update User & Post
					$user_like_count = get_user_option( "_user_like_count", $user_id );
					$user_like_count =  ( isset( $user_like_count ) && is_numeric( $user_like_count ) ) ? $user_like_count : 0;
					update_user_option( $user_id, "_user_like_count", ++$user_like_count );
					if ( $post_users ) {
						update_post_meta( $post_id, "_user_liked", $post_users );
					}
				}
			} else { // user is anonymous
				$user_ip = sl_get_ip();
				$post_users = post_ip_likes( $user_ip, $post_id, $is_comment );
				// Update Post
				if ( $post_users ) {
					if ( $is_comment == 1 ) {
						update_comment_meta( $post_id, "_user_comment_IP", $post_users );
					} else {
						update_post_meta( $post_id, "_user_IP", $post_users );
					}
				}
			}
			$like_count = ++$count;
			$response['status'] = "liked";
			$response['icon'] = get_liked_icon($style);
		} else { // Unlike the post
			if ( is_user_logged_in() ) { // user is logged in
				$user_id = get_current_user_id();
				$post_users = post_user_likes( $user_id, $post_id, $is_comment );
				// Update User
				if ( $is_comment == 1 ) {
					$user_like_count = get_user_option( "_comment_like_count", $user_id );
					$user_like_count =  ( isset( $user_like_count ) && is_numeric( $user_like_count ) ) ? $user_like_count : 0;
					if ( $user_like_count > 0 ) {
						update_user_option( $user_id, "_comment_like_count", --$user_like_count );
					}
				} else {
					$user_like_count = get_user_option( "_user_like_count", $user_id );
					$user_like_count =  ( isset( $user_like_count ) && is_numeric( $user_like_count ) ) ? $user_like_count : 0;
					if ( $user_like_count > 0 ) {
						update_user_option( $user_id, '_user_like_count', --$user_like_count );
					}
				}
				// Update Post
				if ( $post_users ) {
					$uid_key = array_search( $user_id, $post_users );
					unset( $post_users[$uid_key] );
					if ( $is_comment == 1 ) {
						update_comment_meta( $post_id, "_user_comment_liked", $post_users );
					} else {
						update_post_meta( $post_id, "_user_liked", $post_users );
					}
				}
			} else { // user is anonymous
				$user_ip = sl_get_ip();
				$post_users = post_ip_likes( $user_ip, $post_id, $is_comment );
				// Update Post
				if ( $post_users ) {
					$uip_key = array_search( $user_ip, $post_users );
					unset( $post_users[$uip_key] );
					if ( $is_comment == 1 ) {
						update_comment_meta( $post_id, "_user_comment_IP", $post_users );
					} else {
						update_post_meta( $post_id, "_user_IP", $post_users );
					}
				}
			}
			$like_count = ( $count > 0 ) ? --$count : 0; // Prevent negative number
			$response['status'] = "unliked";
			$response['icon'] = get_unliked_icon();
		}
		if ( $is_comment == 1 ) {
			update_comment_meta( $post_id, "_comment_like_count", $like_count );
			update_comment_meta( $post_id, "_comment_like_modified", date( 'Y-m-d H:i:s' ) );
		} else {
			update_post_meta( $post_id, "_post_like_count", $like_count );
			update_post_meta( $post_id, "_post_like_modified", date( 'Y-m-d H:i:s' ) );
		}
		$response['count'] = get_like_count( $like_count );
		$response['testing'] = $is_comment;
		if ( $disabled == true ) {
			if ( $is_comment == 1 ) {
				wp_redirect( get_permalink( get_the_ID() ) );
				exit();
			} else {
				wp_redirect( get_permalink( $post_id ) );
				exit();
			}
		} else {
			wp_send_json( $response );
		}
	}
}

/**
 * Utility to test if the post is already liked
 * @since    0.5
 */
function already_liked( $post_id, $is_comment ) {
	$post_users = NULL;
	$user_id = NULL;
	if ( is_user_logged_in() ) { // user is logged in
		$user_id = get_current_user_id();
		$post_meta_users = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_user_comment_liked" ) : get_post_meta( $post_id, "_user_liked" );
		if ( count( $post_meta_users ) != 0 ) {
			$post_users = $post_meta_users[0];
		}
	} else { // user is anonymous
		$user_id = sl_get_ip();
		$post_meta_users = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_user_comment_IP" ) : get_post_meta( $post_id, "_user_IP" );
		if ( count( $post_meta_users ) != 0 ) { // meta exists, set up values
			$post_users = $post_meta_users[0];
		}
	}
	if ( is_array( $post_users ) && in_array( $user_id, $post_users ) ) {
		return true;
	} else {
		return false;
	}
} // already_liked()

/**
 * Output the like button
 * @since    0.5
 */
function get_simple_likes_button( $post_id, $style, $is_comment = NULL ) {
	$is_comment = ( NULL == $is_comment ) ? 0 : 1;
	$output = '';
	$nonce = wp_create_nonce( 'simple-likes-nonce' ); // Security
	if ( $is_comment == 1 ) {
		$post_id_class = esc_attr( ' sl-comment-button-' . $post_id );
		$comment_class = esc_attr( ' sl-comment' );
		$like_count = get_comment_meta( $post_id, "_comment_like_count", true );
		$like_count = ( isset( $like_count ) && is_numeric( $like_count ) ) ? $like_count : 0;
	} else {
		$post_id_class = esc_attr( ' sl-button-' . $post_id );
		$comment_class = esc_attr( '' );
		$like_count = get_post_meta( $post_id, "_post_like_count", true );
		$like_count = ( isset( $like_count ) && is_numeric( $like_count ) ) ? $like_count : 0;
	}
	$count = get_like_count( $like_count );
	$icon_empty = get_unliked_icon($style);
	$icon_full = get_liked_icon($style);
	// Loader
	$loader = '<span id="sl-loader"></span>';
	// Liked/Unliked Variables
	if ( already_liked( $post_id, $is_comment ) ) {
		$class = esc_attr( ' liked' );
		$title = __( 'Unlike', 'cian' );
		$icon = $icon_full;
	} else {
		$class = '';
		$title = __( 'Like', 'cian' );
		$icon = $icon_empty;
	}
	$output = '<a href="' . admin_url( 'admin-ajax.php?action=process_simple_like' . '&nonce=' . $nonce . '&post_id=' . $post_id . '&disabled=true&is_comment=' . $is_comment ) . '" class="sl-button btn-like-portfolio' . $post_id_class . $class . $comment_class . '" data-nonce="' . $nonce . '" data-post-id="' . $post_id . '" data-iscomment="' . $is_comment . '" title="' . $title . '">'  . $icon . ' ' . $count . '</a>' . $loader;
	
	return $output;
} // get_simple_likes_button()

/**
 * Processes shortcode to manually add the button to posts
 * @since    0.5
 */
add_shortcode( 'jmliker', 'sl_shortcode' );
function sl_shortcode() {
	return get_simple_likes_button( get_the_ID(), 0 );
} // shortcode()

/**
 * Utility retrieves post meta user likes (user id array),
 * then adds new user id to retrieved array
 * @since    0.5
 */
function post_user_likes( $user_id, $post_id, $is_comment ) {
	$post_users = '';
	$post_meta_users = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_user_comment_liked" ) : get_post_meta( $post_id, "_user_liked" );
	if ( count( $post_meta_users ) != 0 ) {
		$post_users = $post_meta_users[0];
	}
	if ( !is_array( $post_users ) ) {
		$post_users = array();
	}
	if ( !in_array( $user_id, $post_users ) ) {
		$post_users['user-' . $user_id] = $user_id;
	}
	return $post_users;
} // post_user_likes()

/**
 * Utility retrieves post meta ip likes (ip array),
 * then adds new ip to retrieved array
 * @since    0.5
 */
function post_ip_likes( $user_ip, $post_id, $is_comment ) {
	$post_users = '';
	$post_meta_users = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_user_comment_IP" ) : get_post_meta( $post_id, "_user_IP" );
	// Retrieve post information
	if ( count( $post_meta_users ) != 0 ) {
		$post_users = $post_meta_users[0];
	}
	if ( !is_array( $post_users ) ) {
		$post_users = array();
	}
	if ( !in_array( $user_ip, $post_users ) ) {
		$post_users['ip-' . $user_ip] = $user_ip;
	}
	return $post_users;
} // post_ip_likes()

/**
 * Utility to retrieve IP address
 * @since    0.5
 */
function sl_get_ip() {
	if ( isset( $_SERVER['HTTP_CLIENT_IP'] ) && ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) && ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = ( isset( $_SERVER['REMOTE_ADDR'] ) ) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
	}
	$ip = filter_var( $ip, FILTER_VALIDATE_IP );
	$ip = ( $ip === false ) ? '0.0.0.0' : $ip;
	return $ip;
} // sl_get_ip()

/**
 * Utility returns the button icon for "like" action
 * @since    0.5
 */
function get_liked_icon() {
	$icon = '<span class="fa fa-heart"></span>';
	return $icon;
} // get_liked_icon()

/**
 * Utility returns the button icon for "unlike" action
 * @since    0.5
 */
function get_unliked_icon() {
	$icon = '<span class="fa fa-heart-o"></span>';
	return $icon;
} // get_unliked_icon()

/**
 * Utility function to format the button count,
 * appending "K" if one thousand or greater,
 * "M" if one million or greater,
 * and "B" if one billion or greater (unlikely).
 * $precision = how many decimal points to display (1.25K)
 * @since    0.5
 */
function sl_format_count( $number ) {
	$precision = 2;
	if ( $number >= 1000 && $number < 1000000 ) {
		$formatted = number_format( $number/1000, $precision ).'K';
	} else if ( $number >= 1000000 && $number < 1000000000 ) {
		$formatted = number_format( $number/1000000, $precision ).'M';
	} else if ( $number >= 1000000000 ) {
		$formatted = number_format( $number/1000000000, $precision ).'B';
	} else {
		$formatted = $number; // Number is less than 1000
	}
	$formatted = str_replace( '.00', '', $formatted );
	return $formatted;
} // sl_format_count()

/**
 * Utility retrieves count plus count options,
 * returns appropriate format based on options
 * @since    0.5
 */
function get_like_count( $like_count ) {
	$count = '<span class="sl-count"> ' . sl_format_count( $like_count ) . '</span>';
	return $count;
} // get_like_count()

// User Profile List
add_action( 'show_user_profile', 'show_user_likes' );
add_action( 'edit_user_profile', 'show_user_likes' );
function show_user_likes( $user ) { ?>
	<table class="form-table">
		<tr>
			<th><label for="user_likes"><?php _e( 'You Like:', 'cian' ); ?></label></th>
			<td>
			<?php
			$types = get_post_types( array( 'public' => true ) );
			$args = array(
			  'numberposts' => -1,
			  'post_type' => $types,
			  'meta_query' => array (
				array (
				  'key' => '_user_liked',
				  'value' => $user->ID,
				  'compare' => 'LIKE'
				)
			  ) );		
			$sep = '';
			$like_query = new WP_Query( $args );
			if ( $like_query->have_posts() ) : ?>
			<p>
			<?php while ( $like_query->have_posts() ) : $like_query->the_post(); 
			echo $sep; ?><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
			<?php
			$sep = ' &middot; ';
			endwhile; 
			?>
			</p>
			<?php else : ?>
			<p><?php _e( 'You do not like anything yet.', 'cian' ); ?></p>
			<?php 
			endif; 
			wp_reset_postdata(); 
			?>
			</td>
		</tr>
	</table>
<?php } // show_user_likes()