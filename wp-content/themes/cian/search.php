<?php
/**
 * The template for the search results
 * 
 * @package CIAN
 * @since CIAN 2.0
 */ 

get_header();

$show_sidebar = cian_sidebar_enable(); ?>

<section id="content" class="content-section section">
	<div class="container container-table">
		<div class="row">
		
			<div class="<?php if ( $show_sidebar ) { echo 'col-md-9 '; } else { echo 'col-md-12 '; } ?>main-section" role="main">	
				<div class="search-info">
					<div class="keyword"><?php printf( __( 'Search results for "%s"', 'cian' ), '<strong>' . get_search_query() . '</strong>' ); ?></div>
					<?php
					global $wp_query, $paged, $posts_per_page; //var_dump($wp_query);
					$start = ( ( $paged == 0 ) ? 0 : $paged - 1 ) * $posts_per_page + 1;
					$end = $start + $wp_query->post_count - 1;
					?>
					<div class="count"><?php printf( __( 'Showing results %s - %s of %s results found', 'cian' ), $start, $end, $wp_query->found_posts ); ?></div>
				</div>
				<div class="search-loop">
				<?php if ( have_posts() ) : while( have_posts() ) : the_post() ; ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'search-post' ); ?>>
						<div class="post-meta-date"><a href="<?php the_permalink(); ?>"><?php echo get_the_date(); ?></a></div>
						<h3 class="post-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
						<div class="post-content"><?php echo '<p>'.cian_get_my_excerpt(70).'</p>'; ?></div>
					</article>

				<?php endwhile; else: ?>

					<p><?php _e( 'Sorry, no results found, please try another keyword', 'cian' ); ?></p>

				<?php 
				endif;
				cian_numeric_posts_nav(); 
				wp_reset_postdata();
				?>
				</div>
			</div>
			
			<?php if ( $show_sidebar ): ?>
			<?php get_sidebar(); ?>
			<?php endif; ?>
			
		</div>

	</div>
</section>

<?php if ( cian_option( 'enable_preloader', 1 ) ) : ?>
	<div id="jpreContent">
		<div id="loading-center"></div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>