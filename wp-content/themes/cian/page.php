<?php 

get_header();

$is_using_vc = get_post_meta( get_the_ID(), '_wpb_vc_js_status', true );

if ( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<?php if ( $is_using_vc === 'true' ) : ?>

		<div id="content" <?php post_class( 'visual-composer-page' ); ?>>

			<?php the_content(); ?>

		</div>

	<?php else : ?>

		<section id="content" class="content-section section">
			<div class="container container-table">

				<div class="main-section" role="main">
					
					<div class="page-loop">

						<article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
							<h3 class="page-title"><?php the_title(); ?></h3>
							<?php the_content(); ?>
						</article>

					</div>

					<?php comments_template(); ?>

				</div>

			</div>
		</section>

	<?php endif; ?>

<?php endwhile; endif; ?>

<?php if ( cian_option( 'enable_preloader', 1 ) ) : ?>
	<div id="jpreContent">
		<div id="loading-center"></div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>