<?php

/**
 * ======================================================================================
 * Functions
 * ======================================================================================
 */

function cian_theme_options_scheme_social_media_links() {
	$return = array();

	foreach ( cian_data_source_social_media_links() as $link ) {
		$return[] = array(
			'type'       => 'textbox',
			'name'       => 'social_media_' . $link['value'],
			'label'      => $link['label'],
		);
	}

	return $return;
}

/**
 * ======================================================================================
 * Scheme
 * ======================================================================================
 */

return array(
	'title' => __( 'Cian Theme Options', 'cian' ),
	'logo'  => ( cian_option( 'admin_logo' ) ) ? cian_option( 'admin_logo' ) : CIAN_IMAGES . 'cian_logo_admin.png',
	'menus' => array(
		array(
			'title'    => __( 'General', 'cian' ),
			'icon'     => 'font-awesome:fa fa-globe',
			'controls' => array(
				array(
					'type'    => 'toggle',
					'name'    => 'enable_preloader',
					'label'   => __( 'Page Preloader', 'cian' ),
					'default' => 1,
				),
				array(
					'type'   => 'section',
					'title'  => __( 'Logo', 'cian' ),
					'fields' => array(
						array(
							'type'        => 'upload',
							'name'        => 'admin_logo',
							'label'       => __( 'Admin Logo', 'cian' ),
						),
						array(
							'type'        => 'upload',
							'name'        => 'header_logo',
							'label'       => __( 'Header Logo', 'cian' ),
							'default' 	  => 'http://www.creabox.es/themes/wp_cian/wp-content/uploads/2016/08/cian_logo_header.png',
						),
						array(
							'type'        => 'upload',
							'name'        => 'header_logo_retina',
							'label'       => __( 'Header Logo (Retina Version)', 'cian' ),
							'default' 	  => 'http://www.creabox.es/themes/wp_cian/wp-content/uploads/2016/08/cian_logo_header.png',
							'description' => __( 'Please name your file following the normal version (e.g. logo.png) with a suffix @2x (e.g. logo@2x.png)', 'cian' ),
						),
					),
				),
				array(
					'type'   => 'section',
					'title'  => __( 'Favicon', 'cian' ),
					'fields' => array(
						array(
							'type'        => 'upload',
							'name'        => 'favicon',
							'description' => esc_html__( 'Recommended: .PNG 16x16px size', 'cian' ),
							'label'       => esc_html__( 'General Site Icon', 'cian' ),
							'default'	  => 'http://www.creabox.es/themes/wp_cian/wp-content/uploads/2015/05/favicon.png',
							'description' => esc_html__( 'This favicon image will be loaded in wordpress versions 4.3 previous. For versions 4.3+, the favicon image has to be uploaded from the page Appearance > Customize, in the option Site Identity > Site Icon.', 'cian' ),
						),
					),
				),
			),
		),
		array(
			'title'    => __( 'Styles', 'cian' ),
			'icon'     => 'font-awesome:fa-adjust',
			'controls' => array(
				array(
					'type'       => 'select',
					'name'       => 'main_typograhpy',
					'label'      => __( 'Main Typography', 'cian' ),
					'items'      => array(
						'data' => array(
							array(
								'source' => 'function',
								'value'  => 'vp_get_gwf_family',
							),
						),
					),
					'default'    => 'Lato',
					'validation' => 'required',
				),
				array(
					'type'       => 'select',
					'name'       => 'accent_typograhpy',
					'label'      => __( 'Accent Typography', 'cian' ),
					'items'      => array(
						'data' => array(
							array(
								'source' => 'function',
								'value'  => 'vp_get_gwf_family',
							),
						),
					),
					'default'    => 'Pacifico',
					'validation' => 'required',
				),
				array(
					'type'    => 'color',
					'name'    => 'main_color',
					'label'   => __( 'Main Color', 'cian' ),
					'format'  => 'hex',
					'default' => '#818A90',
				),
				array(
					'type'    => 'color',
					'name'    => 'heading_color',
					'label'   => __( 'Heading Color', 'cian' ),
					'format'  => 'hex',
					'default' => '#4e565c',
				),
				array(
					'type'    => 'color',
					'name'    => 'accent_color',
					'label'   => __( 'Accent Color', 'cian' ),
					'format'  => 'hex',
					'default' => '#00cccc',
				),
				array(
					'type'    => 'color',
					'name'    => 'light_color',
					'label'   => __( 'Light Color', 'cian' ),
					'format'  => 'hex',
					'default' => '#ffffff',
				),
			)
		),
		array(
			'title'    => __( 'Sidebar', 'cian' ),
			'icon'     => 'font-awesome:fa-columns',
			'controls' => array(
				array(
					'type'    => 'checkbox',
					'name'    => 'pages_with_sidebar',
					'label'   => __( 'Enable Sidebar in', 'cian' ),
					'items'   => array(
						array(
							'label' 	=> __( 'Blog Index (default Posts page)', 'cian' ),
							'value' 	=> 'blog_index',
						),
						array(
							'label' 	=> __( 'Blog Archive (category, tag, author, date archive)', 'cian' ),
							'value' 	=> 'blog_archive',
						),
						array(
							'label' 	=> __( 'Blog Single (can be overrided via single post editor\'s metabox)', 'cian' ),
							'value' 	=> 'blog_single',
						),
						array(
							'label' 	=> __( 'Search Result', 'cian' ),
							'value' 	=> 'search',
						),
					),
				),
			),
		),
		array(
			'title'    => __( 'Blog Settings', 'cian' ),
			'icon'     => 'font-awesome:fa-cogs',
			'controls' => array(
				array(
					'type'    => 'textbox',
					'name'    => 'blog_title',
					'label'   => __( 'Blog Title', 'cian' ),
					'default' => 'Blog',
				),
				array(
					'type'    => 'toggle',
					'name'    => 'enable_blog_title',
					'label'   => __( 'Enable blog title', 'cian' ),
					'default' => 1,
				),
				array(
					'type'    => 'toggle',
					'name'    => 'enable_social_share_blog',
					'label'   => __( 'Enable Social Share in Blog', 'cian' ),
					'default' => 1,
				),
				array(
					'type'    => 'toggle',
					'name'    => 'enable_related_posts',
					'label'   => __( 'Enable Related Posts', 'cian' ),
					'default' => 1,
				),
				array(
					'type'		  => 'radioimage',
					'name' 		  => 'blog_style',
					'label' 	  => esc_html__('Blog layout', 'cian'),
					'description' => esc_html__('Select the style you want for the blog', 'cian'),
					'item_max_width'    => '190',
					'item_max_height'   => '150',
					'items' => array(
						array(
							'value' => 'full',
							'label' => esc_html__('Full version', 'cian'),
							'img' => '../../../images/blog/full.png',
						),
						array(
							'value' => 'masonry',
							'label' => esc_html__('Masonry version', 'cian'),
							'img' => '../../../images/blog/masonry.png',
						),
					),
					'default' => array(
						'full',
					),
				),
				array(
					'type'		  => 'radioimage',
					'name' 		  => 'archive_style',
					'label' 	  => esc_html__('Archive layout', 'cian'),
					'description' => esc_html__('Select the style you want for the archive page', 'cian'),
					'item_max_width'    => '190',
					'item_max_height'   => '150',
					'items' => array(
						array(
							'value' => 'full',
							'label' => esc_html__('Full version', 'cian'),
							'img' => '../../../images/blog/full.png',
						),
						array(
							'value' => 'masonry',
							'label' => esc_html__('Masonry version', 'cian'),
							'img' => '../../../images/blog/masonry.png',
						),
					),
					'default' => array(
						'masonry',
					),
				),
				array(
					'type'    => 'sorter',
					'name'    => 'social_share_links',
					'label'   => __( 'Social Share Links', 'cian' ),
					'items'   => array(
						'data' => array(
							array(
								'source' => 'function',
								'value'  => 'cian_data_source_social_share_links',
							),
						),
					),
				),
			),
		),
		array(
			'title'    => __( 'Footer widgets', 'cian' ),
			'icon'     => 'font-awesome:fa-align-justify',
			'controls' => array(
				array(
					'type'    => 'toggle',
					'name'    => 'enable_footer_widgets',
					'label'   => __( 'Enable Footer Widgets', 'cian' ),
					'default' => 1,
				),
				array(
					'type'    => 'slider',
					'name'    => 'footer_number_of_columns',
					'label'   => __( 'Number of Columns', 'cian' ),
					'min'     => 1,
					'max'     => 4,
					'default' => 3,
				),
				array(
					'type' => 'section',
					'title' => __( 'Column 1', 'cian' ),
					'fields' => array(
						array(
							'type' => 'slider',
							'name' => 'footer_grid_column_1',
							'label' => __( 'Grid of Column 1', 'cian' ),
							'min' => '1',
							'max' => '12',
							'default' => '3',
						),
						array(
							'type' => 'slider',
							'name' => 'footer_offset_column_1',
							'label' => __( 'Offset of Column 1', 'cian' ),
							'min' => '0',
							'max' => '11',
							'default' => '0',
						),
					),
				),
				array(
					'type' => 'section',
					'title' => __( 'Column 2', 'cian' ),
					'fields' => array(
						array(
							'type' => 'slider',
							'name' => 'footer_grid_column_2',
							'label' => __( 'Grid of Column 2', 'cian' ),
							'min' => '1',
							'max' => '12',
							'default' => '3',
						),
						array(
							'type' => 'slider',
							'name' => 'footer_offset_column_2',
							'label' => __( 'Offset of Column 2', 'cian' ),
							'min' => '0',
							'max' => '11',
							'default' => '0',
						),
					),
				),
				array(
					'type' => 'section',
					'title' => __( 'Column 3', 'cian' ),
					'fields' => array(
						array(
							'type' => 'slider',
							'name' => 'footer_grid_column_3',
							'label' => __( 'Grid of Column 3', 'cian' ),
							'min' => '1',
							'max' => '12',
							'default' => '6',
						),
						array(
							'type' => 'slider',
							'name' => 'footer_offset_column_3',
							'label' => __( 'Offset of Column 3', 'cian' ),
							'min' => '0',
							'max' => '11',
							'default' => '0',
						),
					),
				),
				array(
					'type' => 'section',
					'title' => __( 'Column 4', 'cian' ),
					'fields' => array(
						array(
							'type' => 'slider',
							'name' => 'footer_grid_column_4',
							'label' => __( 'Grid of Column 4', 'cian' ),
							'min' => '1',
							'max' => '12',
							'default' => '3',
						),
						array(
							'type' => 'slider',
							'name' => 'footer_offset_column_4',
							'label' => __( 'Offset of Column 4', 'cian' ),
							'min' => '0',
							'max' => '11',
							'default' => '0',
						),
					),
				),
			),
		),
		array(
			'title'    => __( 'Footer & copyright', 'cian' ),
			'icon'     => 'font-awesome:fa-arrow-circle-down',
			'controls' => array(
				array(
				    'type' => 'radiobutton',
				    'name' => 'footer_style',
				    'label' => __('Style', 'vp_textdomain'),
				    'items' => array(
				        array(
				            'value' => 'light',
				            'label' => __('Light', 'vp_textdomain'),
				        ),
				        array(
				            'value' => 'dark',
				            'label' => __('Dark', 'vp_textdomain'),
				        ),
				    ),
				    'default' => 'dark',
				),		
				array(
					'type'        => 'upload',
					'name'        => 'footer_logo',
					'label'       => __( 'Footer Logo', 'cian' ),
					'default' 	  => 'http://www.creabox.es/themes/wp_cian/wp-content/uploads/2016/08/cian_logo_footer.png',
				),
				array(
					'type'        => 'upload',
					'name'        => 'footer_logo_retina',
					'label'       => __( 'Footer Logo (Retina Version)', 'cian' ),
					'description' => __( 'Please name your file following the normal version (e.g. logo.png) with a suffix @2x (e.g. logo@2x.png)', 'cian' ),
					'default' 	  => 'http://www.creabox.es/themes/wp_cian/wp-content/uploads/2016/08/cian_logo_footer.png',
				),
				array(
					'type'    => 'sorter',
					'name'    => 'footer_social_icons',
					'label'   => __( 'Footer Social Share', 'cian' ),
					'items'   => array(
						'data' => array(
							array(
								'source' => 'function',
								'value'  => 'cian_data_source_social_media_links',
							),
						),
					),
				),
				array(
					'type'    => 'textarea',
					'name'    => 'copyright_text',
					'label'   => __( 'Copyright', 'cian' ),
					'default' => 'Copyright &#169; 2016 Cian - All rights reserved',
				),
				array(
					'type'    => 'toggle',
					'name'    => 'enable_top_button',
					'label'   => __( 'Enable Top Button', 'cian' ),
					'default' => 1,
				),
			),
		),
		array(
			'title'    => __( 'Social Media Links', 'cian' ),
			'icon'     => 'font-awesome:fa-twitter',
			'controls' => array(
				array(
					'type'   => 'section',
					'title'  => __( 'Social Media Links', 'cian' ),
					'fields' => cian_theme_options_scheme_social_media_links(),
				),
			),
		),
		array(
			'title' => __( 'Custom Scripts', 'cian' ),
			'name' => 'menu_scripts',
			'icon' => 'font-awesome:fa-code',
			'controls' => array(
				array(
					'type' => 'section',
					'title' => esc_html__( 'Custom script', 'cian' ),
					'fields' => array(
						array(
							'type' => 'textarea',
							'name' => 'custom_script',
							'label' => esc_html__( 'Custom Script', 'cian' ),
							'description' => esc_html__( 'Add the custom script you want without the &#60;script&#62; tags', 'cian' ),
						),
					),
				),
				array(
					'type' => 'section',
					'title' => __( 'Custom CSS', 'cian' ),
					'fields' => array(
						array(
							'type' => 'textarea',
							'name' => 'custom_css',
							'label' => __( 'Custom CSS', 'cian' ),
							'default' => '
@media (min-width: 992px) {
     #spotlight1{ height: 400px; }
     #spotlight2{ height: 600px; }
}'
						),
					),
				),
			),
		),
	),
);