<?php

return array(
	'id'          => '_post_options',
	'types'       => array_merge( array( 'post' ) ),
	'title'       => __( 'Post Options', 'cian' ),
	'template'    => array(
		array(
			'type'        => 'radiobutton',
			'name'        => 'enable_sidebar',
			'label'       => __( 'Enable Sidebar', 'cian' ),
			'items'       => array(
				array(
					'label' => __( 'Inherit from Theme Options', 'cian' ),
					'value' => -1,
				),
				array(
					'label' => __( 'Disabled', 'cian' ),
					'value' => 0,
				),
				array(
					'label' => __( 'Enabled', 'cian' ),
					'value' => 1,
				),
			),
			'default'     => array(
				-1,
			),
		),
	),
);