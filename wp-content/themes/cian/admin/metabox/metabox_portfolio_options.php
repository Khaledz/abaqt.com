<?php

return array(
	'id'          => '_portfolio_options',
	'types'       => array( 'portfolio' ),
	'title'       => __( 'Portfolio Options', 'cian' ),
	'context'     => 'side',
	'template'    => array(
		array(
			'type'        => 'toggle',
			'name'        => 'enable_custom_layout',
			'label'       => __( 'Enable Custom Layout', 'cian' ),
			'description' => __( 'Using custom layout, you can freely create your own portfolio page with Page Builder. Please make sure you have already checked "Portfolio" post type in Visual Composer Settings page.', 'cian' ),
			'default'     => 0,
		),
		array(
			'type'        => 'radiobutton',
			'name'        => 'images_layout',
			'label'       => __( 'Images Layout', 'cian' ),
			'items'       => array(
				array(
					'label' => __( 'Default', 'cian' ),
					'value' => 'default',
				),
				array(
					'label' => __( 'Tiled Layout', 'cian' ),
					'value' => 'tiled',
				),
				array(
					'label' => __( 'Slider', 'cian' ),
					'value' => 'slider',
				),
			),
			'default'     => array(
				'default',
			),
			'dependency'  => array(
				'field'    => 'enable_custom_layout',
				'function' => 'cian_dependency_portfolio_default_layout',
			),
		),
		array(
			'type'        => 'radiobutton',
			'name'        => 'thumbnail_size',
			'label'       => __( 'Thumbail size', 'cian' ),
			'items'       => array(
				array(
					'label' => __( 'Small', 'cian' ),
					'value' => 'small',
				),
				array(
					'label' => __( 'Big', 'cian' ),
					'value' => 'big',
				),
			),
			'default'     => array(
				'small',
			),
		),
		array(
			'type'       => 'textarea',
			'name'       => 'thumbnail_description',
			'label'      => __( 'Thumbnail description (Blog grid)', 'cian' ),
			'default'    => __( 'The awesome project description', 'cian' ),
		),
	),
);