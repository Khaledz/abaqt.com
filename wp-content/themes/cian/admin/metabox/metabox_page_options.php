<?php

return array(
	'id'          => '_page_options',
	'types'       => array_merge( array( 'page' ) ),
	'title'       => __( 'Page Options', 'cian' ),
	'template'    => array(
		array(
			'type'    => 'toggle',
			'name'    => 'enable_slider_section',
			'label'   => __( 'Enable Slider Section', 'cian' ),
			'default' => 0,
		),
		array(
			'type'      => 'group',
			'repeating' => false,
			'length'    => 1,
			'name'      => 'slider_section',
			'title'     => __( 'Slider Section', 'cian' ),
			'fields'    => array(
				array(
					'type'        => 'sorter',
					'name'        => 'slides',
					'label'       => __( 'Pick Slides', 'cian' ),
					'items'       => array(
						'data' => array(
							array(
								'source' => 'function',
								'value'  => 'cian_data_source_slides',
							),
						),
					),
				),
				array(
					'type'        => 'radiobutton',
					'name'        => 'background_overlay',
					'label'       => __( 'Background Overlay', 'cian' ),
					'items'       => array(
						'data' => array(
							array(
								'source' => 'function',
								'value'  => 'cian_data_source_background_overlay',
							),
						),
					),
					'default'     => array(
						'',
					),
				),
				array(
					'type'       => 'textbox',
					'name'       => 'interval',
					'label'      => __( 'Autoslide Interval (ms)', 'cian' ),
					'default'    => 0,
					'validation' => 'numeric'
				),
				array(
					'type'    => 'toggle',
					'name'    => 'enable_parallax',
					'label'   => __( 'Enable Parallax', 'cian' ),
					'default' => 1,
				),
				array(
					'type'    => 'toggle',
					'name'    => 'enable_button_down',
					'label'   => __( 'Enable button to go down', 'cian' ),
					'default' => 1,
				),
			),
			'dependency' => array(
				'field'    => 'enable_slider_section',
				'function' => 'vp_dep_boolean',
			),
		),
	),
);