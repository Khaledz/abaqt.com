<?php

return array(
	'id'          => '_slide_options',
	'types'       => array( 'cian-slide' ),
	'title'       => __( 'Slide Options', 'cian' ),
	'template'    => array(
		array(
			'type'        => 'radiobutton',
			'name'        => 'background_media',
			'label'       => __( 'Background Media', 'cian' ),
			'items'       => array(
				array(
					'label' => __( 'image', 'cian' ),
					'value' => 'image',
				),
				array(
					'label' => __( 'video', 'cian' ),
					'value' => 'video',
				),
			),
			'default'     => 'image',
		),
		array(
			'type'        => 'upload',
			'name'        => 'background_image',
			'label'       => __( 'Background Image', 'cian' ),
			'dependency'  => array(
				'field'    => 'background_media',
				'function' => 'cian_dependency_slide_background_image',
			),
			'validation'  => 'required',
		),
		array(
			'type'        => 'radiobutton',
			'name'        => 'background_repeat',
			'label'       => __( 'Background Repeat', 'cian' ),
			'items'       => array(
				'data' => array(
					array(
						'source' => 'function',
						'value'  => 'cian_data_source_background_repeat',
					),
				),
			),
			'default'     => array(
				'',
			),
			'dependency'  => array(
				'field'    => 'background_media',
				'function' => 'cian_dependency_slide_background_image',
			),
			'validation'  => 'required',
		),
		array(
			'type'        => 'upload',
			'name'        => 'video_source',
			'label'       => __( 'Video Source (.mp4)', 'cian' ),
			'dependency'  => array(
				'field'    => 'background_media',
				'function' => 'cian_dependency_slide_background_video',
			),
		),
		array(
			'type'        => 'upload',
			'name'        => 'video_source_webm',
			'label'       => __( 'Video Source (.webm)', 'cian' ),
			'dependency'  => array(
				'field'    => 'background_media',
				'function' => 'cian_dependency_slide_background_video',
			),
		),
		array(
			'type'        => 'upload',
			'name'        => 'video_source_ogg',
			'label'       => __( 'Video Source (.ogg)', 'cian' ),
			'dependency'  => array(
				'field'    => 'background_media',
				'function' => 'cian_dependency_slide_background_video',
			),
		),
		array(
			'type'        => 'upload',
			'name'        => 'video_poster',
			'label'       => __( 'Fallback Image Poster', 'cian' ),
			'dependency'  => array(
				'field'    => 'background_media',
				'function' => 'cian_dependency_slide_background_video',
			),
			'description' => __( 'jpg, png, gif is recommended', 'cian' ),
		),
		array(
			'type'    => 'radiobutton',
			'name'    => 'video_ratio',
			'label'   => __( 'Video Ratio', 'cian' ),
			'items'   => array(
				array(
					'label' => __( '4:3', 'cian' ),
					'value' => '4:3',
				),
				array(
					'label' => __( '16:9', 'cian' ),
					'value' => '16:9',
				),
			),
			'dependency'  => array(
				'field'    => 'background_media',
				'function' => 'cian_dependency_slide_background_video',
			),
			'default' => array(
				'16:9',
			),
		),
		array(
			'type'    => 'toggle',
			'name'    => 'video_mute',
			'label'   => __( 'Video Mute by Default', 'cian' ),
			'dependency'  => array(
				'field'    => 'background_media',
				'function' => 'cian_dependency_slide_background_video',
			),
			'default' => '1',
		),
		array(
			'type'    => 'radiobutton',
			'name'    => 'color_scheme',
			'label'   => __( 'Color Scheme', 'cian' ),
			'items'   => array(
				'data' => array(
					array(
						'source' => 'function',
						'value'  => 'cian_data_source_color_scheme',
					),
				),
			),
			'default' => array(
				'dark',
			),
		),
		array(
			'type'		  => 'radioimage',
			'name' 		  => 'layout_slider',
			'label' 	  => esc_html__('Slide layout', 'cian'),
			'description' => esc_html__('Select the style you want for the slide', 'cian'),
			'item_max_width'    => '190',
			'item_max_height'   => '150',
			'items' => array(
				array(
					'value' => 'layout-0',
					'label' => esc_html__('Text on the left + Image on the right', 'cian'),
					'img' => '../../../images/slider/layout_0.png',
				),
				array(
					'value' => 'layout-1',
					'label' => esc_html__('Text on the center (no image)', 'cian'),
					'img' => '../../../images/slider/layout_1.png',
				),
				array(
					'value' => 'layout-2',
					'label' => esc_html__('Image on the left + Text on the right', 'cian'),
					'img' => '../../../images/slider/layout_2.png',
				),
			),
			'default' => array(
				'layout-0',
			),
		),
		array(
			'type'        => 'upload',
			'name'        => 'image_slide',
			'label'       => __( 'Image', 'cian' ),
			'dependency'  => array(
				'field'    => 'layout_slider',
				'function' => 'cian_dependency_image_slide',
			),
		),
		/*
		array(
			'type'    => 'radiobutton',
			'name'    => 'text_style',
			'label'   => __( 'Slide Text Style', 'cian' ),
			'items'   => array(
				array(
					'value' => 'style-0',
					'label' => __( 'Text rotator + buttons', 'cian' ),
				),
				array(
					'value' => 'style-1',
					'label' => __( 'Title + text + buttons', 'cian' ),
				),
			),
			'default' => array(
				'style-0',
			),
		),
		*/
		array(
			'type'        => 'textbox',
			'name'        => 'title',
			'label'       => __( 'Title', 'cian' ),
		),
		array(
			'type'        => 'textarea',
			'name'        => 'text_rotator',
			'label'       => __( 'Text rotator', 'cian' ),
			'description' => __( 'To set up the text rotator correctly, add some lines separated with line break', 'cian' ),
		),
		array(
			'type'       => 'textbox',
			'name'       => 'mt_interval',
			'label'      => __( 'Title Rotation Interval (ms)', 'cian' ),
			'default'    => 5000,
			'validation' => 'numeric',
		),
		array(
			'type'        => 'textbox',
			'name'        => 'button_text',
			'label'       => __( 'Button 1 Text', 'cian' ),
		),
		array(
			'type'        => 'textbox',
			'name'        => 'button_link',
			'label'       => __( 'Button 1 Link', 'cian' ),
		),
		array(
			'type'    => 'radiobutton',
			'name'    => 'button_link_style',
			'label'   => __( 'Button 1 Style', 'cian' ),
			'items'   => array(
				array(
					'value' => 'flat',
					'label' => __( 'Flat', 'cian' ),
				),
				array(
					'value' => 'border',
					'label' => __( 'Border', 'cian' ),
				),
			),
			'default' => array(
				'flat',
			),
		),
		array(
			'type'        => 'textbox',
			'name'        => 'button_2_text',
			'label'       => __( 'Button 2 Text', 'cian' ),
		),
		array(
			'type'        => 'textbox',
			'name'        => 'button_2_link',
			'label'       => __( 'Button 2 Link', 'cian' ),
		),
		array(
			'type'    => 'radiobutton',
			'name'    => 'button_2_link_style',
			'label'   => __( 'Button 2 Style', 'cian' ),
			'items'   => array(
				array(
					'value' => 'flat',
					'label' => __( 'Flat', 'cian' ),
				),
				array(
					'value' => 'border',
					'label' => __( 'Border', 'cian' ),
				),
			),
			'default' => array(
				'border',
			),
		),
		array(
			'type'        => 'select',
			'name'        => 'css_animation',
			'label'       => __( 'Enter Animation', 'cian' ),
			'items'       => array(
				'data' => array(
					array(
						'source' => 'function',
						'value'  => 'cian_data_source_animate_css',
					),
				),
			),
		),
	),
);