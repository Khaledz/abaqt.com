<header class="header-horizontal-top hidden-sm hidden-xs <?php echo is_user_logged_in() ? 'header-top-admin' : '' ?> <?php echo $enable_slider_section ? 'header-top' : '' ?> " id="header-horizontal-top">
	<div class="logo-container">
    	<h1>
        	<a href="<?php echo esc_url(home_url('/')); ?>">
	        	<?php if ( cian_option( 'header_logo' ) ) : ?>
				<img src="<?php echo esc_url(cian_option( 'header_logo' )); ?>" alt="<?php bloginfo( 'name' ); ?>" width="<?php echo esc_attr(cian_get_width_image(cian_option( 'header_logo' )));?>" height="<?php echo esc_attr(cian_get_height_image(cian_option( 'header_logo' )));?>"/>
				<?php else : ?>
				<span><?php bloginfo( 'name' ); ?></span>
				<?php endif ?>
            </a>
      	</h1>
	</div>
	<div class="menu-container">
		<nav class="horizontalMenu">
        	<?php 
           		wp_nav_menu( array(
              		'theme_location'    => 'header-navigation',
                	'depth'             => 0,
                	'container'         => false,
            	) );
        	?>
		</nav>
	</div>
</header>
		
<nav class="navbar navbar-default visible-sm visible-xs <?php echo is_user_logged_in() ? 'header-top-admin' : '' ?> <?php echo $enable_slider_section ? 'header-top' : '' ?>">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		  	</button>
	      	<h1>
	       		<a href="<?php echo esc_url(home_url('/')); ?>">
	               	<?php if ( cian_option( 'header_logo' ) ) : ?>
					<img src="<?php echo esc_url(cian_option( 'header_logo' )); ?>" alt="<?php bloginfo( 'name' ); ?>" width="<?php echo esc_attr(cian_get_width_image(cian_option( 'header_logo' )));?>" height="<?php echo esc_attr(cian_get_height_image(cian_option( 'header_logo' )));?>"/>
					<?php else : ?>
					<span><?php bloginfo( 'name' ); ?></span>
					<?php endif ?>
				</a>
			</h1>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      	<?php 
				wp_nav_menu( array(
					'theme_location'    => 'header-navigation',
					'depth'             => 0,
					'container'         => false,
				) );
			?>
		</div>
	</div>
</nav>
					