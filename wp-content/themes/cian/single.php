<?php
/**
 * The template for single posts
 * 
 * @package CIAN
 * @since CIAN 2.0
 */

get_header();

$show_sidebar = cian_sidebar_enable(); ?>

<section id="content" class="content-section section">
	<div class="container container-table">

		<div class="<?php if ( $show_sidebar ) { echo 'col-md-9 '; } else { echo 'col-md-12 '; } ?>main-section" role="main">
			<div class="container">
				<div class="row">
				
				<?php 
				if ( have_posts() ) : 
					while( have_posts() ) : the_post() ;
						get_template_part( 'loop-single' );					
					endwhile; 
				endif; 
				?>
	
				<?php comments_template(); ?>
				
				<?php if ( cian_option( 'enable_related_posts' ) ) : ?>
					<?php
					$recent_posts = cian_get_recent_posts(3,get_the_ID());
					$recent_posts_count = array_filter($recent_posts);
					if (!empty($recent_posts_count)) {?>
						<div class="related-posts">
							<h3 class="separator-title"><?php echo esc_html_e( 'Related posts', 'cian' )?></h3>
							<ul class="related-posts-list row">
							<?php 
							foreach($recent_posts as $post) :
								$id = $post -> ID;
								$format = get_post_format($id);
								if ( $format == 'quote' ) : ?>
								<li class="grid-post format-quote col-sm-4 col-xs-4">
									<a class="grid-post-thumbnail" href="<?php echo get_permalink($id); ?>" rel="bookmark">
										<span class="fa fa-quote-left"></span>
									</a>
									<h5><a class="grid-post-title" href="<?php echo get_permalink($id); ?>"><?php echo esc_html($post -> post_title); ?></a></h5>
								</li>
								
								<?php elseif( $format == 'link' ) : ?>
								<li class="grid-post format-link col-sm-4 col-xs-4">
									<a class="grid-post-thumbnail" href="<?php echo get_permalink($id); ?>" rel="bookmark">
										<span class="fa fa-link"></span>
									</a>
									<h5><a class="grid-post-title" href="<?php echo get_permalink($id); ?>"><?php echo esc_html($post -> post_title); ?></a></h5>
								</li>
								
								<?php else: ?>
								<li class="grid-post col-sm-4 col-xs-4">
									<a class="grid-post-thumbnail" href="<?php echo get_permalink($id); ?>" rel="bookmark">
										<img src="<?php echo cian_aq_resize( get_post_thumbnail_id( $id ), 340, 220, true, true ); ?>" alt="<?php echo esc_attr( $post -> post_title ); ?>" width="340" height="220" />
									</a>
									<h5><a class="grid-post-title" href="<?php echo get_permalink($id); ?>"><?php echo esc_html($post -> post_title); ?></a></h5>
								</li>
								<?php 
								endif;
							endforeach;
							?>
							</ul>		
					<?php } ?> 
					</div>
				<?php 
				endif; ?>
				
				<div class="post-pagination">
		
				<?php
				$prev_post = get_previous_post();
				$next_post = get_next_post();
				?>
				
				<?php if (!empty( $prev_post )) : ?>
					<div class="prev-post">
						<a href="<?php echo get_permalink( $prev_post->ID ); ?>">
							<div class="pagi-text">
								<i class="fa fa-angle-left"></i><span>PREVIOUS POST</span>
								<h5><?php echo get_the_title( $prev_post->ID ); ?></h5>
							</div>
						</a>
					</div>
				<?php endif; ?>
				
				<?php if (!empty( $next_post )) : ?>
					<div class="next-post">
						<a href="<?php echo get_permalink( $next_post->ID ); ?>">
							<div class="pagi-text">
								<span>NEXT POST</span> <i class="fa fa-angle-right"></i>
								<h5><?php echo get_the_title( $next_post->ID ); ?></h5>
							</div>
						</a>
					</div>
				<?php endif; ?>
					
			</div>
				
				</div>
			</div>
		</div>

		<?php 
		if ( $show_sidebar ) {
			get_sidebar(); 
		} ?>

	</div>
</section>

<?php if ( cian_option( 'enable_preloader', 1 ) ) : ?>
	<div id="jpreContent">
		<div id="loading-center"></div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>