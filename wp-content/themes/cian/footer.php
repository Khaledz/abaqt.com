			<?php 		
			for ( $i = 1; $i <= cian_option( 'footer_number_of_columns' );  $i++ ) {
				if ( is_active_sidebar( 'footer-sidebar-' . $i ) ) {
					$footer_widgets = '';
				}
			}
			if ( !is_page() && cian_option( 'enable_footer_widgets' ) && isset($footer_widgets) ) {
				$footer_widgets = true;
			}

			?>
			
			<footer class="<?php echo esc_attr(cian_option( 'footer_style' ));?> <?php if ( !empty($footer_widgets) ) { echo 'footer-top-border';} ?>" role="contentinfo">
				<?php if ( !empty($footer_widgets) ) : ?>
				<div class="container">
					<div class="row footer-widgets">
						<?php for ( $i = 1; $i <= cian_option( 'footer_number_of_columns' );  $i++ ) : ?>
							<div class="col-md-<?php echo cian_option( 'footer_grid_column_' . $i ); ?>">
								<?php if ( is_active_sidebar( 'footer-sidebar-' . $i ) ) {
									dynamic_sidebar( 'footer-sidebar-' . $i );
								} ?>
							</div>
						<?php endfor; ?>
					</div>
				</div>
				<?php endif; ?>
				<div class="container">
					<div class="row footer-content text-center">
						<hr>
						<?php if ( cian_option( 'enable_top_button' ) ) : ?>
						<button class="toTop css3transition" title="Up to the top">
							<span class="fa fa-angle-up"></span>
						</button>
						<?php endif; ?>
						<div class="logo">
							<?php if ( cian_option( 'footer_logo' ) ) : ?>
								<img src="<?php echo cian_option( 'footer_logo' ); ?>" alt="<?php bloginfo( 'name' ); ?>" width="<?php echo cian_get_width_image(cian_option( 'footer_logo' )); ?>" height="<?php echo cian_get_height_image(cian_option( 'footer_logo' )); ?>" />
							<?php else : ?>
								<span><?php bloginfo( 'name' ); ?></span>
							<?php endif ?>
						</div>
						<?php if ( ! empty( cian_option( 'copyright_text' ) ) ) : ?>
							<p class="copyright"><?php echo cian_option( 'copyright_text' ); ?></p>
						<?php endif; ?>
						<?php $footer_social_icons = cian_option( 'footer_social_icons', array() ); ?>
						<?php if ( ! empty( $footer_social_icons ) ) : ?>
							<ul class="footer-social">
							<?php foreach ( $footer_social_icons as $value ) : ?>
								<?php $link = cian_option( 'social_media_' . $value, null ); if ( empty( $link ) ) continue; ?>
								<li>
									<a class="<?php echo $value; ?>" href="<?php echo esc_url($link); ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $value; ?>">
										<span class="fa fa-<?php echo esc_attr($value); ?>"></span>
									</a>
								</li>
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</div>	
				</div>
			</footer>

		<?php wp_footer(); ?>

	</body>

</html>