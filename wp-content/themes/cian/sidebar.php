<div class="col-md-3 aside-section">
	<aside id="sidebar" class="sidebar">
		<?php if ( is_active_sidebar( 'content-sidebar' ) ) {
			dynamic_sidebar( 'content-sidebar' );
		} ?>
	</aside>
</div>