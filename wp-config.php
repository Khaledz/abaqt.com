<?php
/**
 * إعدادات الووردبريس الأساسية
 *
 * عملية إنشاء الملف wp-config.php تستخدم هذا الملف أثناء التنصيب. لا يجب عليك
 * استخدام الموقع، يمكنك نسخ هذا الملف إلى "wp-config.php" وبعدها ملئ القيم المطلوبة.
 *
 * هذا الملف يحتوي على هذه الإعدادات:
 *
 * * إعدادات قاعدة البيانات
 * * مفاتيح الأمان
 * * بادئة جداول قاعدة البيانات
 * * المسار المطلق لمجلد الووردبريس
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** إعدادات قاعدة البيانات - يمكنك الحصول على هذه المعلومات من مستضيفك ** //

/** اسم قاعدة البيانات لووردبريس */
define('DB_NAME', 'abaq');

/** اسم مستخدم قاعدة البيانات */
define('DB_USER', 'root');

/** كلمة مرور قاعدة البيانات */
define('DB_PASSWORD', '');

/** عنوان خادم قاعدة البيانات */
define('DB_HOST', 'localhost');

/** ترميز قاعدة البيانات */
define('DB_CHARSET', 'utf8mb4');

/** نوع تجميع قاعدة البيانات. لا تغير هذا إن كنت غير متأكد */
define('DB_COLLATE', '');

/**#@+
 * مفاتيح الأمان.
 *
 * استخدم الرابط التالي لتوليد المفاتيح {@link https://api.wordpress.org/secret-key/1.1/salt/}
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ezi)+Le4?nQtl!T+OWz=X7C`6Be1?qJb1<Cg&M:+}3<THQ>UiM=N9JJ6<ulRe6X>');
define('SECURE_AUTH_KEY',  '%PCNZ<kWCs}eG+6hRWgh`zG:6!/;K{#?>1y?<>`8(qY40xN*P.LXAG2ij3NGicCp');
define('LOGGED_IN_KEY',    'qhgARwavfJJK&:SOQhwz3%F$kfJO1N;H([)W#eX}!_>?48[8(e]=L-ny_c~Hy1Tc');
define('NONCE_KEY',        'F$D]ujyp$F(dR#D!_%B&0zR2V*]WIE6M&y.hlW}aolt}E]|;2`J/Gxq``uG@?%5$');
define('AUTH_SALT',        'TB<;sj~wtPgfD)}tKQ=Xl[5zU4xT+4gdhFf7B;]g9Jti2A+$x^zi<(hH:6GwIf83');
define('SECURE_AUTH_SALT', 'Y{pt-=Uh}E*>Bhw$DlH{-YPJw<GyuQ:hYdYD#|QU~td]-j$tk]*4T4E7qibow&(1');
define('LOGGED_IN_SALT',   '>~F8)-;=FU:uo,VaHo.aib*8Y3F )lw%E:M K<r@#VHO$Y{js]!=h-I+.<Ad@w8j');
define('NONCE_SALT',       ']=t4[p5*9BKX}]:O^O3;N!(3T/*zHgHokvYEaWHOf?Eh@CTIw/AX}|oMkLiijh9q');

/**#@-*/

/**
 * بادئة الجداول في قاعدة البيانات.
 *
 * تستطيع تركيب أكثر من موقع على نفس قاعدة البيانات إذا أعطيت لكل موقع بادئة جداول مختلفة
 * يرجى استخدام حروف، أرقام وخطوط سفلية فقط!
 */
$table_prefix  = 'wp_';

/**
 * للمطورين: نظام تشخيص الأخطاء
 *
 * قم بتغييرالقيمة، إن أردت تمكين عرض الملاحظات والأخطاء أثناء التطوير.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* هذا هو المطلوب، توقف عن التعديل! نتمنى لك التوفيق. */

/** المسار المطلق لمجلد ووردبريس. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** إعداد متغيرات الووردبريس وتضمين الملفات. */
require_once(ABSPATH . 'wp-settings.php');